USE AVATARDW

DROP PROCEDURE [Grants].[JCMH_SP_A_Rpt_JeffersonCountyGrant]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------
--Created 10/3/2018 to run Grant data for Jefferson County Grant
--MelodyB
--exec [Grants].[JCMH_SP_A_Rpt_JeffersonCountyGrant] '01/01/2018', '04/01/2018'
--Would place on DataMart today, but data is not complete
------------------------------------------------
Create Procedure .[Grants].[JCMH_SP_A_Rpt_JeffersonCountyGrant] @StartDate varchar(100), @EndDate varchar(100)
As
Begin
declare @startdateMod datetime, @enddateMod datetime
Set @startdateMod = Cast(@StartDate as datetime)
Set @enddateMod= DateAdd(s,-1,DateAdd(d,1,Cast(@EndDate as datetime))) 

-----------------Find Jeffersonians----------------------------------
If object_id('tempdb..#Jeffcos') is not null 
Drop table #Jeffcos
select distinct dets.patid, patient_county_Value 
into #Jeffcos
From avatardw.system.billing_tx_Charge_Detail dets
inner join 
(
select Row_Number() over(Partition by patid order by data_entry_date desc) 
	as RowNum,facility,blank_row_id,patid,data_entry_date, patient_county_value  
	from AVATARDW.[SYSTEM].[patient_demographic_history]
	where data_entry_date<= @enddateMod
	and patient_county_value = 'Jefferson'
)sub
on dets.patid=sub.patid
and dets.facility = sub.facility
where  dets.date_of_service <=@enddateMod 
--select * from #jeffcos where patid=129195


------------------Find the self pays-----8689-------------------------
--DECLARE @StartDate datetime
--		,@EndDate datetime
--Set @StartDate = '2018-01-01 00:00:00'
--Set @EndDate = '2018-03-31 23:59:59'
If object_id('tempdb..#SelfPays') is not null DROP TABLE #SelfPays
Select distinct patid, txhist_episode 
into #SelfPays
from JCMH.[dbo].[JCMH__Service_Avatar_research]
where 
(primary_guarantor in(1027,1105,1106)
	or 	primary_payor_guar_order in(1027,1105,1106)
	or 	primary_payor_note in(1027,1105,1106)
	or 	primary_payor_indigent in(1027,1105,1106))
and date_of_service < @enddateMod--8663

--select * from  JCMH.[dbo].[JCMH__Service_Avatar_research] where patid=16108245 order by date_of_service desc
--select * from #selfpays where patid=3025037

------------Admit records from Tier--------194647---------------
If object_id('tempdb..#Tier_Temp_Admits') is not null Drop table #Tier_Temp_Admits
If object_id('tempdb..#Temp') is not null Drop table #temp
If object_id('tempdb..#Tier_Admits') is not null Drop table #Tier_Admits

Select row_number()   Over( partition by ActType Order by clientid asc)  as rownum                                  
     ,clientid as PATID,[ActType],[AdmtDate],[OvLOF],[OvSymSev]
	 ,Case [Resid] WHEN 'X' then 'NA' WHEN 1 then 'Correctional Facility'   When 02 then 'Inpatient'   When 03 then 'ATU, Adults Only'    When 04 then 'Residential Treatment/Group' When 05 then 'Foster Home'  When  06 then 'Boarding home (Adult)'  When  07 then 'Group Home'  When  08 then 'Nursing Home'  When  09 then 'Residential Facility (MH Adult)'    When  10 then 'Residential Facility (Other)'    When 11 then 'Sober Living'    When  12 then 'Homeless' When  13 then 'Supported Housing' When  14 then 'Assisted Living' When 15 then 'Independent Living'  When 16 then 'Halfway House' End As RESID
	 ,Case ROLE WHEN 'X' then 'NA' When 1 then 'Employed full time (35+ hours/week)'    When 02 then 'Employed part time (<35 hours/week)'    When 03 then 'Unemployed'    When 04 then 'Supported Employment'    When 05 then 'Homemaker'    When 06 then 'Student'    When 07 then 'Retired'    When 08 then 'Disabled'    When 09 then 'Inmate'    When 10 then 'Military'    When 11 then 'Volunteer'End As Role 
Into #Tier_Temp_Admits
from [TIER].[dbo].[FD__CCAR2006] 
WHERE --clientid=46054426 and
 ActType = '01'

Select Patid, Max(Admtdate) MaxCC into #temp From #Tier_Temp_Admits GRoup by patid

Select rownum,b.PATID,ActType,AdmtDate,Replace(IsNull(OvLOF,0),'X','0') OVLOF,Replace(IsNull(OvSymSev,0),'X','0') OvSymSev,Isnull(Resid,0) Resid,Replace(Isnull(Role,0),'X','0') Role  
into #Tier_Admits 
from #Tier_Temp_Admits b 
inner join #temp t 
on  t.patid = b.patid
and t.MaxCC = b.AdmtDate 
--select * from #Tier_Admits where patid=129195 

------------Admit from Avatar---------------4791-------------
--DECLARE @StartDate datetime		,@EndDate datetime
--Set @StartDate = '2018-01-01 00:00:00'
--Set @EndDate = '2018-03-31 23:59:59'
If object_id('tempdb..#AV_Admit') is not null  Drop table #AV_Admit
SELECT distinct cc.PATID
,C011_Medicaid_State_Id
,C123_Action_Type
,'Admit' ActionType
,cc.C135_Admission_Date
,Case When cc.[C269_Crr_Role_Emply_Stat]=01 then 'Employed full time (35+ hours/week)'  When cc.[C269_Crr_Role_Emply_Stat] = 02 then 'Employed part time (<35 hours/week)'    When cc.[C269_Crr_Role_Emply_Stat] = 03 then 'Unemployed'    When cc.[C269_Crr_Role_Emply_Stat] = 04 then 'Supported Employment'    When cc.[C269_Crr_Role_Emply_Stat] = 05 then 'Homemaker'    When cc.[C269_Crr_Role_Emply_Stat] = 06 then 'Student'    When cc.[C269_Crr_Role_Emply_Stat] = 07 then 'Retired'    When cc.[C269_Crr_Role_Emply_Stat] = 08 then 'Disabled'    When cc.[C269_Crr_Role_Emply_Stat] = 09 then 'Inmate'    When cc.[C269_Crr_Role_Emply_Stat] = 10 then 'Military'    When cc.[C269_Crr_Role_Emply_Stat] = 11 then 'Volunteer'End As 'A_Employment'
,Case When cc.[C271_Place_of_Residence] = 01 then 'Correctional Facility'    When cc.[C271_Place_of_Residence] = 02 then 'Inpatient'    When cc.[C271_Place_of_Residence] = 03 then 'ATU, Adults Only'    When cc.[C271_Place_of_Residence] = 04 then 'Residential Treatment/Group'    When cc.[C271_Place_of_Residence] = 05 then 'Foster Home'    When cc.[C271_Place_of_Residence] = 06 then 'Boarding home (Adult)'    When cc.[C271_Place_of_Residence] = 07 then 'Group Home'    When cc.[C271_Place_of_Residence] = 08 then 'Nursing Home'    When cc.[C271_Place_of_Residence] = 09 then 'Residential Facility (MH Adult)'    When cc.[C271_Place_of_Residence] = 10 then 'Residential Facility (Other)'    When cc.[C271_Place_of_Residence] = 11 then 'Sober Living'    When cc.[C271_Place_of_Residence] = 12 then 'Homeless'    When cc.[C271_Place_of_Residence] = 13 then 'Supported Housing'    When cc.[C271_Place_of_Residence] = 14 then 'Assisted Living'    When cc.[C271_Place_of_Residence] = 15 then 'Independent Living'     When cc.[C271_Place_of_Residence] = 16 then 'Halfway House' End As 'A_Residency'
,cc.[C402_DOM_Ovral_Sympt_Sev]  [AVAdmit-SympSev]
,cc.[C408_DOM_Lvl_Functioning]  [AVAdmit-LOF]
,CC.[C198_Discharge_Date]		[AVAdmit-DISCHDATE]
Into #AV_Admit
FROM (Select patid, Max(C000_Assessment_date) MaxAssessDt 
			from [AvatarDW].[SYSTEM].[CCAR_Colorado_Client_Assessm] 
			where [C123_Action_Type] in('01','01Admission')
			and C000_Assessment_Date< @enddateMod	
			Group by patid) Max
inner join [AvatarDW].[SYSTEM].[CCAR_Colorado_Client_Assessm] cc 
	on Max.patid = cc.patid
	and Max.MaxAssessDt = cc.C000_Assessment_date
left join (Select patid, facility, episode_number from AVATARDW.system.episode_history
		where date_of_discharge<=@enddateMod ) eh
on cc.patid = eh.patid
and cc.facility = eh.facility
where cc.[Draft_Final] = 'F'
and cc.[C135_Admission_Date] <= @enddateMod
and cc.[C123_Action_Type] in('01','01Admission')--14441
--order by 8 desc
--select * from #AV_Admit order by 8 where patid=129195

----------Discharged Clients in Quarter---3244-------------------------------
--DECLARE @StartDate datetime
--		,@EndDate datetime
--Set @StartDate = '2018-01-01 00:00:00'
--Set @EndDate = '2018-03-31 23:59:59'
If object_id('tempdb..#AV_Disch') is not null  Drop table #AV_Disch
SELECT d.PATID, d.FACILITY, d.EPISODE_NUMBER, date_of_discharge, preadmit_admission_date admitDate
,ssd.[ss_discharge_adm_dict_1_value] GoalProgress
Into #AV_Disch
FROM AVATARDW.SYSTEM.view_episode_summary_discharge d
LEFT JOIN  AVATARDW.SYSTEM.site_specific_discharge_adm ssd 
on  d.patid				= ssd.patid 
and d.facility			= ssd.facility
and d.episode_number	= ssd.episode_number
WHERE date_of_discharge between @startdateMod and @enddateMod
and ssd.[ss_discharge_adm_dict_1_value] not in('CCC Only Admit')
--select * from #AV_Disch where patid = 129195 order by patid

---------DISCHARGE CCARS----------------1690------------------
--DECLARE @StartDate datetime
--		,@EndDate datetime
--Set @StartDate = '2018-01-01 00:00:00'
--Set @EndDate = '2018-03-31 23:59:59'
If object_id('tempdb..#AV_DisCCARS') is not null  Drop table #AV_DisCCARS
Select 
ID,cc.patid
,C011_Medicaid_State_Id
,[C135_Admission_Date] d_AdmitDate
,admitDate
,C198_Discharge_Date d_DischDate
,C000_Assessment_Date AssessmentDate
,C402_DOM_Ovral_Sympt_Sev d_SymSev
,C408_DOM_Lvl_Functioning_Value d_LOF
,Case 
    When cc.[C269_Crr_Role_Emply_Stat] = 01 then 'Employed full time (35+ hours/week)'
        When cc.[C269_Crr_Role_Emply_Stat] = 02 then 'Employed part time (<35 hours/week)'
        When cc.[C269_Crr_Role_Emply_Stat] = 03 then 'Unemployed'
        When cc.[C269_Crr_Role_Emply_Stat] = 04 then 'Supported Employment'
        When cc.[C269_Crr_Role_Emply_Stat] = 05 then 'Homemaker'
        When cc.[C269_Crr_Role_Emply_Stat] = 06 then 'Student'
        When cc.[C269_Crr_Role_Emply_Stat] = 07 then 'Retired'
        When cc.[C269_Crr_Role_Emply_Stat] = 08 then 'Disabled'
        When cc.[C269_Crr_Role_Emply_Stat] = 09 then 'Inmate'
        When cc.[C269_Crr_Role_Emply_Stat] = 10 then 'Military'
        When cc.[C269_Crr_Role_Emply_Stat] = 11 then 'Volunteer'
End As 'D_Employment'
,Case
        When cc.[C271_Place_of_Residence] = 01 then 'Correctional Facility'
        When cc.[C271_Place_of_Residence] = 02 then 'Inpatient'
        When cc.[C271_Place_of_Residence] = 03 then 'ATU, Adults Only'
        When cc.[C271_Place_of_Residence] = 04 then 'Residential Treatment/Group'
        When cc.[C271_Place_of_Residence] = 05 then 'Foster Home'
        When cc.[C271_Place_of_Residence] = 06 then 'Boarding home (Adult)'
        When cc.[C271_Place_of_Residence] = 07 then 'Group Home'
        When cc.[C271_Place_of_Residence] = 08 then 'Nursing Home'
        When cc.[C271_Place_of_Residence] = 09 then 'Residential Facility (MH Adult)'
		When cc.[C271_Place_of_Residence] = 10 then 'Residential Facility (Other)'
        When cc.[C271_Place_of_Residence] = 11 then 'Sober Living'
        When cc.[C271_Place_of_Residence] = 12 then 'Homeless'
        When cc.[C271_Place_of_Residence] = 13 then 'Supported Housing'
		When cc.[C271_Place_of_Residence] = 14 then 'Assisted Living'
        When cc.[C271_Place_of_Residence] = 15 then 'Independent Living' 
        When cc.[C271_Place_of_Residence] = 16 then 'Halfway House' 
End As 'D_Residency'
into #AV_DisCCARS
FROM [AvatarDW].[SYSTEM].[CCAR_Colorado_Client_Assessm] cc 
inner join (Select patid, admitdate from #AV_Disch) d
on cc.patid=d.patid
where C000_Assessment_Date <= @EndDate
and [C123_Action_Type] in ('05','05Discharge')
and [C198_Discharge_Date]>= @startdateMod
and [C198_Discharge_Date]<=@enddateMod
--select  * from #AV_DisCCARS where patid = 129195 order by Cast(patid as int)
--1690 dis ccars
----------COMBINE FOR END RESULTSET--------------------------------------------------------------------
TRUNCATE TABLE [GRANTS].[JeffersonCountyQuarterlyResults]
INSERT INTO [GRANTS].[JeffersonCountyQuarterlyResults]
Select  d.patid, c.ID
,d_AdmitDate DISAdmit ,c.AdmitDate CCARAdmit ,ta.AdmtDate TierAdmit ,c.d_DischDate
,Datediff(m,d_admitdate,d_DischDate) LOS_MM
,Case ISNUMERIC(d_SymSev) when 1 then d_SymSev else '999' END  SympSevAtDisch
,Case ISNULL([AVAdmit-SympSev],0)  when '0'  then 
			Case ta.OvSymSev when 0 then d_SymSev else ta.OvSymSev end when '' then Case ta.OvSymSev when 0 then d_SymSev else ta.OvSymSev end else [AVAdmit-SympSev] end  AdmitSymSev
,Case [OvSymSev]	when 0 then [AVAdmit-SympSev] else [OvSymSev] end		TierSymSev
,Left(d_LOF,1) DischargeLevelOfFunction
,Case IsNULL([AVAdmit-LOF],0) when 0 then [OVLOF] when '' then [OVLOF] else [AVAdmit-LOF] end AdmitLOF
,Case ISNULL([OVLOF],0) when 0 then [AVAdmit-LOF] else [OVLOF] end TierLOF
,[d_Residency] ResidencyAtDischarge
,Case ISNULL(A_Residency,'') when '' then RESID  else A_Residency END A_Residency
,Resid TierResidency
,[d_Employment] EmployAtDisch
,Case ISNULL(a_employment,'') when '' then ROLE else A_Employment END A_Employment
,Role TierEmployment
,GoalProgress 
,c.C011_Medicaid_State_Id
from #AV_Disch d
inner join #AV_DisCCARS c
	on c.patid=d.patid
inner join (Select * from #SelfPays ) s 
	on d.patid = s.patid
	and d.episode_number = s.txhist_episode
left join #Tier_Admits ta
	on c.patid = ta.patid	-- ta.admtdate<d_admitdate	--and ta.Admtdate>c.admitdate
left join #AV_Admit a
	on c.patid=a.patid
--where d.patid=10715620
order by cast(c.patid as int)

End
GO


