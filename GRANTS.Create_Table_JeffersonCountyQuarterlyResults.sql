

USE [AvatarDW]
GO

DROP TABLE Grants.JeffersonCountyQuarterlyResults
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE GRANTS.JeffersonCountyQuarterlyResults(
patid varchar(20) NULL,
ID varchar(50) NULL,
DISAdmit datetime NULL,
CCARAdmit datetime NULL,
TierAdit datetime NULL,
d_DischDate datetime NULL,
LOS_MM int NULL,
SympSevAtDisch int NULL,
AdmitSymSev varchar(10) NULL,
TierSymSev int NULL,
DischargeLevelOfFunction int NULL,
AdmitLOF int NULL,
TierLOF int NULL,
ResidencyAtDischarge varchar(100) NULL,
A_Residency varchar(100) NULL,
TierResidency varchar(100) NULL,
EmployAtDisch varchar(100) NULL,
a_employment varchar(100) NULL,
TierEmployment varchar(100) NULL,
GoalProgress varchar(100) NULL,
Medicaid_State_Id	varchar(20) NULL,
) ON [PRIMARY] 
GO


