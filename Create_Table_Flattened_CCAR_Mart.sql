
USE [DataMart2]
GO

DROP TABLE Reports.FlattenedCCAR_Mart
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE Reports.FlattenedCCAR_Mart(
	Manager Varchar(100) NULL,
	CareCoor varchar(100) NULL,
	[PATID] [varchar](10) NULL,
	[Patient_Name] [varchar](100) NULL,
	PreAdmissionDate [date] NULL,
	AssessmentDate [date] NULL,
	UpdateDate date NULL,
	Draft_Final varchar(5) NULL,
	LastDateOfService [date] NULL,
	ActionType [varchar](100) NULL,
	UpdateValue [varchar](100) NULL,
	Cov_EffectiveDate date NULL,
	[Team] [varchar](100) NULL,
	DueDateCalc date NULL,
	Green bit NULL,
	Yellow bit NULL,
	Red bit NULL,
	FutureDate bit NULL,
	Flag Varchar(10) NULL,
	Note_UniqueID varchar(50) NULL
) ON [PRIMARY]
GO


