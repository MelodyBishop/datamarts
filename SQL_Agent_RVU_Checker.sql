--Checks for missing Locations in RVU table
--Impacts CCHA guar 1116 only
--if missing, then emails notification to follow up
--then inserts rows into the table so that we are not missing any, but the value is NO for facility
--email is to force a response to follow up on the flag

Declare @Count int
DECLARE @bodyText varchar(200)

Set @Count=(select Count(*)
  from  (select code, code_value from AVATARDW.[SYSTEM].[dictionaries_pat_ext]
			where field_description='Location'
			and ext_field_number=578) loc
 Left join jcmh.dbo.[JCMH_A_TL_LK_Sev_RVUFacilities] Fac
  on loc.code = Fac.Location_code
  where Fac.Location_code 
  is null)

Set @BodyText =N'Please request from David Goff whether these are IsFacilty:<BR><BR>' +
N'<table border="1">' +
N'<tr><th>Location</th><th>Name</th>' +
CAST ( ( SELECT td = code,    '',
                td = code_value,  ''
from (select code, code_value from AVATARDW.[SYSTEM].[dictionaries_pat_ext]
			where field_description='Location'
			and ext_field_number=578) loc
 Left join jcmh.dbo.[JCMH_A_TL_LK_Sev_RVUFacilities] Fac
  on loc.code = Fac.Location_code
  where Fac.Location_code 
  is null
          FOR XML PATH('tr'), TYPE 
) AS NVARCHAR(MAX) ) +
N'</table>' ;

EXEC msdb.dbo.sp_send_dbmail  
    @profile_name = 'DataWarehouse',  
    @recipients = 'melodyb@jcmh.org',  
    @body_format = 'HTML',
	@body = @bodyText,
    @subject = 'RVU Facility Updates needed' 
	--@query ='Select code, code_value from (select code, code_value from AVATARDW.[SYSTEM].[dictionaries_pat_ext]
	--		where field_description=''Location''
	--		and ext_field_number=578) loc
	--		Left join jcmh.dbo.[JCMH_A_TL_LK_Sev_RVUFacilities] Fac
	--		on loc.code = Fac.Location_code
	--		  where Fac.Location_code 
	--		  is null' ; 

Insert into jcmh.dbo.[JCMH_A_TL_LK_Sev_RVUFacilities]
Select code, code_value, 'N' from (select code, code_value from AVATARDW.[SYSTEM].[dictionaries_pat_ext]
			where field_description='Location'
			and ext_field_number=578) loc
			Left join jcmh.dbo.[JCMH_A_TL_LK_Sev_RVUFacilities] Fac
			on loc.code = Fac.Location_code
			  where Fac.Location_code 
			  is null

--If (@Count>0)  
--exec dbo.RVU_LocationChecker
--else 
--select 1



GO




