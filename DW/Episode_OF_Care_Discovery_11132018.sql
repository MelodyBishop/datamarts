SELECT eh.patid, eh.episode_number,eh.program_value,eh.[preadmit_admission_date]
, ea.[program_value] AdmitProgram, ea.Episode_Number AdmitEpisode, esc.[program_value] CurrProgram, esc.Episode_Number CurrEpisode,dsc.program_value DischProgram,dsc.EPISODE_NUMBER DischEpisode
FROM [SYSTEM].[episode_history] eh --138,672 clients
LEFT JOIN [SYSTEM].[view_episode_summary_admit] ea
ON eh.patid = ea.PATID
AND eh.facility = ea.facility
AND eh.episode_number = ea.EPISODE_NUMBER
LEFT JOIN [SYSTEM].[view_episode_summary_current] esc
ON eh.patid = esc.PATID
AND eh.facility = esc.facility
AND eh.episode_number = esc.EPISODE_NUMBER
LEFT JOIN [SYSTEM].[view_episode_summary_discharge] dsc
ON eh.patid = dsc.PATID
AND eh.facility = dsc.facility
AND eh.episode_number = dsc.EPISODE_NUMBER
WHERE v_Patient_Name NOT LIKE 'Test,%'
AND v_Patient_Name NOT LIKE 'Group,%'
AND v_Patient_Name NOT LIKE 'ZZZ,%'


ORDER BY patid, episode_number
