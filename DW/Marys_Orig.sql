declare @endDate        datetime  
set @enddate = getdate()





select  txhist.id His_ID, cw.blank_row_id Not_ID, txhist.patid as txhist_patid, 
       cw.patid as cw_patid,      txhist.v_patient_name , 
       txhist.episode_number , cw.episode_number,
       txhist.JOIN_TO_TX_HISTORY as txhist_jointo_txhist, cw.JOIN_TO_TX_HISTORY as cw_jointo_txhist,
       txhist.ORIG_JOIN_TO_TX_HISTORY,
       txhist.service_code as txhist_serv_code, txhist.v_service_value as txhist_serv_value,
       cw.service_charge_code as cw_serv_chrg_code, cw.service_charge_value as cw_serv_chrg_value,
       txhist.location_value as txhist_loc_value, txhist.location_code as txhist_loc_code,
       cw.location_code as cw_loc_code, cw.location_value as cw_loc_value,
       txhist.program_code as txhist_pgm_code, txhist.program_value as txhist_pgm_value, 
       cw.service_program_value as cw_serv_pgm_value,  cw.service_program_code as cw_serv_pgm_code,
       txhist.provider_id as txhist_prov_id, txhist.v_provider_name as txhist_prov_name,
       cw.practitioner_id as cw_pract_id, cw.practitioner_name as cw_pract_name,
       txhist.duration as txhist_duration, cw.service_duration as cw_duration,
       cw.note_addresses,
       txhist.date_of_service as txhist_date_of_serv, txhist.start_time as txhist_serv_start_time,
       cw.date_of_service as cw_date_of_serv, cw.service_start_time as cw_serv_start_time,
       txhist.number_of_clients as txhist_nbr_of_clients, cw.number_clients_in_group as cw_nbr_client_grp,
       txhist.appointment_status_code as txhist_appt_status_code, txhist.appointment_status_value as txhist_appt_status_value,
       '' as cw_appt_status_code, '' as cw_appt_statu_value,
       txhist.data_entry_date as txhist_data_entry_date, txhist.data_entry_time as txhist_data_entry_time, 
       txhist.data_entry_by as txhist_data_entry_by, 
       cw.data_entry_date as cw_data_entry_date, cw.data_entry_time as cw_data_entry_time, cw.data_entry_by as cw_data_entry_by,
       txhist.NOT_uniqueid as txhist_NOT_Uniqueid, cw.NOT_uniqueid as cw_NOT_uniqueid,

       txhist.cost_of_service, txhist.medical_diag_1_icd10_code,  txhist.medical_diag_1_icd10_value,
       txhist.primary_guarantor, txhist.units_of_service,

       cw.option_desc, cw.option_id, cw.note_type_value

--into #temp_billtx_cwpat_notes
from avatardw.[SYSTEM].billing_tx_history txhist
  LEFT  join
      avatardw.cwssystem.cw_patient_notes cw
   on txhist.JOIN_TO_TX_HISTORY = cw.JOIN_TO_TX_HISTORY and txhist.patid = cw.patid
where txhist.facility = '1' 
  and (txhist.date_of_service < @enddate or txhist.date_of_service is null)
  AND txhist.service_code<>'DELETE'--- EXCLUDE?  Verify
  AND v_patient_name NOT LIKE 'Test,%'
  AND v_patient_name NOT LIKE 'Group,%'
  AND v_patient_name NOT LIKE 'ZZZ,%'
order by convert(int,txhist.patid)