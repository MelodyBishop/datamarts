/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2005 (9.0.3042)
    Source Database Engine Edition : Microsoft SQL Server Standard Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2005
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [JCMH]
GO

/****** Object:  StoredProcedure [dbo].[JC_A_SP_Service_Avatar]    Script Date: 11/13/2018 10:32:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--
--
--/* ************************************************************************************************************************************************************************************************************************************************ 
--Name:		    JC_A_SP_Service_Avatar
--Author:	    Mary Steffeck, Jefferson Center
--Created:	    February 2017
--Description:  This proc creates the Avatar side of the JCMH__SERVICE table
--              
--Modified:	    3/3/2017, MLS -- Add Unit Cost from TIER table BLU_UnitCost per the Cost Class
--              3/23/2017, MLS -- Add code to retrieve group notes (draft_final_value = unknown), RVU (base and sum) and break up
--                                Medicaid Eligiblity queries into 3 seperate steps and use the JOIN_TO_TX_HISTORY from 
--                                billing_tx_history to read billing_tx_charge_detail for the note's guarantor
--              4/10/2017, MLS -- For School Based group notes, set the primary payer to Medicaid JCMH, CAP if they are Medicaid
--                                eligible at the date of service, otherwise make the primary payer School Grant. And, changed the
--                                calculation of ISENROLLED to look at Jefferson Center admission and not Pre-Screening or 
--                                Community Contact.
--              7/28/2017, MLS -- Change how to lookup RVU values based on service code and cpt code.  Also, point to the
--                                billing_tx_master_fee_table instead of the audit version.
--              8/29/2017, MLS -- Add Indigent logic to Service table
--              10/23/2017, MLS -- Add Care Coordinator and program at the time of table creation.  And, add admission and discharge 
--                                 dates for the admission episode for the note.
--              10/30/2017, MLS -- Run creation job to daily, instead of monthly.
--              11/16/2017, MLS -- Add DX description for lk_joindx code.
--              12/8/2017, MLS -- Skip voided diagnoses (billing_order = ' ') to the diagnosis lookup code. And, add diagnosis time 
--                                to sort, which will help bring back the most recent diagnosis.
--              1/10/2018, MLS -- Refine skipping VOID diagnosis (diagnosis_status_code <> 5).  And, add the type of diagnosis to 
--                                the lookup logic (diagnosis_status_value).
--              1/24/2018, MLS -- Add note and primary diagnosis rank, status and description
--              4/3/2018, MLS -- Change query to get primary diagnosis; the sort (data_entry_date desc, date_of_diagnosis desc, 
--                               time_of_diagnosis desc), ranking order = 1 (primary)
--              9/18/2018, MLS -- Change query to get diagnosis, sort order: date_of_diagnosis desc, time_of_diagnosis desc,
--                                data_entry_date desc.  And, add ORIG_JOIN_TO_TX_HISTORY from billing_tx_history table.
--              11/6/2018, MLS - Change the sort for looking up the diagnosis.  Due to an automated diagnosis record being added,
--                               need to make sure this new record (midnight dx time) sorts to the bottom.
--
--*************************************************************************************************************************************/
CREATE   PROCEDURE [dbo].[JC_A_SP_Service_Avatar]
AS


declare @endDate        datetime  

--set @begindate = '10/1/2016'   -- Avatar go-live date
--set @endDate = '02/01/2017'


------ first day of current month
----set @endDate  = (select cast(convert(char(8), dateadd(d, -datepart(dd,getdate())+1, getdate()),
----                  112) as datetime))
--
-- --- get all notes prior to today, MLS 10/30/2017
set @enddate = getdate()

select 'Report Dates:  enddate='+ convert(varchar(12), @endDate, 101)




----drop table #temp_billtx_cwpat_notes
---- -----------------------------------
---- STEP 1 -- get all notes first
---- -----------------------------------
select txhist.id as txhist_opdocid, cw.blank_row_id as cw_blankrow_id, txhist.patid as txhist_patid, 
       cw.patid as cw_patid, 
       txhist.v_patient_name as txhist_pat_name, 
       txhist.episode_number as txhist_episode, cw.episode_number,
       txhist.JOIN_TO_TX_HISTORY as txhist_jointo_txhist, cw.JOIN_TO_TX_HISTORY as cw_jointo_txhist,
       txhist.ORIG_JOIN_TO_TX_HISTORY,
       txhist.service_code as txhist_serv_code, txhist.v_service_value as txhist_serv_value,
       cw.service_charge_code as cw_serv_chrg_code, cw.service_charge_value as cw_serv_chrg_value,
       txhist.location_value as txhist_loc_value, txhist.location_code as txhist_loc_code,
       cw.location_code as cw_loc_code, cw.location_value as cw_loc_value,
       txhist.program_code as txhist_pgm_code, txhist.program_value as txhist_pgm_value, 
       cw.service_program_value as cw_serv_pgm_value,  cw.service_program_code as cw_serv_pgm_code,
       txhist.provider_id as txhist_prov_id, txhist.v_provider_name as txhist_prov_name,
       cw.practitioner_id as cw_pract_id, cw.practitioner_name as cw_pract_name,
       txhist.duration as txhist_duration, cw.service_duration as cw_duration,
       cw.note_addresses,
       txhist.date_of_service as txhist_date_of_serv, txhist.start_time as txhist_serv_start_time,
       cw.date_of_service as cw_date_of_serv, cw.service_start_time as cw_serv_start_time,
       txhist.number_of_clients as txhist_nbr_of_clients, cw.number_clients_in_group as cw_nbr_client_grp,
       txhist.appointment_status_code as txhist_appt_status_code, txhist.appointment_status_value as txhist_appt_status_value,
       '' as cw_appt_status_code, '' as cw_appt_statu_value,
       txhist.data_entry_date as txhist_data_entry_date, txhist.data_entry_time as txhist_data_entry_time, 
       txhist.data_entry_by as txhist_data_entry_by, 
       cw.data_entry_date as cw_data_entry_date, cw.data_entry_time as cw_data_entry_time, cw.data_entry_by as cw_data_entry_by,
       txhist.NOT_uniqueid as txhist_NOT_Uniqueid, cw.NOT_uniqueid as cw_NOT_uniqueid,

       txhist.cost_of_service, txhist.medical_diag_1_icd10_code,  txhist.medical_diag_1_icd10_value,
       txhist.primary_guarantor, txhist.units_of_service,

       cw.aps_link, cw.option_desc, cw.option_id, cw.note_type_value

into #temp_billtx_cwpat_notes
from avatardw.[SYSTEM].billing_tx_history txhist
  inner join
      avatardw.cwssystem.cw_patient_notes cw
   on txhist.JOIN_TO_TX_HISTORY = cw.JOIN_TO_TX_HISTORY and txhist.patid = cw.patid
where txhist.facility = '1' 
  and (txhist.date_of_service < @enddate or txhist.date_of_service is null)
order by convert(int,txhist.patid)

--select *
--from #temp_billtx_cwpat_notes


--drop table #temp_services_notedemog
---- -------------------------------------------------
---- STEP 2 -- get current client's demographic info
---- -------------------------------------------------
select nt.txhist_opdocid, nt.txhist_patid, txhist_episode, nt.txhist_jointo_txhist, 
       nt.ORIG_JOIN_TO_TX_HISTORY, nt.txhist_serv_code, 
       nt.txhist_serv_value, nt.txhist_loc_value, nt.txhist_loc_code,
       nt.txhist_pgm_code, nt.txhist_pgm_value, nt.txhist_prov_id, nt.txhist_prov_name, nt.txhist_duration, 
       nt.txhist_date_of_serv, nt.note_addresses, nt.cw_jointo_txhist,
       nt.txhist_serv_start_time, nt.txhist_nbr_of_clients, nt.txhist_appt_status_code, nt.txhist_appt_status_value,
       nt.cw_data_entry_date, nt.cw_data_entry_time, nt.cw_data_entry_by, nt.cw_NOT_uniqueid, nt.cost_of_service,
       nt.medical_diag_1_icd10_code, nt.medical_diag_1_icd10_value, 

       nt.primary_guarantor, nt.units_of_service, nt.aps_link,
       nt.option_desc, nt.option_id, nt.note_type_value,

       pat.patient_name, patient_name_last, patient_name_first, pat.patient_sex_code, pat.date_of_birth, 
       pat.patient_add_city, pat.patient_add_county_value, pat.patient_add_zipcode, pat.patient_ssn
into #temp_services_notedemog 
from #temp_billtx_cwpat_notes nt

   outer apply (select top 1 pt.patient_name, patient_name_last, patient_name_first, pt.patient_sex_code, 
                             pt.date_of_birth, pt.patient_add_city, 
                             pt.patient_add_county_value, pt.patient_add_zipcode, pt.patient_ssn
                from AvatarDW.SYSTEM.patient_current_demographics pt
                where pt.patid  = nt.txhist_patid) pat

--select *
--from #temp_services_notedemog
--where note_dxtype in ('Void','Working','Resolved')
--order by txhist_patid

---- -------------------------------------------------
---- STEP 2a -- get note's diagnosis properties
---- -------------------------------------------------
select t.txhist_opdocid, t.txhist_patid, t.txhist_episode, t.txhist_jointo_txhist, 
       t.ORIG_JOIN_TO_TX_HISTORY, t.txhist_serv_code, 
       t.txhist_serv_value, t.txhist_loc_value, t.txhist_loc_code,
       t.txhist_pgm_code, t.txhist_pgm_value, t.txhist_prov_id, t.txhist_prov_name, t.txhist_duration, 
       t.txhist_date_of_serv, t.note_addresses, t.cw_jointo_txhist,
       t.txhist_serv_start_time, t.txhist_nbr_of_clients, t.txhist_appt_status_code, t.txhist_appt_status_value,
       t.cw_data_entry_date, t.cw_data_entry_time, t.cw_data_entry_by, t.cw_NOT_uniqueid, t.cost_of_service,
       t.medical_diag_1_icd10_code, t.medical_diag_1_icd10_value, 

       note_dx.diagnosis_status_value as note_dxtype, note_dx.ranking_value as note_dxrank,

       t.primary_guarantor, t.units_of_service, t.aps_link,
       t.option_desc, t.option_id, t.note_type_value,
       t.patient_name, t.patient_name_last, t.patient_name_first, t.patient_sex_code, t.date_of_birth, 
       t.patient_add_city, t.patient_add_county_value, t.patient_add_zipcode, t.patient_ssn

into #temp_services_notedx 
from #temp_services_notedemog  t

   -- get the type and rank of note's diagnosis  -- added, MLS 1/10/2018
   outer apply (select top 1 dxe.diagnosis_status_value, dxe.ranking_value
                from avatardw.system.client_diagnosis_entry dxe
                  inner join
                     avatardw.system.client_diagnosis_record dxr
                   on dxe.diagnosisrecord = dxr.id
                where dxe.patid = t.txhist_patid 
                  and dxe.icd_code = t.medical_diag_1_icd10_code
                  and cast(t.txhist_date_of_serv as datetime) >= cast(dxr.date_of_diagnosis as datetime)
                order by dxe.id, dxr.date_of_diagnosis desc, dxr.time_of_diagnosis, dxe.billing_order) note_dx
--
--select  *
--from #temp_services_notedx


--drop table #temp_Services_demog_DX
-- --------------------
-- STEP 3 -- get primary diagnosis at the time of the service (runs 10 min)
-- --------------------
select txhist_opdocid, txhist_patid as patid, txhist_episode, note_type_value, cw_data_entry_date, 
       cw_data_entry_time, cw_data_entry_by,
       txhist_pgm_code, txhist_pgm_value, txhist_serv_code, txhist_serv_value, txhist_duration, note_addresses,
       cw_NOT_uniqueid, txhist_jointo_txhist, ORIG_JOIN_TO_TX_HISTORY,
       txhist_date_of_serv, txhist_serv_start_time, 
       txhist_loc_code, txhist_loc_value, medical_diag_1_icd10_code, medical_diag_1_icd10_value, 
       note_dxtype, note_dxrank,

       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, cost_of_service,
       primary_guarantor, units_of_service, aps_link, option_desc, option_id,

       (select fy.FiscalYear
        from jcmh.dbo.JCMH__FiscalYear fy
        where t.txhist_date_of_serv >= fy.BeginDate and t.txhist_date_of_serv <= fy.EndDate) as note_FY,

        dx_select.icd_code as lk_joindx, dx_select.diagnosis_status_value as lk_dxtype, 
        dx_select.ranking_value as lk_dxrank, -- no diagnosis on note, lookup diagnosis

        txhist_prov_id, txhist_prov_name, 
        patient_name, patient_name_last, patient_name_first, patient_sex_code, date_of_birth,
         -- calculate age at beginning of fiscal year (July 1st)     
        datediff (year, t.date_of_birth, (select fy.begindate
                                          from jcmh.dbo.JCMH__FiscalYear fy
                                          where CAST(CONVERT(char(8), t.cw_data_entry_date, 112) AS DATETIME) >= fy.begindate
                                            and CAST(CONVERT(char(8), t.cw_data_entry_date, 112) AS DATETIME) <= fy.enddate)) as FYAge,

        patient_add_city, patient_add_county_value, patient_add_zipcode, patient_ssn,

       (select top 1 social_security_number 
        from avatardw.[SYSTEM].patient_demographic_history pdh
        where pdh.patid = t.txhist_patid
        order by MPI_uniqueid desc) as lk_ssn

into #temp_Services_demog_DX
from #temp_services_notedx  t

  outer apply (select top 1 dxe.icd_code, dxe.diagnosis_status_value, dxe.ranking_value
               from avatardw.system.client_diagnosis_entry dxe
                 inner join
                    avatardw.system.client_diagnosis_record dxr
                  on dxe.diagnosisrecord = dxr.id
               where dxe.patid = t.txhist_patid  
                 and dxe.billing_order <> ' '  -- skip VOIDED Dx - MLS, 12/8/2017
                 --and diagnosis_status_code <> 5  -- change 'skip VOID DX - MLS, 1/10/2018
                 and dxe.ranking_code = '1'  -- primary dx only  -- MLS, 4/3/2018
                 and cast(t.txhist_date_of_serv as datetime) >= cast(dxr.date_of_diagnosis as datetime)
               order by dxr.date_of_diagnosis desc, 
                        DateAdd(Second, -1, Cast(dxr.time_of_diagnosis as datetime)) desc,  -- sort midnight dx time to bottom, MLS 
                        dxe.data_entry_date desc) dx_select
               --order by dxr.date_of_diagnosis desc, dxr.time_of_diagnosis desc, dxe.data_entry_date desc) dx_select  -- change sort, MLS 9/18/2018
               --order by dxe.data_entry_date desc, dxr.date_of_diagnosis desc, dxr.time_of_diagnosis desc) dx_select  -- change sort, MLS 4/3/2018

where UPPER(patient_name_last) not in ('TEST','GROUP')  -- remove test clients
  and substring(txhist_prov_name, 1,5) <> 'TEST,'  -- remove test clinicians


--select  *
--from #temp_Services_demog_DX



--drop table #temp_misc_fields
-- --------------------------------------------------------------------------------------
-- STEP 4 -- grab clinician's credential, fedpos of service location, client's income, etc.
-- --------------------------------------------------------------------------------------
select txhist_opdocid, patid, txhist_episode, note_type_value, cw_data_entry_date, cw_data_entry_time, cw_data_entry_by,
       txhist_pgm_code, txhist_pgm_value, txhist_serv_code, txhist_serv_value, txhist_duration, note_addresses,
       cw_NOT_uniqueid, txhist_jointo_txhist, ORIG_JOIN_TO_TX_HISTORY,
       txhist_date_of_serv, txhist_serv_start_time, 
       txhist_loc_code, txhist_loc_value, 
       medical_diag_1_icd10_code, medical_diag_1_icd10_value, note_dxtype, note_dxrank,

       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, cost_of_service,
       primary_guarantor, units_of_service, aps_link, option_desc, option_id, note_FY, 
       lk_joindx, lk_dxtype, lk_dxrank,
       txhist_prov_id, txhist_prov_name, patient_name, patient_name_last, patient_name_first, 
       patient_sex_code, date_of_birth, FYAge, patient_add_city, patient_add_county_value,
       patient_add_zipcode, patient_ssn, lk_ssn,

        case 
           when (FYAge >= 0 and FYAge <= 11) then 'Child (0-11)'
           when (FYAge >= 12 and FYAge <= 17) then 'Adolescent (12 - 17)'
           when (FYAge >= 18 and FYAge <= 20) then 'Young Adult (18 - 20)'
           when (FYAge >= 21 and FYAge <= 59) then 'Adult (21 - 59)'
           when (FYAge >= 60) then 'Older Adult (60+)'
        end as AgeCategory,

       -- this only get a credential at date of service
       (select top 1 cr.practitioner_category_value
        from avatardw.system.staff_current_demographics cr
        where cr.staffid = t.txhist_prov_id
          and t.txhist_date_of_serv >= cr.registration_date
        order by cr.registration_date desc) as credentials,

       -- find fedpos for service location
       (select top 1 extended_attribute_code  -- FEDPOS nbr (element #578)
        from avatardw.system.dictionaries_pat_ext ext
        where ext.code = t.txhist_loc_code
          and field_description = 'Location'
          and ext_field_number = '578') as FEDPOS,

        -- find income for the date of service
       (select top 1 fin.item1  -- fin.income_total_dollar  -- change source column for income, MLS 1/17/2017
        from avatardw.system.JCMH_C_Financial_Primary fin
        where fin.patid  = t.patid
          and t.txhist_date_of_serv >= fin.entry_date
        order by fin.entry_date desc) as income,

       -- nbr of dependents - avatar -- added to indigent income calculation  -- add number of dependents, MLS 6/5/2017
       (select top 1 fin.item2
        from Avatardw.[SYSTEM].JCMH_C_Financial_Primary fin
        where fin.patid = t.patid 
        order by fin.entry_date desc) as nbr_dependents,

       -- current income - tier  -- added for fee schedule income indigent calculation logic, MLS 6/6/2017
       (select top 1 fin.totalincome   -- change source column for income, MLS 1/17/2017
        from tier.dbo.FD__INCOME_SOURCE fin
        where fin.clientkey = t.patid 
        order by fin.effective_date desc) as tier_income,

       -- nbr of dependents - tier -- added for fee schedule income indigent calculation logic, MLS 6/6/2017
       (select top 1 fin.Numb_dependents
        from tier.dbo.FD__INCOME_SOURCE fin
        where fin.clientkey = t.patid 
        order by fin.effective_date desc) as tier_nbr_dependents,

       (select top 1 vosc.[VO CPT CODE]  -- ????? or should I check service fee maint table (file_import_svc_fee, 
                                                 --billing_tx_master_fee_table, billing_tx_master_fee_audit)
        from Feeds.dbo.JC_A_TL_LK_Enc_ServCodes vosc
        where vosc.[Service Code] = t.txhist_serv_code) as vo_cpt_code_conv,

       -- what time of procedure is this, enc, hour, 15min, day  -- 99404 will show as an 'enc' and later converted 
       --                                                           to 90832, 90834 and 90837
       --    read the lookup table for the coding manual cpt code
       (select top 1 vor.Unit_Calc_Type
        from Feeds.dbo.JC_A_TL_LK_Enc_CPTCode_Rates vor
        where vor.vo_cpt_code = (select top 1 vosc.[VO_CPT_Code]
                                 from Feeds.dbo.JC_A_TL_LK_Enc_CPTCode_Rates vosc
                                 where vosc.[VO_CPT_CODE]  =  (select top 1 sc.[VO CPT CODE]
                                                               from Feeds.dbo.JC_A_TL_LK_Enc_ServCodes sc
                                                               where sc.[Service Code] = t.txhist_serv_code))) as enc_type,

       -- is prockey/cpt code "reimbursed behavioral health care services" or covered by Medicare - can not be considered indigent
       case 
         when t.txhist_serv_code in ('100','101','102','103','104','105','106','107','108','109','110','111','112','113','114',
                                      '115','116','117','118','119','120','121','122','123','200','201','202','203','204','500',
                                      '501','502','503','504','505','506','508','600','601','603','604','700','702','910','911',
                                      '918','936','937')
           then 'Y'
         else 'N'
       end as mcrecovcptcode

into #temp_misc_fields
from #temp_Services_demog_DX t

--
--select *
--from #temp_misc_fields

--drop table #temp_enc_feesched_avatar
-- --------------------------------------------------------------------------------------
-- STEP 5 -- get first pass at fee schedule - avatar and Medicare and Medicaid payer info
-- --------------------------------------------------------------------------------------
select txhist_opdocid, patid, txhist_episode, note_type_value, cw_data_entry_date, 
       cw_data_entry_time, cw_data_entry_by,
       txhist_pgm_code, txhist_pgm_value, txhist_serv_code, txhist_serv_value, txhist_duration, note_addresses,
       cw_NOT_uniqueid, txhist_jointo_txhist, ORIG_JOIN_TO_TX_HISTORY,
       txhist_date_of_serv, txhist_serv_start_time, 
       txhist_loc_code, txhist_loc_value, 
       medical_diag_1_icd10_code, medical_diag_1_icd10_value, note_dxtype, note_dxrank,

       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, cost_of_service,
       primary_guarantor, units_of_service, aps_link, option_desc, option_id, note_FY, 
       lk_joindx, lk_dxtype, lk_dxrank, 
       txhist_prov_id, txhist_prov_name, patient_name, patient_name_last, patient_name_first, 
       patient_sex_code, date_of_birth, FYAge, patient_add_city, patient_add_county_value,
       patient_add_zipcode, patient_ssn, lk_ssn,

       agecategory, credentials, fedpos, income, nbr_dependents, 
       vo_cpt_code_conv, enc_type, mcrecovcptcode,

       (select ext.extended_attribute_value
        from avatardw.system.dictionaries_pat_ext ext
        where ext_field_description = 'Treatment Service'
          and ext.code = txhist_pgm_code) as dept_name,

       -- are any of these Medicare supplemental payers in effect at the date of service
       (select count(*)
        from avatardw.[SYSTEM].billing_guar_emp_data py
        where py.patid  = t.patid 
          and (t.txhist_date_of_serv >= py.cov_effective_date 
            and (t.txhist_date_of_serv <= py.cov_expiration_date or py.cov_expiration_date is NULL))
          and py.guarantor_id in (1068,1066,1065,1076,1010,1015,1016,1023,1031,1043,1060,1100,1000,1006,1046,1079,
                                  1002,1095,1067,1057,1101)) as nbr_mcarepayers,

        -- is there Jefferson Center Medicaid JCMH, CAP payer in effect 
       (select top 1 gu.subs_policy 
        from avatardw.[SYSTEM].billing_guar_subs_data gu
        where gu.patid  = t.patid 
          and gu.guarantor_id in (1065)) as JCMH_McaidID,  

       -- is there Jefferson Center Medicaid JCMH, CAP payer in effect at the date of service
       (select count(*)
        from avatardw.[SYSTEM].billing_guar_emp_data py
        where py.patid  = t.patid 
          and (t.txhist_date_of_serv >= py.cov_effective_date 
            and (t.txhist_date_of_serv <= py.cov_expiration_date or py.cov_expiration_date is NULL))
          and py.guarantor_id in (1065)) as JCMH_Mcaidpayer,

       -- are any of these excluded payers in effect at the date of service
       (select count(*)
        from avatardw.[SYSTEM].billing_guar_emp_data py
        where py.patid = t.patid 
          and (t.txhist_date_of_serv >= py.cov_effective_date 
            and (t.txhist_date_of_serv <= py.cov_expiration_date or py.cov_expiration_date is NULL))
          and py.guarantor_id in (1003,1011,1014,1017,1024,1029,1037,1041,1051,1053,1055,1056,1061,1062,1063,1064,
                                  1075,1085,1087,1089,1091,1098,
                                  1007,1012,1013,1020,1021,1022,1026,1033,1034,1108,1047,1049,         -- added per Ann Jones       
                                  1052,1058,1065,1078,1082,1086,1106,1096,1103)) as exclude_payers,    --    3/10/2017, MLS

       -- are there any Medicare Advantage payers in effect at the date of service
       (select count(*)
        from avatardw.[SYSTEM].billing_guar_emp_data py
        where py.patid = t.patid
          and (t.txhist_date_of_serv >= py.cov_effective_date 
            and (t.txhist_date_of_serv <= py.cov_expiration_date or py.cov_expiration_date is NULL))
          and py.guarantor_id in (1000,1039,1094,1107,2002)) as nbr_mcareAdv,

       feesch.nbrdep_1, feesch.nbrdep_2, feesch.nbrdep_3, feesch.nbrdep_4, feesch.nbrdep_5,
       -- using the income and number of dependents, the sliding fee is determined
       case
         when nbr_dependents = 0 then 6   -- check if this value is valid - use 6% sliding fee schedule  -- also check if dep & income NULL
         when nbr_dependents = 1 then feesch.nbrdep_1
         when nbr_dependents = 2 then feesch.nbrdep_2
         when nbr_dependents = 3 then feesch.nbrdep_3
         when nbr_dependents = 4 then feesch.nbrdep_4
         when nbr_dependents >= 5 then feesch.nbrdep_5
         else NULL
       end as fee_percent,  -- 6% means maybe indigent, 100% means not indigent

       tier_income, tier_nbr_dependents

into  #temp_enc_feesched_avatar
from  #temp_misc_fields t
                -- look at lookup table for fee schedule by income and return all 5 fee percent -- 6/2017, MLS
  outer apply (select top 1 nbrdep_1, nbrdep_2, nbrdep_3, nbrdep_4, nbrdep_5
               from feeds.dbo.JC_A_TL_LK_Enc_Fee_Sched fs
               where t.income >= fs.from_income and t.income <= fs.to_income)feesch

--select *
--from #temp_enc_feesched_avatar

-- drop table #temp_enc_guar_rnd1
-- --------------------------------------------------------------------------------------
-- STEP 6 -- get first pass at fee schedule - tier and guarantor info
-- --------------------------------------------------------------------------------------
select txhist_opdocid, patid, txhist_episode, note_type_value, cw_data_entry_date, 
       cw_data_entry_time, cw_data_entry_by,
       txhist_pgm_code, txhist_pgm_value, txhist_serv_code, txhist_serv_value, txhist_duration, note_addresses,
       cw_NOT_uniqueid, txhist_jointo_txhist, ORIG_JOIN_TO_TX_HISTORY,
       txhist_date_of_serv, txhist_serv_start_time, 
       txhist_loc_code, txhist_loc_value, medical_diag_1_icd10_code, medical_diag_1_icd10_value,
       note_dxtype, note_dxrank,

       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, cost_of_service,
       primary_guarantor, units_of_service, aps_link, option_desc, option_id, note_FY, 
       lk_joindx, lk_dxtype, lk_dxrank,
       txhist_prov_id, txhist_prov_name, patient_name, patient_name_last, patient_name_first, 
       patient_sex_code, date_of_birth, FYAge, patient_add_city, patient_add_county_value,
       patient_add_zipcode, patient_ssn, lk_ssn,

      left(replace(lk_ssn, '-', ''), 9) as format_ssn,  -- format SSN for medicaid elig read 

       agecategory, credentials, fedpos, 
       vo_cpt_code_conv, enc_type, mcrecovcptcode, dept_name,
       nbr_mcarepayers, JCMH_Mcaidpayer, JCMH_McaidID, exclude_payers, nbr_mcareAdv,

      -- get primary payor by charge waterfall order
       --  if no payer (fee_standard > 0), make primary payor and payor type UNKNOWN
       (select top 1 dt.guarantor_id 
        from avatardw.[SYSTEM].billing_tx_charge_detail dt
          inner join 
             avatardw.[SYSTEM].billing_guar_subs_data gu
            on dt.patid = gu.patid and dt.guarantor_id = gu.guarantor_id
        where dt.patid = t.patid
          and dt.join_to_tx_history = t.txhist_jointo_txhist
          and dt.guarantor_id not in (2000,2001)
        order by gu.guarantor_order) as primary_guar_id, 

       (select top 1 dt.guarantor_id  --gu.guarantor_name  -- changed from guar ID to guar name -- MLS, 5/25/2017
        from avatardw.[SYSTEM].billing_tx_charge_detail dt
          inner join 
             avatardw.[SYSTEM].billing_guar_subs_data gu
            on dt.patid = gu.patid and dt.guarantor_id = gu.guarantor_id
        where dt.patid  = t.patid  and dt.join_to_tx_history = t.txhist_jointo_txhist
          and dt.guarantor_id not in (2000,2001)
        order by gu.guarantor_order) as primary_payor, 

       -- changed the primary billing payor to look at guarantor order and date of service  -- MLS, 5/24/2017
       (select top 1 sd.guarantor_id  --sd.guarantor_name 
        from avatardw.[SYSTEM].billing_guar_emp_data ed
          inner join
             avatardw.[SYSTEM].billing_guar_subs_data sd
            on ed.patid = sd.patid and ed.s_index = sd.s_index
        where ed.patid = t.patid
          and sd.guarantor_id not in (2000,2001)  -- bypass Writeoff Guarantor
          and ((convert(datetime, t.txhist_date_of_serv, 112) >= 
                convert(datetime, ed.cov_effective_date, 112))
           and ((convert(datetime, t.txhist_date_of_serv, 112) <= 
                 convert(datetime, ed.cov_expiration_date, 112)) or ed.cov_expiration_date is null))
        order by sd.guarantor_order) as primary_billing_payor,

       income, nbr_dependents, nbrdep_1, nbrdep_2, nbrdep_3, nbrdep_4, nbrdep_5, fee_percent,
       tier_income, tier_nbr_dependents,

       feesch.tier_nbrdep_1, feesch.tier_nbrdep_2, feesch.tier_nbrdep_3, feesch.tier_nbrdep_4, feesch.tier_nbrdep_5,

       case
         when tier_nbr_dependents = 0 then 6   -- check if this value is valid - use 6% sliding fee schedule  -- also check if dep & income NULL
         when tier_nbr_dependents = 1 then feesch.tier_nbrdep_1
         when tier_nbr_dependents = 2 then feesch.tier_nbrdep_2
         when tier_nbr_dependents = 3 then feesch.tier_nbrdep_3
         when tier_nbr_dependents = 4 then feesch.tier_nbrdep_4
         when tier_nbr_dependents >= 5 then feesch.tier_nbrdep_5
         else NULL
       end as tier_fee_percent

into #temp_enc_guar_rnd1
from #temp_enc_feesched_avatar t

  outer apply (select top 1 nbrdep_1 as tier_nbrdep_1, nbrdep_2 as tier_nbrdep_2, nbrdep_3 as tier_nbrdep_3, 
                            nbrdep_4 as tier_nbrdep_4, nbrdep_5 as tier_nbrdep_5
               from feeds.dbo.JC_A_TL_LK_Enc_Fee_Sched fs
               where t.tier_income >= fs.from_income and t.tier_income <= fs.to_income) feesch

----
--select *
--from #temp_enc_guar_rnd1
----
--drop table #temp_serv_mcaid_elig1
-- --------------------------------------------------------------------------------------
-- STEP 7 -- initial pass through Medicaid Eligibility file
-- --------------------------------------------------------------------------------------
select txhist_opdocid, patid, txhist_episode, note_type_value, cw_data_entry_date, 
       cw_data_entry_time, cw_data_entry_by,
       txhist_pgm_code, txhist_pgm_value, txhist_serv_code, txhist_serv_value, txhist_duration, note_addresses,
       cw_NOT_uniqueid, txhist_jointo_txhist, ORIG_JOIN_TO_TX_HISTORY,
       txhist_date_of_serv, txhist_serv_start_time, 
       txhist_loc_code, txhist_loc_value, medical_diag_1_icd10_code, medical_diag_1_icd10_value,
       note_dxtype, note_dxrank,

       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, cost_of_service,
       primary_guarantor, units_of_service, aps_link, option_desc, option_id, note_FY, 
       lk_joindx, lk_dxtype, lk_dxrank,
       txhist_prov_id, txhist_prov_name, patient_name, patient_name_last, patient_name_first, 
       patient_sex_code, date_of_birth, FYAge, patient_add_city, patient_add_county_value,
       patient_add_zipcode, patient_ssn, lk_ssn, format_ssn,

       agecategory, credentials, fedpos,
       vo_cpt_code_conv, enc_type, mcrecovcptcode, dept_name,
       nbr_mcarepayers, JCMH_Mcaidpayer, JCMH_McaidID, exclude_payers, nbr_mcareAdv,
       primary_guar_id, primary_payor, primary_billing_payor, 
       income, nbr_dependents,
       nbrdep_1, nbrdep_2, nbrdep_3, nbrdep_4, nbrdep_5, fee_percent, 
       tier_income, tier_nbr_dependents,
       tier_nbrdep_1, tier_nbrdep_2, tier_nbrdep_3, tier_nbrdep_4, tier_nbrdep_5, tier_fee_percent,

       -- can a combo of income and nbr of dependents considered indigent -- 6/2017, MLS
       --    so if fee_percent = 100 then income not indigent
       --    and if fee_percent = 6 then income indigent
       case
         when t.fee_percent = 6 then 'Yes'  -- avatar at 6% of full fee
         when t.fee_percent = 100 then 'No'  -- avatar at 100% of full fee
         when t.tier_fee_percent = 6 then 'Yes'  -- tier at 6% of full fee
         when t.tier_fee_percent = 100 then 'No' -- tier at 100% of full fee
         else NULL
       end as indigent_byincomedepend,

       (select top 1 e.Medicaid 
        from tierrpt.dbo.fbh_full_eligibility_piped_wVO e 
        where e.SSN   = format_ssn 
          and format_ssn  not in ('', '000000000')
          and (convert(datetime, convert(char(10), EnrollStart, 112)) <= 
                 convert(datetime, convert(char(10), t.txhist_date_of_serv, 112)) and 
               convert(datetime, convert(char(10), EnrollEnd, 112)) >= 
                 convert(datetime, convert(char(10), t.txhist_date_of_serv, 112)))
        order by convert(datetime, convert(char(10), e.EnrollEnd, 112)) desc) as elig_SSN_Mcaid

into #temp_serv_mcaid_elig1
from #temp_enc_guar_rnd1 t

--select *
--from #temp_serv_mcaid_elig1

-- drop table #temp_serv_mcaid_elig2
-- --------------------------------------------------------------------------------------
-- STEP 8 -- second pass through Medicaid Eligibility file
-- --------------------------------------------------------------------------------------
select txhist_opdocid, patid, txhist_episode, note_type_value, cw_data_entry_date, 
       cw_data_entry_time, cw_data_entry_by,
       txhist_pgm_code, txhist_pgm_value, txhist_serv_code, txhist_serv_value, txhist_duration, note_addresses,
       cw_NOT_uniqueid, txhist_jointo_txhist, ORIG_JOIN_TO_TX_HISTORY,
       txhist_date_of_serv, txhist_serv_start_time, 
       txhist_loc_code, txhist_loc_value, medical_diag_1_icd10_code, medical_diag_1_icd10_value,
       note_dxtype, note_dxrank,

       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, cost_of_service,
       primary_guarantor, units_of_service, aps_link, option_desc, option_id, note_FY, 
       lk_joindx, lk_dxtype, lk_dxrank,
       txhist_prov_id, txhist_prov_name, patient_name, patient_name_last, patient_name_first, 
       patient_sex_code, date_of_birth, FYAge, patient_add_city, patient_add_county_value,
       patient_add_zipcode, patient_ssn, lk_ssn, format_ssn,

       agecategory, credentials, fedpos, 
       vo_cpt_code_conv, enc_type, mcrecovcptcode, dept_name,
       nbr_mcarepayers, JCMH_Mcaidpayer, JCMH_McaidID, exclude_payers, nbr_mcareAdv,
       primary_guar_id, primary_payor, primary_billing_payor, 
       income, nbr_dependents, 
       nbrdep_1, nbrdep_2, nbrdep_3, nbrdep_4, nbrdep_5, fee_percent, 
       tier_income, tier_nbr_dependents,
       tier_nbrdep_1, tier_nbrdep_2, tier_nbrdep_3, tier_nbrdep_4, tier_nbrdep_5, tier_fee_percent,
       indigent_byincomedepend, elig_SSN_Mcaid,

       case 
         when elig_SSN_Mcaid is null 
           then (select top 1 Medicaid
                 from tierrpt.dbo.fbh_full_eligibility_piped_wVO e
                 where e.Medicaid  = t.JCMH_McaidID  -- this will be the medicaid id if there is one
                   --and CoCode in (10, 24, 30)  -- Gilpin, Clear Creek, Jefferson
                   and (convert(datetime, convert(char(10), EnrollStart, 112)) <= 
                          convert(datetime, convert(char(10), t.txhist_date_of_serv, 112)) and 
                        convert(datetime, convert(char(10), EnrollEnd, 112)) >= 
                          convert(datetime, convert(char(10), t.txhist_date_of_serv, 112)))
                 order by convert(datetime, convert(char(10), EnrollEnd, 112)) desc)
         else NULL
       end as elig_mcaid_Mcaid

into #temp_serv_mcaid_elig2
from #temp_serv_mcaid_elig1 t

--select *
--from #temp_serv_mcaid_elig2

-- drop table #temp_serv_mcaid_elig3
-- --------------------------------------------------------------------------------------
-- STEP 9 -- final pass through Medicaid Eligibility file
-- --------------------------------------------------------------------------------------
select txhist_opdocid, patid, txhist_episode, note_type_value, cw_data_entry_date, 
       cw_data_entry_time, cw_data_entry_by,
       txhist_pgm_code, txhist_pgm_value, txhist_serv_code, txhist_serv_value, txhist_duration, note_addresses,
       cw_NOT_uniqueid, txhist_jointo_txhist, ORIG_JOIN_TO_TX_HISTORY,
       txhist_date_of_serv, txhist_serv_start_time, 
       txhist_loc_code, txhist_loc_value, medical_diag_1_icd10_code, medical_diag_1_icd10_value,
       note_dxtype, note_dxrank,

       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, cost_of_service,
       primary_guarantor, units_of_service, aps_link, option_desc, option_id, note_FY, 
       lk_joindx, lk_dxtype, lk_dxrank,
       txhist_prov_id, txhist_prov_name, patient_name, patient_name_last, patient_name_first, 
       patient_sex_code, date_of_birth, FYAge, patient_add_city, patient_add_county_value,
       patient_add_zipcode, patient_ssn, lk_ssn, format_ssn,

       agecategory, credentials, fedpos, 
       vo_cpt_code_conv, enc_type, mcrecovcptcode, dept_name,
       nbr_mcarepayers, JCMH_Mcaidpayer, JCMH_McaidID, exclude_payers, nbr_mcareAdv,
       primary_guar_id, primary_payor, primary_billing_payor, 
       income, nbr_dependents, 
       nbrdep_1, nbrdep_2, nbrdep_3, nbrdep_4, nbrdep_5, fee_percent, 
       tier_income, tier_nbr_dependents,
       tier_nbrdep_1, tier_nbrdep_2, tier_nbrdep_3, tier_nbrdep_4, tier_nbrdep_5, tier_fee_percent,
       indigent_byincomedepend, elig_SSN_Mcaid, elig_mcaid_Mcaid,

       case 
         when elig_SSN_Mcaid is null and elig_mcaid_Mcaid is null
           then (select top 1 e.Medicaid
                 from tierrpt.dbo.fbh_full_eligibility_piped_wVO e 
                 where e.LastName  = t.patient_name_last
                   and (convert(datetime, convert(char(10), e.DOB, 112)) =
                          convert(datetime, convert(char(10), date_of_birth, 112)))
                   and substring(e.firstname,1,1)   = substring(t.patient_name_first,1,1)  
                   and (convert(datetime, convert(char(10), EnrollStart, 112)) <= 
                          convert(datetime, convert(char(10), t.txhist_date_of_serv, 112)) and 
                        convert(datetime, convert(char(10), EnrollEnd, 112)) >= 
                          convert(datetime, convert(char(10), t.txhist_date_of_serv, 112)))
                 order by convert(datetime, convert(char(10), e.EnrollEnd, 112)) desc) 
         else NULL
       end as elig_namedob_Mcaid

into #temp_serv_mcaid_elig3
from #temp_serv_mcaid_elig2 t

--drop table mary_serv_mcaid_elig3
--select *
--from #temp_serv_mcaid_elig3

--drop table #temp_serv_Avatar_CCAR
-- --------------------------------------------------------------------------------------
-- STEP 10 -- Read Avatar for CCAR target status
-- --------------------------------------------------------------------------------------
select txhist_opdocid, patid, txhist_episode, note_type_value, cw_data_entry_date, 
       cw_data_entry_time, cw_data_entry_by,
       txhist_pgm_code, txhist_pgm_value, txhist_serv_code, txhist_serv_value, txhist_duration, note_addresses,
       cw_NOT_uniqueid, txhist_jointo_txhist, ORIG_JOIN_TO_TX_HISTORY,
       txhist_date_of_serv, txhist_serv_start_time, 
       txhist_loc_code, txhist_loc_value, medical_diag_1_icd10_code, medical_diag_1_icd10_value,
       note_dxtype, note_dxrank,

       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, cost_of_service,
       primary_guarantor, units_of_service, aps_link, option_desc, option_id, note_FY, 
       lk_joindx, lk_dxtype, lk_dxrank, 
       txhist_prov_id, txhist_prov_name, patient_name, patient_name_last, patient_name_first, 
       patient_sex_code, date_of_birth, FYAge, patient_add_city, patient_add_county_value,
       patient_add_zipcode, patient_ssn, lk_ssn, format_ssn,

       agecategory, credentials, fedpos, 
       vo_cpt_code_conv, enc_type, mcrecovcptcode, dept_name,
       nbr_mcarepayers, JCMH_Mcaidpayer, JCMH_Mcaidid, exclude_payers, nbr_mcareAdv,
       primary_guar_id, primary_payor, primary_billing_payor, 
       income, nbr_dependents, 
       nbrdep_1, nbrdep_2, nbrdep_3, nbrdep_4, nbrdep_5, fee_percent, 
       tier_income, tier_nbr_dependents,
       tier_nbrdep_1, tier_nbrdep_2, tier_nbrdep_3, tier_nbrdep_4, tier_nbrdep_5, tier_fee_percent,
       indigent_byincomedepend, elig_SSN_Mcaid, elig_mcaid_Mcaid, elig_namedob_Mcaid,

       -- put the 3 state medicaid elig ids together,  MLS 3/23/2017
       case
         when elig_ssn_mcaid is not null then elig_SSN_Mcaid
         when elig_namedob_Mcaid is not null then elig_namedob_Mcaid
         when elig_mcaid_Mcaid is not null then elig_mcaid_Mcaid
         else NULL
       end as Mcaid_Elig_State,

--       -- look up CPT Code based on duration 
       case
          when (select top 1 [From Minutes]  -- cpt code doesn't have a time breakdown
                from Feeds.dbo.JC_A_TL_LK_Enc_ServCodes fi
                where fi.[service code]   =  t.txhist_serv_code)  is NULL 
            then (select top 1 fi.[cpt code]
                  from Feeds.dbo.JC_A_TL_LK_Enc_ServCodes fi
                  where fi.[service code]  =  t.txhist_serv_code )
          when (select top 1 fi.[cpt code]  -- cpt code does have a time breakdown
                from Feeds.dbo.JC_A_TL_LK_Enc_ServCodes fi
                where fi.[service code]  =  t.txhist_serv_code
                  and (t.txhist_duration >= fi.[From Minutes]
                  and t.txhist_duration <= fi.[To Minutes])) is not null
            then (select top 1 fi.[cpt code]
                  from Feeds.dbo.JC_A_TL_LK_Enc_ServCodes fi
                  where fi.[service code]  =  t.txhist_serv_code 
                    and (t.txhist_duration >= fi.[From Minutes]
                    and t.txhist_duration <= fi.[To Minutes])) 
          else (select top 1 fi.[cpt code]  -- cpt code's duration is below the minimum minutes
                from Feeds.dbo.JC_A_TL_LK_Enc_ServCodes fi
                where fi.[service code]  =  t.txhist_serv_code )
       end as cpt_code,

--       -- look up VO CPT Code based on duration 
       case
          when (select top 1 [From Minutes]  -- cpt code doesn't have a time breakdown
                from Feeds.dbo.JC_A_TL_LK_Enc_ServCodes fi
                where fi.[service code]   =  t.txhist_serv_code ) is NULL 
            then (select top 1 fi.[vo cpt code]
                  from Feeds.dbo.JC_A_TL_LK_Enc_ServCodes fi
                  where fi.[service code] =  t.txhist_serv_code )
          when (select top 1 fi.[vo cpt code] -- cpt code does have a time breakdown
                from Feeds.dbo.JC_A_TL_LK_Enc_ServCodes fi
                where fi.[service code]  =  t.txhist_serv_code
                  and (t.txhist_duration >= fi.[From Minutes]
                  and t.txhist_duration <= fi.[To Minutes])) is not null
            then (select top 1 fi.[vo cpt code]
                  from Feeds.dbo.JC_A_TL_LK_Enc_ServCodes fi
                  where fi.[service code]  =  t.txhist_serv_code
                    and (t.txhist_duration >= fi.[From Minutes]
                    and t.txhist_duration <= fi.[To Minutes])) 
          else (select top 1 fi.[vo cpt code]  -- cpt code's duration is below the minimum minutes
                from Feeds.dbo.JC_A_TL_LK_Enc_ServCodes fi
                where fi.[service code]  =  t.txhist_serv_code )
       end as vo_cpt_code,

      -- was the service during an admission episode
      case
        when (select top 1 eh.patid
              from avatardw.system.episode_history eh
              where eh.patid = t.patid
                and CAST(CONVERT(char(8), t.txhist_date_of_serv, 112) AS DATETIME) >= 
                                                      CAST(CONVERT(char(8), eh.preadmit_admission_date, 112) AS DATETIME)
                and (CAST(CONVERT(char(8), t.txhist_date_of_serv, 112) AS DATETIME) <= 
                                                       CAST(CONVERT(char(8), eh.date_of_discharge, 112) AS DATETIME) 
                 or date_of_discharge is null)
                and program_value = 'Jefferson Center for Mental Health'  
              order by eh.EPN_uniqueid desc) is not null
        then 'Y'
        else 'N'
      end as 'IsEnrolled',

       ccar.C000_Assessment_Date, ccar.Target_Status, ccar.Target_Status_Value, ccar.C161_Special_Studies_2_Value as av_spec2, 
       ccar.Draft_Final as av_ccar_draft_final, ccar.C134_Meds_Only_Client_Value as av_meds_only

into #temp_serv_Avatar_CCAR
from #temp_serv_mcaid_elig3 t

   -- added code to ignore initial TIER import record done by Peter and look for the finaled CCAR -- 6/2/2017, MLS
   outer apply (select top 1 cc.C000_Assessment_Date, cc.Target_Status, cc.Target_Status_Value, 
                             cc.C161_Special_Studies_2_Value, cc.Draft_Final, cc.C134_Meds_Only_Client_Value
                from AvatarDW.[SYSTEM].CCAR_Colorado_Client_Assessm cc
                where cc.patid = t.patid
                  and (option_desc <> 'CCAR Import' or data_entry_user_id <> 'PDUNLAP')  -- do not look at initial TIER import to Avatar
                  and Draft_Final = 'F'
                  --and t.date_of_service >= cc.C000_Assessment_Date
                  -- look for the CCAR created during the fiscal year of the note
                  and ((cc.C000_Assessment_Date >= (select fy.begindate
                                                    from jcmh.dbo.JCMH__FiscalYear fy
                                                    where fy.fiscalyear = note_FY)) and
                       (cc.C000_Assessment_Date <= (select fy.enddate
                                                    from jcmh.dbo.JCMH__FiscalYear fy
                                                    where fy.fiscalyear = note_FY)))
                --order by c000_assessment_date desc, c123_action_type desc) ccar  -- added action type sort
                order by c000_assessment_date desc, data_entry_date desc, data_entry_time desc) ccar  -- get most recent for CCAR found

--
--select *
--from #temp_serv_Avatar_CCAR

--drop table #temp_serv_TIER_CCAR
-- --------------------------------------------------------------------------------------
-- STEP 11 -- Read TIER for CCAR target status and other fields
-- --------------------------------------------------------------------------------------
select txhist_opdocid, patid, txhist_episode, note_type_value, cw_data_entry_date, 
       cw_data_entry_time, cw_data_entry_by,
       txhist_pgm_code, txhist_pgm_value, txhist_serv_code, txhist_serv_value, txhist_duration, note_addresses,
       cw_NOT_uniqueid, txhist_jointo_txhist, ORIG_JOIN_TO_TX_HISTORY,
       txhist_date_of_serv, txhist_serv_start_time, 
       txhist_loc_code, txhist_loc_value, medical_diag_1_icd10_code, medical_diag_1_icd10_value,
       note_dxtype, note_dxrank,

       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, cost_of_service,
       primary_guarantor, units_of_service, aps_link, option_desc, option_id, note_FY, 
       lk_joindx, lk_dxtype, lk_dxrank,
       txhist_prov_id, txhist_prov_name, patient_name, patient_name_last, patient_name_first, 
       patient_sex_code, date_of_birth, FYAge, patient_add_city, patient_add_county_value,
       patient_add_zipcode, patient_ssn, lk_ssn, format_ssn,

       agecategory, credentials, fedpos,
       vo_cpt_code_conv, enc_type, mcrecovcptcode, dept_name,
       nbr_mcarepayers, JCMH_Mcaidpayer, JCMH_Mcaidid, exclude_payers, nbr_mcareAdv,
       primary_guar_id, primary_payor, primary_billing_payor, 
       income, nbr_dependents,
       nbrdep_1, nbrdep_2, nbrdep_3, nbrdep_4, nbrdep_5, fee_percent, 
       tier_income, tier_nbr_dependents,
       tier_nbrdep_1, tier_nbrdep_2, tier_nbrdep_3, tier_nbrdep_4, tier_nbrdep_5, tier_fee_percent,
       indigent_byincomedepend, elig_SSN_Mcaid, elig_mcaid_Mcaid, elig_namedob_Mcaid, Mcaid_Elig_State, IsEnrolled,

       C000_Assessment_Date, Target_Status, Target_Status_Value, av_spec2, av_meds_only, av_ccar_draft_final,
       cpt_code, vo_cpt_code,

      -- find the type of service (enc, hour, 15min, 30min, day)
      (select top 1 Unit_Calc_Type
       from feeds.dbo.JC_A_TL_LK_Enc_CPTCode_Rates rt
       where rt.VO_CPT_Code = t.vo_cpt_code) as unit_calc_type,

     -- get the policy number of the payer on the note
      (select top 1 gu.subs_policy
       from avatardw.system.billing_guar_subs_data gu
       where gu.patid = t.patid
         and gu.guarantor_id = t.primary_guar_id) as Policy_Num,

      -- get payer type for the guarantor
      (select top 1 financial_class_value
       from AvatarDW.system.billing_guar_table gu
       where gu.GUARANTOR_ID = t.primary_guar_id) as payertype,

      -- get cost class from lookup
      (select top 1 [Cost Class]
       from Feeds.dbo.JC_A_TL_LK_Enc_ServCodes fi
       where fi.[service code]  =  t.txhist_serv_code ) as CostClass,

--      -- get billable credential 
--      (select top 1 practitioner_category_value
--       from AvatarDW.system.staff_current_demographics st
--       where st.STAFFID = t.txhist_prov_id) as [credential],

       tier.sed, tier.spmi, tier.smi, 

       -- need to get TIER effective date because the initial import CCAR from Avatar will be ignored 
       (select top 1 tcc.Meds  
        from tier.dbo.FD__CCAR2006 tcc
        where tcc.clientid = t.patid
          and t.txhist_date_of_serv >= tcc.effdate
        order by tcc.effdate desc) as tiermeds,

       -- need to get TIER effective date because the initial import CCAR from Avatar will be ignored 
       (select top 1 tcc.EffDate  
        from tier.dbo.FD__CCAR2006 tcc
        where tcc.clientid = t.patid
          and t.txhist_date_of_serv >= tcc.effdate
        order by tcc.effdate desc) as TIEReffdate


into #temp_serv_TIER_CCAR
from #temp_serv_Avatar_CCAR t

   outer apply (select tr.sed, tr.spmi, tr.smi
                from tier.dbo.tlv_ccar_sed2006 tr
                where tr.ccarid = (select top 1 cct.op__docid
                                   from tier.dbo.FD__CCAR2006 cct
                                   where cct.clientid = t.patid
                                     and t.txhist_date_of_serv >= cct.effdate
                                   order by cct.effdate desc)) tier


--select *
--from #temp_serv_TIER_CCAR

-- drop table #temp_serv_more_fields
-- --------------------------------------------------------------------------------------
-- STEP 12 -- yet more fields
-- --------------------------------------------------------------------------------------
select txhist_opdocid, t.patid, txhist_episode, note_type_value, cw_data_entry_date, 
       cw_data_entry_time, cw_data_entry_by,
       txhist_pgm_code, txhist_pgm_value, txhist_serv_code, txhist_serv_value, txhist_duration, note_addresses,
       cw_NOT_uniqueid, txhist_jointo_txhist, ORIG_JOIN_TO_TX_HISTORY,
       txhist_date_of_serv, txhist_serv_start_time, 
       txhist_loc_code, txhist_loc_value, medical_diag_1_icd10_code, medical_diag_1_icd10_value,
       note_dxtype, note_dxrank,

       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, cost_of_service,
       primary_guarantor, units_of_service, aps_link, option_desc, option_id, note_FY, 
       lk_joindx, lk_dxtype, lk_dxrank,
       txhist_prov_id, txhist_prov_name, patient_name, patient_name_last, patient_name_first, 
       patient_sex_code, date_of_birth, FYAge, patient_add_city, patient_add_county_value,
       patient_add_zipcode, patient_ssn, lk_ssn, format_ssn,

       agecategory, credentials, fedpos,
       vo_cpt_code_conv, enc_type, mcrecovcptcode, dept_name,
       nbr_mcarepayers, JCMH_Mcaidpayer, JCMH_Mcaidid, exclude_payers, nbr_mcareAdv,
       primary_guar_id, primary_payor, primary_billing_payor, 
       income, nbr_dependents,
       nbrdep_1, nbrdep_2, nbrdep_3, nbrdep_4, nbrdep_5, fee_percent, 
       tier_income, tier_nbr_dependents,
       tier_nbrdep_1, tier_nbrdep_2, tier_nbrdep_3, tier_nbrdep_4, tier_nbrdep_5, tier_fee_percent,
       indigent_byincomedepend, elig_SSN_Mcaid, elig_mcaid_Mcaid, elig_namedob_Mcaid, Mcaid_Elig_State, IsEnrolled,

       C000_Assessment_Date, Target_Status, Target_Status_Value, av_spec2,  av_meds_only, av_ccar_draft_final,
       cpt_code, vo_cpt_code, unit_calc_type,
       Policy_Num, payertype, CostClass, 

      (select top 1 unitcost 
       from tier.dbo.blu_unitcost
	   where costclass = t.costclass) as 'UnitCost',

       --- need to add calc units -- when minutes exceed the lookup tables
       case
         when unit_calc_type = 'hour' 
           then (select top 1 vou.units
                 from  Feeds.dbo.JC_A_TL_LK_Enc_CPTCode_Units_60min vou
                 where t.txhist_duration >= vou.FromMinutes
                   and t.txhist_duration <= vou.ToMinutes)                 
         when unit_calc_type = '15min'
           then (select top 1 vou.units
                 from  Feeds.dbo.JC_A_TL_LK_Enc_CPTCode_Units vou
                 where t.txhist_duration >= vou.FromMinutes
                   and t.txhist_duration <= vou.ToMinutes)   
         when unit_calc_type = '30min'
           then (select top 1 vou.units
                 from  Feeds.dbo.JC_A_TL_LK_Enc_CPTCode_Units_30min vou
                 where t.txhist_duration >= vou.FromMinutes
                   and t.txhist_duration <= vou.ToMinutes) 
         when unit_calc_type in ('enc','day') then 1
         when t.vo_cpt_code in ('82947QW', '80061QW','J0401','J1630','J1631','J1942','J2315','J2426','J2680','J2794','J3490') -- injectables
                   then 1
         else NULL
       end as calc_units_1stlook,

       smi, spmi, sed, tiermeds, TIEReffdate,

       -- determine the fiscal year for the avatar CCAR -- 6/2017, MLS
       (select fy.FiscalYear
        from jcmh.dbo.JCMH__FiscalYear fy
        where t.C000_Assessment_Date >= fy.BeginDate and t.C000_Assessment_Date <= fy.EndDate) as av_CCAR_FY,

          -- determine the fiscal year for the TIER CCAR -- 6/2017, MLS
       (select fy.FiscalYear
        from jcmh.dbo.JCMH__FiscalYear fy
        where t.TIEReffdate >= fy.BeginDate and t.TIEReffdate <= fy.EndDate) as tier_CCAR_FY,

       -- add RVU to table,  MLS 3/23/2017
       (select RVUISFacility
        from jcmh.dbo.JCMH_A_TL_LK_Sev_RVUFacilities rv
        where rv.location_code = t.txhist_loc_code) as RVUIsFacility,

        episode.patid as ep_patid, episode.program_value as episode_program_value, 
        episode.program_code as episode_program_code, 
        episode.preadmit_admission_date as admit_date, episode.date_of_discharge as discharge_date


into #temp_serv_more_fields
from #temp_serv_TIER_CCAR t

                -- admission & discharge dates and type of admission program - MLS, 10/23/2017
   outer apply (select top 1 eph.PATID, eph.episode_number, eph.program_value, eph.program_code, 
                       eph.preadmit_admission_date, eph.date_of_discharge
                from avatardw.SYSTEM.episode_history eph
                where eph.patid = t.patid
                  and (convert(datetime, t.txhist_date_of_serv) >= convert(datetime,eph.preadmit_admission_date) and
                       (convert(datetime, t.txhist_date_of_serv) <= convert(datetime,eph.date_of_discharge) or 
                        eph.date_of_discharge is null))
                order by eph.patid, eph.episode_number desc) episode

--select *
--from #temp_serv_more_fields
----order by convert(int,patid)
--


--drop table #temp_serv_calc_units  
-- --------------------------------------------------------------------------------------
-- STEP 13 -- calculate units and current care coordinator and program
-- --------------------------------------------------------------------------------------
select txhist_opdocid, patid, txhist_episode, note_type_value, cw_data_entry_date, 
       cw_data_entry_time, cw_data_entry_by,
       txhist_pgm_code, txhist_pgm_value, txhist_serv_code, txhist_serv_value, txhist_duration, note_addresses,
       cw_NOT_uniqueid, txhist_jointo_txhist, ORIG_JOIN_TO_TX_HISTORY,
       txhist_date_of_serv, txhist_serv_start_time, 
       txhist_loc_code, txhist_loc_value, medical_diag_1_icd10_code, medical_diag_1_icd10_value,
       note_dxtype, note_dxrank,

       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, cost_of_service,
       primary_guarantor, units_of_service, aps_link, option_desc, option_id, note_FY, 
       lk_joindx, lk_dxtype, lk_dxrank, 
       txhist_prov_id, txhist_prov_name, patient_name, patient_name_last, patient_name_first, 
       patient_sex_code, date_of_birth, FYAge, patient_add_city, patient_add_county_value,
       patient_add_zipcode, patient_ssn, lk_ssn, format_ssn,

       agecategory, credentials, fedpos,  
       vo_cpt_code_conv, enc_type, mcrecovcptcode, dept_name,
       nbr_mcarepayers, JCMH_Mcaidpayer, JCMH_Mcaidid, exclude_payers, nbr_mcareAdv,
       primary_guar_id, primary_payor, primary_billing_payor, 
       income, nbr_dependents,
       nbrdep_1, nbrdep_2, nbrdep_3, nbrdep_4, nbrdep_5, fee_percent, 
       tier_income, tier_nbr_dependents,
       tier_nbrdep_1, tier_nbrdep_2, tier_nbrdep_3, tier_nbrdep_4, tier_nbrdep_5, tier_fee_percent,
       indigent_byincomedepend, elig_SSN_Mcaid, elig_mcaid_Mcaid, elig_namedob_Mcaid, Mcaid_Elig_State, IsEnrolled,

       C000_Assessment_Date, Target_Status, Target_Status_Value, av_spec2, av_meds_only, av_ccar_draft_final,
       cpt_code, vo_cpt_code, 
       Policy_Num, payertype, CostClass,
       UnitCost, unit_calc_type,
       smi, spmi, sed, tiermeds, TIEReffdate, av_CCAR_FY, tier_CCAR_FY, RVUIsFacility,
       calc_units_1stlook,

       -- when the service duration exceed the look tables, calc the units  ==== NEED TO TEST
       case
         when t.calc_units_1stlook is null 
           then (case
                   when  t.enc_type = 'hour' then convert(int,(floor(t.txhist_duration / 60) + .5))
                   when  t.enc_type = '15min' then convert(int,(floor(t.txhist_duration / 15) + .5))
                   when  t.enc_type = '30min' then convert(int,(floor(t.txhist_duration / 30) + .5))
                   else calc_units_1stlook  
                 end)
         else t.calc_units_1stlook
       end as calc_units_2ndlook,

       ep_patid, episode_program_value, episode_program_code, 
       admit_date, discharge_date,

       casemgmt.current_program, casemgmt.current_care_coord
       

into #temp_serv_calc_units
from #temp_serv_more_fields t

                -- current program and care coordinator at time of run - MLS, 10/23/2017
   outer apply (select c.care_coordin_program_value as current_program, 
                       c.care_coordin_ADD_Value as current_care_coord
                from avatardw.CWSSYSTEM.caseload_management c
                where c.EPISODE_NUMBER = (SELECT top 1 b.EPISODE_NUMBER -- current episode
                                          FROM avatardw.SYSTEM.view_episode_summary_current b 
                                          WHERE b.PATID = c.patid
                                            and b.program_code = 7000  -- Jefferson Center episode only
                                          order by episode_number desc)
                  and c.ID = (select max(c2.[ID])  -- most current caseload entry
                              from avatardw.CWSSYSTEM.caseload_management c2 
                              WHERE c2.PATID = c.patid
                                and c2.option_id = 'USER13'    --'Caseload Management (Add to Caseload)'
                                and (c2.care_coordin_add <> '' or
                                     c2.care_coordin_program <> '')) -- an add must have something to add
                  and c.option_id = 'USER13'  --'Caseload Management (Add to Caseload)'  --USER13
                  and c.patid in (select vw.patid  -- only if client is currently open to center
                                  from avatardw.SYSTEM.view_episode_summary_current vw
                                  where vw.patid = c.patid)
                  and c.care_coordin_add_caseloadid not in (select c3.care_coordin_remove_caseloadid  -- doesn't have a remove for current episode
                                                            from avatardw.CWSSYSTEM.caseload_management c3
                                                            where c3.option_id = 'USER13')  --'Caseload Management (Remove from Caseload)')
                  and c.patid = t.patid) casemgmt


--
--select *
--from #temp_serv_calc_units
----order by convert(int,patid)


--drop table #temp_round1_format
-- --------------------------------------------------------------------------------------------------------------------------
--  STEP 14 - first round of formatting the data
-- --------------------------------------------------------------------------------------------------------------------------
select txhist_opdocid, patid, txhist_episode, note_type_value, 
       cw_data_entry_date as data_entry_date, 
       cw_data_entry_time, cw_data_entry_by as data_entry_by,
       txhist_pgm_value as service_program_value, txhist_pgm_code as service_program_code, dept_name, 
       txhist_serv_value as service_charge_value, txhist_serv_code as service_charge_code, txhist_duration as duration,
       note_addresses, cw_NOT_uniqueid as NOT_Uniqueid, txhist_jointo_txhist as join_to_tx_history, ORIG_JOIN_TO_TX_HISTORY,
       txhist_date_of_serv as date_of_service, 

       convert(varchar(10), txhist_date_of_serv,112)  as 'DateSvc_ymd',
       convert(varchar(4), DATEPART(year, txhist_date_of_serv)) as 'DateSvc_yyyy',
       case 
         when datepart(month, txhist_date_of_serv) = 1 then '01'
         when datepart(month, txhist_date_of_serv) = 2 then '02'                                                                  
         when datepart(month, txhist_date_of_serv) = 3 then '03'
         when datepart(month, txhist_date_of_serv) = 4 then '04'
         when datepart(month, txhist_date_of_serv) = 5 then '05'
         when datepart(month, txhist_date_of_serv) = 6 then '06'
         when datepart(month, txhist_date_of_serv) = 7 then '07'
         when datepart(month, txhist_date_of_serv) = 8 then '08'
         when datepart(month, txhist_date_of_serv) = 9 then '09'
         when datepart(month, txhist_date_of_serv) = 10 then '10'
         when datepart(month, txhist_date_of_serv) = 11 then '11'
         when datepart(month, txhist_date_of_serv) = 12 then '12'
       end as 'DateSvc_mm',

       txhist_serv_start_time as service_start_time,

       -- get start time for same date of note
       (select top 1 aa.appointment_start_time
        from avatardw.[SYSTEM].appt_data aa
        where aa.patid = t.patid
          and aa.appointment_date = t.txhist_date_of_serv) as appt_time,

       convert(varchar(10), cw_data_entry_date, 112)  as 'FinalSave_ymd',
       convert(varchar(4), DATEPART(year, cw_data_entry_date)) as 'FinalSave_yyyy',
       case 
         when datepart(month, cw_data_entry_date) = 1 then '01'
         when datepart(month, cw_data_entry_date) = 2 then '02'
         when datepart(month, cw_data_entry_date) = 3 then '03'
         when datepart(month, cw_data_entry_date) = 4 then '04'
         when datepart(month, cw_data_entry_date) = 5 then '05'
         when datepart(month, cw_data_entry_date) = 6 then '06'
         when datepart(month, cw_data_entry_date) = 7 then '07'
         when datepart(month, cw_data_entry_date) = 8 then '08'
         when datepart(month, cw_data_entry_date) = 9 then '09'
         when datepart(month, cw_data_entry_date) = 10 then '10'
         when datepart(month, cw_data_entry_date) = 11 then '11'
         when datepart(month, cw_data_entry_date) = 12 then '12'
       end as 'FinalSave_mm',

       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, primary_guarantor, aps_link,


       txhist_loc_code as location_code, txhist_loc_value as location_value, 
       medical_diag_1_icd10_code as Diag_On_Note_code, 
       medical_diag_1_icd10_value as Diag_On_Note_value, 
       note_dxtype as Diag_On_Note_DxType, 
       note_dxrank as Diag_On_Note_DxRank,
       
       lk_joindx, lk_dxtype, lk_dxrank,

       -- added per Ann Jones, 11/16/2017
       (select top 1 dc.diagnosis_value
        from avatardw.system.client_diagnosis_codes dc
        where dc.patid = patid
          and diagnosis_code = lk_joindx
          and code_set_code = 'ICD10') as lk_joindx_value,

       -- take diagnosis on note first for primary diagnosis, if not there take primary diagnosis
       case
         when medical_diag_1_icd10_code <> ' ' then medical_diag_1_icd10_code
         else lk_joindx
       end as PrimDiagKey_fin,

       txhist_prov_id as practitioner_id, txhist_prov_name as practitioner_name, 
       patient_name_last, patient_name_first, patient_sex_code, date_of_birth, FYAge, AgeCategory,
       patient_add_city, patient_add_county_value, patient_add_zipcode, format_ssn, patient_ssn, lk_ssn,

       cost_of_service, credentials,
       FEDPOS, cpt_code, vo_cpt_code, vo_cpt_code_conv, enc_type, 

       -- for cpt code of 99404 or service charge code of 201, need to translate to coding manual cpt code  
       --                       99404 has been added to JC_A_TL_LK_Enc_CPTCode_Rates as an 'enc', even though code is not in manual
       case
         when txhist_serv_code = '201' and (txhist_duration >= 16 and txhist_duration <= 37)
           then '90832' 
         when txhist_serv_code = '201' and (txhist_duration >= 38 and txhist_duration <= 52)
           then '90834'
         when txhist_serv_code = '201' and (txhist_duration >= 53)
           then '90837'
         when vo_cpt_code in ('90832','90834','90837') and credentials = 'BACHELORS'  -- Lic Intakes, bachelors **** credentials?
           then 'H0004'
         -- for ICCS location (fedpos = 14) and EM CPT Codes -- new as of 4/10/2017
         when txhist_loc_code = '57' and vo_cpt_code = '99201' then '99324'
         when txhist_loc_code = '57' and vo_cpt_code = '99202' then '99325'
         when txhist_loc_code = '57' and vo_cpt_code = '99203' then '99326'
         when txhist_loc_code = '57' and vo_cpt_code = '99204' then '99327'
         when txhist_loc_code = '57' and vo_cpt_code = '99205' then '99328'
         when txhist_loc_code = '57' and vo_cpt_code = '99212' then '99334'
         when txhist_loc_code = '57' and vo_cpt_code = '99213' then '99335'
         when txhist_loc_code = '57' and vo_cpt_code = '99214' then '99336'
         when txhist_loc_code = '57' and vo_cpt_code = '99215' then '99337'
         -- for nursing home facilities
         when vo_cpt_code = '99201' and fedpos = '31' then '99304'
         when vo_cpt_code = '99202' and fedpos = '31' then '99305'
         when vo_cpt_code = '99203' and fedpos = '31' then '99306'
         when vo_cpt_code = '99212' and fedpos = '31' then '99307'
         when vo_cpt_code = '99213' and fedpos = '31' then '99308'
         when vo_cpt_code = '99214' and fedpos = '31' then '99309'
         when vo_cpt_code = '99215' and fedpos = '31' then '99310'
         -- for residential facilties
         when vo_cpt_code = '99201' and fedpos = '13' then '99324'
         when vo_cpt_code = '99202' and fedpos = '13' then '99325'
         when vo_cpt_code = '99203' and fedpos = '13' then '99326'
         when vo_cpt_code = '99212' and fedpos = '13' then '99334'
         when vo_cpt_code = '99213' and fedpos = '13' then '99335'
         when vo_cpt_code = '99214' and fedpos = '13' then '99336'
         when vo_cpt_code= '99215' and fedpos = '13' then '99337'
         -- else note's cpt code       
         else vo_cpt_code
       end as vo_cpt_code_fin,

       --JCMH_McaidID  -- missing
       primary_guar_id, primary_payor as primary_payor_note, primary_billing_payor as primary_payor_guar_order, 
       elig_SSN_Mcaid, elig_mcaid_Mcaid, elig_namedob_Mcaid, Mcaid_elig_state,
       Policy_Num, CostClass, PayerType, UnitCost,

       case
         when t.calc_units_1stlook is null then t.calc_units_2ndlook
         else t.calc_units_1stlook
       end as calc_units,

       t.calc_units_2ndlook, t.calc_units_1stlook,

       case
         when tiermeds = 'T' or av_meds_only = 'Yes' then 'Meds'
         else NULL
       end as CCARType,

       income, nbr_dependents,
       nbrdep_1, nbrdep_2, nbrdep_3, nbrdep_4, nbrdep_5, fee_percent, 
       tier_income, tier_nbr_dependents,
       tier_nbrdep_1, tier_nbrdep_2, tier_nbrdep_3, tier_nbrdep_4, tier_nbrdep_5, tier_fee_percent,
       indigent_byincomedepend, IsEnrolled,

       C000_Assessment_Date, Target_Status, Target_status_value, av_meds_only, av_ccar_draft_final,
       SED, SMI, SPMI, 
       isnull(SED,'F') as TIERsed, isnull(SMI,'F') as TIERsmi, isnull(SPMI,'F') as TIERspmi,
       tiermeds, TIEReffdate,

       case
         when t.C000_Assessment_Date >= '2016-10-01' then t.C000_Assessment_Date -- avatar assessment date
         else t.TIEReffdate  -- tier effective date
       end as CCAR_Assessment_Date,

       -- look at both avatar assessment and tier effective dates to set FYCCARInEffect
       case
         when (t.C000_Assessment_Date is not null) and (note_FY = av_CCAR_FY)  -- for the same year
           then 'Yes'
         when (t.TIEReffdate is not null) and (note_FY = tier_CCAR_FY)  -- for the same year
           then 'Yes'
         else 'No'
       end as FYCCARInEffect,

       option_desc, option_id, 
       note_FY, av_CCAR_FY, tier_CCAR_FY,

       unit_calc_type, --calc_units
       RVUIsFacility,

-- lookup non-faciltiy RVU and facility RVU
       case
         when RVUIsFacility = 'Y' -- RVU Facility - building owned by Jefferson Center
           then (select top 1 sc.state_rp_code_2
                 from avatardw.SYSTEM.billing_tx_master_fee_table sc --billing_tx_master_fee_audit sc  -- changed 7/26/2017, MLS
                 where sc.service_code = t.txhist_serv_code
                   and sc.cpt_code = t.cpt_code  -- add cpt code to the lookup, RVU may differ based on duration  -- 7/28/2017, MLS
--                   and cast(convert(char(8), sc.data_entry_date, 112) as datetime) <=
--                         cast(convert(char(8), t.date_of_service, 112) as datetime)
                 order by sc.data_entry_date desc)
         when RVUIsFacility = 'N'  -- Not RVU Facility
           then (select top 1 sc.state_rp_code_1
                 from avatardw.SYSTEM.billing_tx_master_fee_table sc --billing_tx_master_fee_audit sc  -- changed 7/26/2017, MLS
                 where sc.service_code = t.txhist_serv_code
                   and sc.cpt_code = t.cpt_code  -- add cpt code to the lookup, RVU may differ based on duration  -- 7/28/2017, MLS
--                   and cast(convert(char(8), sc.data_entry_date, 112) as datetime) <=
--                         cast(convert(char(8), t.date_of_service, 112) as datetime)
                 order by sc.data_entry_date desc)
         else NULL
       end as Base_RVU,

       mcrecovcptcode, nbr_mcarepayers, JCMH_Mcaidpayer, JCMH_Mcaidid, t.exclude_payers as nbr_exclude_payers, nbr_mcareAdv, 
       t.nbr_mcarepayers as nbr_mcare_supp, t.mcrecovcptcode as proc_cov_mcare,
       t.primary_billing_payor as primary_payor_indigent,  -- using date of service and guarantor order, get indigent payer

       t.current_program, t.current_care_coord,

       episode_program_value, episode_program_code, admit_date, discharge_date


       
into #temp_round1_format
from #temp_serv_calc_units t
----
--select *
--from  #temp_round1_format


--drop table #temp_round2_format
-- --------------------------------------------------------------------------------------------------------------------------
--  STEP 15 - second round of formatting the data
-- --------------------------------------------------------------------------------------------------------------------------
select txhist_opdocid, patid, txhist_episode, note_type_value, data_entry_date, 
       cw_data_entry_time, data_entry_by, 
       service_program_value, service_program_code, dept_name, 
       service_charge_value, service_charge_code, duration,
       note_addresses, NOT_Uniqueid, join_to_tx_history, ORIG_JOIN_TO_TX_HISTORY,
       date_of_service, datesvc_ymd, datesvc_yyyy, datesvc_mm, service_start_time, appt_time,

       -- for missing time, look at start time of appointment  -- if no appointment, set time to noon (1200)
       --        and convert time to military time without semi-colons 
       case
         when t.service_start_time = ' ' and t.appt_time is not null
           --converted time from 12 to 24 hour format - ex. 4:00PM to 1600
           then t.appt_time
         when t.appt_time is null and t.service_start_time = ' '
           then '12:00 PM'
         else t.service_start_time
       end as lk_time_start,

       FinalSave_ymd, FinalSave_yyyy, FinalSave_mm, 
       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, 
       primary_guarantor, aps_link,
       location_code, location_value, FEDPOS, Credentials, Diag_on_note_code, Diag_on_note_value, 
       Diag_On_Note_DxType, Diag_On_Note_DxRank,
       lk_joindx, lk_dxtype, lk_dxrank, lk_joindx_value, primdiagkey_fin, 

       case
          when t.PrimDiagKey_fin is NULL or t.PrimDiagKey_fin = 'NOT FOUND' then 'R69'
          else t.PrimDiagKey_fin
       end as PrimDiagKey_fin2,

       practitioner_id, practitioner_name, 
       patient_name_last, patient_name_first, patient_sex_code, patient_add_city, patient_add_county_value, patient_add_zipcode,
       date_of_birth, fyage, agecategory, 
       format_ssn, patient_ssn, lk_ssn, cost_of_service, enc_type,       
       cpt_code, vo_cpt_code, vo_cpt_code_conv, vo_cpt_code_fin, 
       primary_guar_id, primary_payor_note, primary_payor_guar_order, 
       elig_SSN_Mcaid, elig_mcaid_Mcaid, elig_namedob_Mcaid, Mcaid_elig_state, 
       policy_num, costclass, payertype, unitcost, 
       calc_units, calc_units_2ndlook, calc_units_1stlook,

       CCARType, income,nbr_dependents, nbrdep_1, nbrdep_2, nbrdep_3, nbrdep_4, nbrdep_5, fee_percent, 
       tier_income, tier_nbr_dependents,
       tier_nbrdep_1, tier_nbrdep_2, tier_nbrdep_3, tier_nbrdep_4, tier_nbrdep_5, tier_fee_percent,
       indigent_byincomedepend, IsEnrolled,

       C000_Assessment_Date, target_status, target_status_value, av_meds_only, av_ccar_draft_final,
       TIEReffdate, tiermeds, TIERsed, TIERsmi, TIERspmi, 
       FYCCARInEffect, note_FY, av_CCAR_FY, tier_CCAR_FY, CCAR_Assessment_Date,
       option_desc, option_id, 
       RVUIsFacility, Base_RVU,

       mcrecovcptcode, nbr_mcarepayers, JCMH_Mcaidpayer, JCMH_Mcaidid, nbr_exclude_payers, nbr_mcareAdv, 
       nbr_mcare_supp, proc_cov_mcare,
       primary_payor_indigent,  -- using date of service and guarantor order, get indigent payer


       -- calculate indigent2
       case 
         when CHARINDEX('CC-', service_program_value) > 0 or CHARINDEX('PS-', service_program_value) > 0 
           then NULL  -- skip pre-screening or community contact programs, which will include CCC-WIC & CCC_MCT  
         when JCMH_Mcaidpayer > 0 or Mcaid_Elig_State is not null  -- has medicaid
           then NULL
         when nbr_mcare_supp > 0   -- has medicare supplemental payer
           then NULL
         when nbr_exclude_payers > 0  -- skip due to payer
           then NULL
         when proc_cov_mcare = 'Y'  -- skip covered medicare services
              -- primary_payor_indigent = 'Medicare', 'Medicare Railroad', 'AARP - Medicare Complete',
              --                               'Kaiser Medicare','Humana Medicare'
              and primary_payor_indigent in (1069,1083,1104,1107,2002)  -- change guarantor check from name to ID  
           then NULL
         when FYAge > 11  -- not kids, but adolescent, adults and older adults
              --and (ccaronfile = '1'  -- has a CCAR on date of service
              and (FYCCARInEffect = 'Yes'  -- during the past year, a CCAR was in effect  -- 6/14/2017, MLS
              --and income < 33510
              and indigent_byincomedepend = 'Yes'  -- based on income / nbr of dependents, client may be indigent 
--                primary_payor_indigent = 'Self Pay','Self Pay Sliding Fee 6%','Medicare','Medicare - Railroad',
--                                         'AARP - Medicare Complete','Kaiser Medicare','Humana Medicare',
--                                         'Evercare/UBH Claims','Total Long Term Care'
               and primary_payor_indigent in (1027,1105,1069,1083,1104,1107,2002,1039,1094) -- change guarantor check from name to ID  
                     --or nbr_mcareAdv > 0  -- doesn't quite work, aded payers to above list
               and ((CCAR_Assessment_Date >= '2016-10-01' and Target_Status in ('D','G','J')) 
--                    Target_Status_Value in ('D-Adolescent Not-SED', 'G-Adult Not SMI/SPMI', 
--                                             'J-Older Adult Not SMI/SPMI'))
                  OR (CCAR_Assessment_Date < '2016-10-01' and (TIERsed = 'F' and TIERspmi = 'F' and TIERsmi = 'F'))))
           then 'Indigent2'
         else NULL
       end spec1_indigent2,

       -- calculate ntindigent
       case 
         when CHARINDEX('CC-', service_program_value) > 0 or CHARINDEX('PS-', service_program_value) > 0   
           then NULL  -- skip pre-screening or community contact programs, which will include CCC-WIC & CCC_MCT 
         when JCMH_Mcaidpayer > 0 or Mcaid_Elig_State is not null  -- has medicaid
           then NULL
         when nbr_mcare_supp > 0   -- has medicare supplemental payer
           then NULL
         when nbr_exclude_payers > 0  -- skip due to payer
           then NULL
         when proc_cov_mcare = 'Y'  -- skip covered medicare services
              -- primary_payor_indigent = 'Medicare', 'Medicare Railroad', 'AARP - Medicare Complete',
              --                          'Kaiser Medicare','Humana Medicare'
              and primary_payor_indigent in (1069,1083,1104,1107,2002)  -- change guarantor check from name to ID 
           then NULL
         when option_id = 'CWSPN22003'  -- skip any group notes for NTINDIGENT only 
           then NULL
         when FYAge <= 11  -- not kids, but adolescent, adults and older adults
               and (FYCCARInEffect = 'Yes'  -- in the past year, a CCAR was in effect
               and indigent_byincomedepend = 'Yes'  -- based on income / nbr of dependents, client may be indigent
--               primary_payor_indigent = 'Self Pay','Self Pay Sliding Fee 6%','Medicare','Medicare - Railroad',
--                                        'AARP - Medicare Complete','Kaiser Medicare','Humana Medicare',
--                                        'Evercare/UBH Claims','Total Long Term Care')
               and primary_payor_indigent in (1027,1105,1069,1083,1104,1107,2002,1039,1094) -- change guarantor check from name to ID
               and ((CCAR_Assessment_Date >= '2016-10-01' and Target_Status in ('B'))
                     --Target_Status_Value in ('B-Child Not SED'))
                 OR (CCAR_Assessment_Date < '2016-10-01' and (TIERsed = 'F' and TIERspmi = 'F' and TIERsmi = 'F'))))
           then 'NTIndigent'
         else NULL
       end spec1_ntindigent,

       -- calculate indigent
       case 
         when CHARINDEX('CC-', service_program_value) > 0 or CHARINDEX('PS-', service_program_value) > 0   
           then NULL  -- skip pre-screening or community contact programs, which will include CCC-WIC & CCC_MCT
         when JCMH_Mcaidpayer > 0 or Mcaid_Elig_State is not null  -- has medicaid
           then NULL
         when nbr_mcare_supp > 0   -- has medicare supplemental payer
           then NULL
         when nbr_exclude_payers > 0  -- skip due to payer
           then NULL
         when proc_cov_mcare = 'Y'  -- skip covered medicare services
              -- primary_payor_indigent = 'Medicare', 'Medicare Railroad', 'AARP - Medicare Complete',
              --                          'Kaiser Medicare','Humana Medicare')
              and primary_payor_indigent in (1069,1083,1104,1107,2002)  -- change guarantor check from name to ID
           then NULL
         when FYCCARInEffect = 'Yes'  -- has a CCAR in effect during the past year  -- 6/14/2017, MLS
             and indigent_byincomedepend = 'Yes'  -- based on income / nbr of dependents, client may be indigent 
------           primary_payor_indigent = 'Self Pay','Self Pay Sliding Fee 6%','Medicare','Medicare - Railroad',
------                                    'AARP - Medicare Complete','Kaiser Medicare','Humana Medicare',
------                                    'Evercare/UBH Claims','Total Long Term Care'
             and primary_payor_indigent in (1027,1105,1069,1083,1104,1107,2002,1039,1094) -- change guarantor check from name to ID  
             and ((CCAR_Assessment_Date >= '2016-10-01' and Target_Status in ('A','C','E','F','H','I'))
--                  Target_Status_Value in ('A-Child SED','C-Adolescent SED','E-Adult SPMI','F-Adult SMI',
--                                          'H-Older Adult SPMI','I-Older Adult SMI'))
               OR (CCAR_Assessment_Date < '2016-10-01' and (TIERsed = 'T' or TIERspmi = 'T' or TIERsmi = 'T')))
           then 'Indigent'
         else NULL
       end spec1_indigent,

       t.current_program, t.current_care_coord,

       episode_program_value, episode_program_code, admit_date, discharge_date

into #temp_round2_format
from #temp_round1_format t

--select *
--from #temp_round2_format


--drop table #temp_final_format
-- --------------------------------------------------------------------------------------------------------------------------
--  STEP 16 - final format for service table
-- --------------------------------------------------------------------------------------------------------------------------
select txhist_opdocid, patid, txhist_episode, note_type_value, data_entry_date, 
       cw_data_entry_time as data_entry_time, data_entry_by,
       service_program_value, service_program_code, dept_name, 
       service_charge_value, service_charge_code, duration,
       note_addresses, NOT_Uniqueid, join_to_tx_history, ORIG_JOIN_TO_TX_HISTORY,
       date_of_service, datesvc_ymd, datesvc_yyyy, datesvc_mm, service_start_time, 
       appt_time, lk_time_start,

       data_entry_date as FinalSave, FinalSave_ymd, FinalSave_yyyy, FinalSave_mm, 
       txhist_nbr_of_clients, txhist_appt_status_code, txhist_appt_status_value, 
       primary_guarantor, aps_link,
       location_code, location_value, FEDPOS, Credentials, Diag_on_note_code, Diag_on_note_value, 
       Diag_On_Note_DxType, Diag_On_Note_DxRank,
       lk_joindx, lk_joindx_value, lk_dxtype, lk_dxrank, primdiagkey_fin, PrimDiagKey_fin2,

       practitioner_id, practitioner_name, 
       patient_name_last, patient_name_first, patient_sex_code, patient_add_city, patient_add_county_value, patient_add_zipcode,
       date_of_birth, fyage, agecategory, 
       format_ssn, patient_ssn, lk_ssn, cost_of_service, enc_type,        
       cpt_code, vo_cpt_code, vo_cpt_code_conv, vo_cpt_code_fin, primary_guar_id,  
       elig_SSN_Mcaid, elig_mcaid_Mcaid, elig_namedob_Mcaid, Mcaid_elig_state, 
       policy_num, costclass, payertype, unitcost, 
       calc_units, calc_units_2ndlook, calc_units_1stlook,
       CCARType, 
       income, nbr_dependents, nbrdep_1, nbrdep_2, nbrdep_3, nbrdep_4, nbrdep_5, fee_percent, 
       tier_income, tier_nbr_dependents,
       tier_nbrdep_1, tier_nbrdep_2, tier_nbrdep_3, tier_nbrdep_4, tier_nbrdep_5, tier_fee_percent,
       indigent_byincomedepend, IsEnrolled,

       C000_Assessment_Date, target_status, Target_status_value,
       av_meds_only, av_ccar_draft_final,
       TIEReffdate, tiermeds, TIERsed, TIERsmi, TIERspmi,
       FYCCARInEffect, note_FY, av_CCAR_FY, tier_CCAR_FY, 
       CCAR_Assessment_Date, option_desc, option_id,
       RVUIsFacility, Base_RVU,

       -- part of the RVU addition, calculate SUM RVU
       case 
         when base_rvu  = '' then NULL 
         else (convert(decimal(5,2),Base_RVU) * calc_units)
       end as SUM_RVU,

       mcrecovcptcode, nbr_mcarepayers, JCMH_Mcaidpayer, JCMH_Mcaidid, nbr_exclude_payers, nbr_mcareAdv, 
       nbr_mcare_supp, proc_cov_mcare,

       case
         -- for School Based notes without a payer, set to Medicaid when client is Medicaid eligible
         when (primary_payor_note is null  and primary_payor_guar_order is null)
              and service_program_value in ('CC-School Based','PS-School Based','School Based')
              and Mcaid_Elig_State is not null  -- has Medicaid
              and primary_payor_note is null -- doesn't have a payer
           then 'Medicaid CAP, JCMH'
         -- for School Based notes without a payer, set to School Gran when client is not Medicaid eligible
         when (primary_payor_note is null  and primary_payor_guar_order is null)
              and service_program_value in ('CC-School Based','PS-School Based','School Based')
              and Mcaid_Elig_State is null   -- doesn't have Medicaid
              and primary_payor_note is null  -- doesn't have a payer
           then 'School Grant'         
         when primary_payor_note is not null 
           then (select gu.guarantor_name
                 from avatardw.SYSTEM.billing_guar_table gu
                 where gu.GUARANTOR_ID = t.primary_payor_note)
         else (select gu.guarantor_name
               from avatardw.SYSTEM.billing_guar_table gu
               where gu.GUARANTOR_ID = t.primary_payor_guar_order)
              --          primary_payor_guar_order
         end as primary_payor_note_school,

       primary_payor_guar_order,
       (select gu.guarantor_name
        from avatardw.SYSTEM.billing_guar_table gu
        where gu.GUARANTOR_ID = t.primary_payor_guar_order) as primary_payor_guar_order_name,

       primary_payor_note,
       (select gu.guarantor_name
        from avatardw.SYSTEM.billing_guar_table gu
        where gu.GUARANTOR_ID = t.primary_payor_note) as primary_payor_note_name,

       primary_payor_indigent,
       (select gu.guarantor_name
        from avatardw.SYSTEM.billing_guar_table gu
        where gu.GUARANTOR_ID = t.primary_payor_indigent) as primary_payor_indigent_name,

       spec1_indigent2, spec1_ntindigent, spec1_indigent,

       case
         when t.service_program_code in ('9900','9901','8400','7127','7227','7128','7228') --'3200', -- removed Solutions Now (3200)
           then NULL  --- CCC clients can't be indigent
         --when t.spec2_fin <> '' then NULL  -- not working, still MLS 3/14/2017
         when t.spec1_indigent2 = 'Indigent2' then t.spec1_indigent2
         when t.spec1_ntindigent = 'NTIndigent' then t.spec1_ntindigent
         when t.spec1_indigent = 'Indigent' then t.spec1_indigent
         else NULL
       end as indigent,

       case
         when C000_Assessment_Date >= '2016-10-01' and Target_Status_Value in ('A-Child SED', 'C-Adolesecent SED', 'E-Adult SPMI',
                                                                               'F-Adult SMI','H-Older Adult SPMI','I-Older Adult SMI')
           then 'Y'  -- in Avatar
         when C000_Assessment_Date < '2016-10-01' and (TIERsed = 'T' or TIERspmi = 'T' or TIERsmi = 'T')
           then 'Y'  -- in TIER
         else 'N'
       end as Target,

       case
         when C000_Assessment_Date >= '2016-10-01' and Target_Status_Value in ('A-Child SED', 'C-Adolesecent SED', 'E-Adult SPMI',
                                                                               'F-Adult SMI','H-Older Adult SPMI','I-Older Adult SMI')
           then Income  -- in Avatar
         when C000_Assessment_Date < '2016-10-01' and (TIERsed = 'T' or TIERspmi = 'T' or TIERsmi = 'T')
           then Tier_Income  -- in TIER
         else Income
       end as Income_fin,

      case
         when Mcaid_elig_state is not null then Mcaid_Elig_State
         else JCMH_McaidID
       end as MedicaidID,

       case 
         when Mcaid_elig_state is not null then 'Y'
         else 'N'
       end as IsEligible,

       t.current_program, t.current_care_coord,

       episode_program_value, episode_program_code, admit_date, discharge_date

into #temp_final_format    --mary_jcmh_service_avartar_research
from #temp_round2_format t

--select *
--from #temp_final_format


---- --------------------------------------------------------------------------------------------------------------------------
----  STEP 17 - create research version of service table
---- --------------------------------------------------------------------------------------------------------------------------
-- 
drop table jcmh.dbo.JCMH__Service_Avatar_research
SELECT *
INTO jcmh.dbo.JCMH__Service_Avatar_research
FROM #temp_final_format
ORDER BY CONVERT(INT,patid)

--select *
--from jcmh.dbo.JCMH__Service_Avatar_research


-- --------------------------------------------------------------------------------------------------------------------------
--  STEP 18 - create service table with Avatar data
-- --------------------------------------------------------------------------------------------------------------------------
-- before creating service table, delete the current version
--drop table jcmh.dbo.JCMH__Service_Avatar
-- 
SELECT patid AS inquirykey, patid AS clientkey, join_to_tx_history AS chargekey, NULL AS AREVClientkey, 
       patient_name_last + ', ' + patient_name_first AS fullname, patient_name_last AS namel,
       date_of_service AS datesvc, datesvc_ymd, 
       CONVERT(VARCHAR(12), CONVERT(DATETIME, service_start_time),114) AS TimeSvc,

       DateSvc_yyyy + DateSvc_mm AS DateSvc_ym,
       NULL AS IntProcCode, service_charge_value AS descr, service_program_value AS pgmname, service_program_code AS pgmnum, 
       dept_name AS DeptName,
       cpt_code AS JCMH_CPT_Code, NULL AS FlexServCode, CostClass, UnitCost, primary_payor_note_school AS PrimaryPayor,

       Policy_num AS PolicyNum, PrimDiagKey_fin2 AS PrimaryDX, NULL AS GAF, NULL AS [DX Grouping], NULL AS JCMHID,
       practitioner_name AS ClinicianName, practitioner_id AS StaffKey, Credentials, location_value AS PlaceOfService,
       Duration, patient_add_zipcode AS zip, date_of_birth AS DOB, FYAge AS Age, AgeCategory,

       patient_sex_code AS Gender, patient_add_county_value AS County, patient_add_city AS City, nbr_dependents AS NumofDep,
       Target, PayerType AS PayorType, data_entry_date AS FinalSave, FinalSave_ymd, FinalSave_yyyy + FinalSave_mm AS FinalSave_ym,
       NULL AS IsEncounter, 

       IsEnrolled, MedicaidID, IsEligible, NULL AS EligCategory,
       CCARType, Base_RVU, SUM_RVU,
       service_charge_code AS ProcKey, RVUIsFacility,
       vo_cpt_code_fin AS VO_CPT_Code, Income_fin AS Income, 
       calc_units_2ndlook AS Units, cost_of_service AS Charge_Amt, NULL AS PayorPlanKey, NOT_uniqueid AS opdocid,
       NULL AS Consolidation, Indigent,

       current_program, current_care_coord,

       episode_program_value, episode_program_code, admit_date, discharge_date

INTO jcmh.dbo.JCMH__Service_Avatar
FROM jcmh.dbo.JCMH__Service_Avatar_research
ORDER BY CONVERT(INT,patid), date_of_service
----     

--
GO


