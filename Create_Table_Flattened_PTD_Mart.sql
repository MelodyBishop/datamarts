
USE [DataMart2]
GO

DROP TABLE Reports.FlattenedPTD_Mart
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE Reports.FlattenedPTD_Mart(
	[PATID] [varchar](10) NULL,
	[Draft_Final_Value] [varchar](10) NULL,
	[Patient_Name] [varchar](100) NULL,
	[NoteDateOfService] [date] NULL,
	[DateOfNote] [date] NULL,
	[Data_Entry_By] [varchar](100) NULL,
	[Service_Program_Value] [varchar](100) NULL,
	[Service_Charge_Value] [varchar](100) NULL,
	[DueDate] [date] NULL,
	[PractitionerID] [varchar](10) NULL,
	[Practitioner_Name] [varchar](100) NULL,
	[Manager_Name] [varchar](100) NULL,
	[Team] [varchar](100) NULL,
	Green bit NULL,
	Yellow bit NULL,
	Red bit NULL,
	FutureDate bit NULL,
	Flag varchar(10) NULL,
	Note_UniqueID varchar(25) NULL
) ON [PRIMARY]
GO


