USE DataMarts
GO

DROP PROCEDURE EDW.JC_A_SP_UpdateCaseloadManagement_OverAssigned_Care_Coordinators_Cache
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--execute EDW.JC_A_SP_UpdateCaseloadManagement_OverAssigned_Care_Coordinators


-- =============================================
-- Author:		Melody Bishop
-- Create date: 7/2/2018
-- Description:	Captures OverAssignments to the care coodinator field
-- =============================================
CREATE PROCEDURE EDW.JC_A_SP_UpdateCaseloadManagement_OverAssigned_Care_Coordinators_Cache
	
AS
BEGIN

SET NOCOUNT ON;

if OBJECT_ID('tempdb.dbo.#temp','U') is not null DROP table #temp;
Select Row_Number() Over(Partition by patid order by patid asc, assignment_date desc) as Ctr,* 
into #temp from 
(Select 
 esc.patid
 ,esc.episode_number ep
, (esc.patient_name_last + ',' + esc.patient_name_first) Patient_Name
, ISNULL(clm.care_coordin_add_value,'NoCC') CareCoor
,clm.care_coordin_ADD_CASELOADID
,clr.care_coordin_Remove_CASELOADID
,clm.care_coordin_program_Value
,clm.assignment_date
,clm.care_coordin_add_value CC_ADD
,clr.care_coordin_Remove_value CC_REMOVE
, esc.preadmit_admission_date
--, Case ISNULL(hr.ManagerName, 'No Manager') when '' then 'No Manager' else ISNULL(hr.ManagerName, 'No Manager') end ManagerName
,c_attend_practitioner_name
From 
 (Select patid, cm.facility,episode_number, assignment_date,care_coordin_ADD_CASELOADID ,care_coordin_add,
care_coordin_ADD_Value,care_coordin_program_Value 
from AvatarDW.CWSSYSTEM.caseload_management cm
WHERE option_id='USER13' 
 ) clm 

left join  (Select patid, cm.facility,episode_number, assignment_date,care_coordin_Remove_CASELOADID ,care_coordin_Remove,
care_coordin_Remove_Value,care_coordin_program_Value 
from AvatarDW.CWSSYSTEM.caseload_management cm
WHERE option_id='USER22' 
 ) clr
 on clm.patid=clr.patid
 and clm.facility = clr.facility
 and clm.episode_number =clr.episode_number 
 and clm.care_coordin_add_caseloadid = clr.care_coordin_remove_caseloadid
 inner join (Select  userid, CASE staff_member_id when '' then 'NORADPLUS USERID' else staff_member_id end staff_member_id
				from AvatarDW.SYSTEM.RADplus_users
					) rad
				on clm.care_coordin_add = rad.userid	
 --left join (Select avatarstaffid, ManagerName from HR.Emp_Avatar where avatarstaffid >0 --empstatus='Active'
 --) hr
	-- on rad.staff_member_id = hr.avatarstaffid
inner join 
(Select patid, facility, episode_number, patient_name_last, patient_name_first,last_date_of_service,c_admit_type_of_code,preadmit_admission_date ,program_code,c_attend_practitioner_name
				from AvatarDW.system.view_episode_summary_current
				where program_code='7000' 
				and c_admit_type_of_code = 1
				and Upper(patient_name_last) not like 'TEST%'
				and Upper(patient_name_last) not like 'GROUP%'
				and Upper(patient_name_last) not like 'ZZZ%'

 )   esc
				on  clm.patid 		= esc.patid 
				and clm.facility		= esc.facility
				and clm.episode_number 	= esc.Episode_number

WHERE 
clr.care_coordin_Remove is null
) sub
GROup by patid, ep 
, Patient_Name
, CareCoor
,care_coordin_ADD_CASELOADID
,care_coordin_Remove_CASELOADID
,care_coordin_program_Value
,assignment_date
, CC_ADD
, CC_REMOVE
, preadmit_admission_date
--, ManagerName
,c_attend_practitioner_name 
ORDER BY PATID, assignment_date

Select 
RowNumber
,PATID 
,EP 
,Patient_Name
,CareCoor
,Care_Coordin_ADD_CASELOADID
,Care_Coordin_Remove_CASELOADID
,Care_Coordin_Program_Value
,Assignment_Date
,CC_ADD
,CC_REMOVE
,PreAdmit_Admission_Date
---,ManagerName
,c_attend_practitioner_name AttendingPhysician
from #temp where
patid in (Select patid from #temp where ctr>1)
order by patid asc, ctr desc

End 
Go
