
USE [DataMart2]
GO

DROP PROCEDURE [Reports].[FlattenedPTDs]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--execute  Reports.FlattenedPTDs 
--select * from Reports.FlattenedPTD_Mart order by 1
/*------------------------------------------------------
Create a flattened version of PTD reporting for dashboard- Melody Bishop 6/18/2018
------------------------------------------------------*/
Create Procedure [Reports].[FlattenedPTDs] 
As
Begin
Truncate table Reports.FlattenedPTD_Mart
Insert into Reports.FlattenedPTD_Mart
 
Select patid
,draft_final_value
, patient_name
, NoteDateOfService
,DateOfNote
,data_entry_by
,Service_program_value
,service_charge_value
,DueDateCalc
,PractitionerID
,Practitioner_name
,ManagerName
,ISNULL(Team,'DP0000 No Team') Team
,Green
,Yellow
,Red
,FutureDate
,Case when Green=1 then 'Green'  else 
		Case when Yellow=1 then 'Yellow'  else 
			Case when Red=1 then 'Red'  else 
				Case when FutureDate=1 then 'FutureDate'  else '' end 
		 end  end  end flag
,NOT_uniqueid
From 
(
Select patid
,draft_final_value
, patient_name
, NoteDateOfService
,DateOfNote
,data_entry_by
,Service_program_value
,service_charge_value
,DueDateCalc
,PractitionerID
,Practitioner_name
,ManagerName
,ISNULL(Team,'DP0000 No Team') Team
,CASE  when DateDiff(dd,DueDateCalc,GetDate()) <-2 then 1 else 0 end Green
,Case  when DateDiff(dd,DueDateCalc,GetDate()) between -2 and 0 then 1 else 0 end Yellow
,Case  when DateDiff(dd,DueDateCalc,GetDate()) >0 then 1 else 0 End Red
,Case  when NoteDateOfService >DateAdd(dd,0,GetDate()) then 1 else 0 end FutureDate
,NOT_uniqueid
From 
(
select
 n.patid
   ,n.draft_final_value
  , Upper(esc.patient_name_last +',' + esc.patient_name_first) patient_name	 
  , n.date_of_service NoteDateOfService
  , n.date_of_note DateOfNote
  ,n.data_entry_by
  , n.service_program_value Service_program_value
  , n.service_charge_value
 ,  CASE DatePart(dw, n.date_of_Service)  when  5 Then DateAdd (dd,5 ,n.date_of_Service)
		  when 6 Then DateAdd (DD,4,n.date_of_Service )
			 when 7 Then DateAdd (DD,3 ,n.date_of_Service )
			 when 1 Then DateAdd (DD,3 ,n.date_of_Service )
			 when 2 Then DateAdd (DD,3 ,n.date_of_Service )
			 when 3 Then DateAdd (DD,5 ,n.date_of_Service )
			 when 4 Then DateAdd (DD,5 ,n.date_of_Service )
			end DueDateCalc
 , Case Len(n.practitioner_id) when 0 then '000000' else n.practitioner_id end PractitionerID
  , n.practitioner_name Practitioner_Name
 , Case n.practitioner_name when 'Unknown' then 'No Manager' else  ISNULL(hr.managername, 'No Manager')  end ManagerName	
 , Case Team when 'Unknown' then 'No Team' else Team end Team
 , n.NOT_uniqueid
 		  From 
 AvatarDW.cwssystem.cw_patient_notes n
INNER JOIN (Select patid, facility, episode_number, last_date_of_service,c_admit_type_of_code,preadmit_admission_date ,program_code, patient_name_last, patient_name_first
				from AvatarDW.system.view_episode_summary_current
				where program_code='7000' 
				and c_admit_type_of_code = 1
				and UPPER(Patient_name_last) not like 'TEST%'
				and UPPER(Patient_name_last) not like 'GROUP%'
			    and UPPER(Patient_name_last) not like 'ZZZ%'
				) esc
				on n.patid = esc.patid
left  join (Select empmanager ManagerName, avatarstaffid , empteam Team from HR.HR.Emp_Avatar where avatarstaffid is not null and len(avatarstaffid)>0) hr
		on hr.avatarstaffid = n.practitioner_id
		
where 
	 n.Service_Charge_Code NOT IN (909,940,947) --These are 909 HAF house, 940 Therapeutic day and 947 Travel W/out client
	 and n.draft_final_code='D'
	 and n.practitioner_name<>'MCS,Record'
	 and n.date_of_service is not null and n.date_of_service<>''

	 --Questions for SarahT
	 --Are there dates we can use as minimums?  Many records are dated a long time ago
	 --Do we need to delete notes, or request data repairs to remove them, or can we mark them somehow to close and not bill

	)sub
 
)sub1
order by duedatecalc
--where patid = 45604618
End
GO


