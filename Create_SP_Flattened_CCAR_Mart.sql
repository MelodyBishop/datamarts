USE [DataMart2]
GO
DROP PROCEDURE [Reports].[FlattenedCCARs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--execute  Reports.FlattenedCCARs 
--select flag,duedatecalc,red, green, yellow, futuredate,* from Reports.FlattenedCCAR_Mart
/*------------------------------------------------------
Create a flattened version of CCAR reporting for dashboard- Melody Bishop 6/20/2018
------------------------------------------------------*/
Create Procedure [Reports].[FlattenedCCARs] 
As
Begin
Truncate table Reports.FlattenedCCAR_Mart
Insert into Reports.FlattenedCCAR_Mart
Select 
Manager
 ,CareCoor
 ,Patid
, Patient_Name
, PreAdmissionDate
, AssessmentDate
, UpdateDate
, Draft_Final
, LastDateOfService
, ActionType
, UpdateValue
, Cov_Effective_Date
, Team
, duedatecalc
, Green
, Yellow
, Red
, FutureDate --no real indicator yet for this
,Case when Green=1 then 'Green'  else 
		Case when Yellow=1 then 'Yellow'  else 
			Case when Red=1 then 'Red'  else ''
				--Case when FutureDate=1 then 'FutureDate'  else '' end 
		 end  end  end flag
,MaxID
From 
(
Select Manager
 ,CareCoor
 ,Patid
, Patient_Name
, PreAdmissionDate
, AssessmentDate
, UpdateDate
, Draft_Final
, LastDateOfService
, ActionType
, UpdateValue
, Cov_Effective_Date
, Isnull(Team,'No Team') Team
, duedatecalc
,CASE when DueDateCalc >GetDate() then 1 else 0 end Green
,Case  when DueDateCalc between DateAdd(dd,-60,GetDate()) and DateAdd(dd,-0,GetDate()) then 1 else 0 end Yellow
,Case  when DueDateCalc < DateAdd(dd,-61,GetDate()) then 1 else 0 End Red
,Case when AssessmentDate>GetDate() then 1 else 0 end FutureDate --no real indicator yet for this
, MaxID 
from 

(
Select distinct
 ISNULL(hr.empmanager, 'No Manager') manager
 , Case CCAdd.care_coordin_add_value when NULL then 'No CC' when '' then 'No CC' else CCAdd.care_coordin_ADD_Value end CareCoor
 , esc.patid
, (esc.patient_name_last + ',' + esc.patient_name_first) Patient_Name
, Cast(esc.preadmit_admission_date as date) PreAdmissionDate
, Cast(cc.C000_Assessment_Date as date) AssessmentDate
, Cast(cc.C115_Date_of_Update as date) UpdateDate
, cc.Draft_Final
, Cast(ISNULL(esc.last_date_of_service,'2099-12-31') as date) LastDateOfService
, Case cc.C123_Action_Type 
				when '01' Then 'Admission'
				when '01Admission' Then 'Admission'
				when '03' Then 'Update' 
				when '03Update' Then 'Update' 
				when '05' then 'Discharge'
				when '05Discharge' then 'Discharge'
				when '06' then 'Evaluation Only' 
				when '06Evaluation Only' then 'Evaluation Only' 
				else ''
				END  	ActionType
, cc.C125_Type_Of_Update_Value UpdateValue
, cast(Cov.cov_effective_date as date) Cov_Effective_Date
, ISNULL(empTeam ,'DP0000 No Team') Team
,'' Green 
,'' Yellow
,'' Red
,'' FutureDate
, CCMax.MaxID
, Case  ISNULL(cc.draft_final,'x') when 'D' then DateAdd(m,1,cc.C000_Assessment_Date)
									when 'F' then DateAdd(d,365,cc.C115_Date_of_Update)
									when 'x' then Cov.cov_effective_date
	  else ''
	  end DueDateCalc

From 
(Select patid, facility, episode_number, patient_name_last, patient_name_first,last_date_of_service,c_admit_type_of_code,preadmit_admission_date ,program_code
				from AVATARDW.system.view_episode_summary_current
				where program_code='7000' 
				and c_admit_type_of_code = 1
				and Upper(patient_name_last) not like 'TEST,%'
				and Upper(patient_name_last) not like 'GROUP%'
				and Upper(patient_name_last) not like 'ZZZ%'
				and facility =1
		 )   esc

 inner join 
 (	Select patid, facility,episode_number, care_coordin_ADD_CASELOADID ,care_coordin_add,care_coordin_add_value
	from AVATARDW.CWSSYSTEM.caseload_management 
	WHERE option_id='USER13' ) CCAdd
				on  CCAdd.patid			= esc.patid
				and CCAdd.facility 		= esc.facility 
				and CCAdd.episode_number= esc.episode_number
left join 
(Select patid, facility,episode_number, care_coordin_remove_CASELOADID ,care_coordin_remove
	from AVATARDW.CWSSYSTEM.caseload_management 
	WHERE option_id='USER22' 
 )CCRem
on esc.facility = CCrem.facility
and esc.patid   = CCrem.patid
and esc.episode_number = CCrem.episode_number
and CCAdd.care_coordin_ADD_CASELOADID=CCRem.care_coordin_remove_CASELOADID

left join AVATARDW.cwssystem.caseload_management cm
				on  cm.patid 			= esc.patid 
				and cm.facility			= esc.facility
				and cm.episode_number 	= CCAdd.episode_number 
				and cm.care_coordin_ADD_CASELOADID 	= CCAdd.care_coordin_ADD_CASELOADID
left join (Select  userid, staff_member_id 
				from [AvatarDW].[SYSTEM].[RADplus_users] 
					) rad
					on cm.care_coordin_add = rad.userid
left join HR.[HR].[Emp_Avatar] hr
				on rad.staff_member_id = hr.avatarstaffid
							
left join 
(
	Select Max(CCAR.ID) MaxID, CCAR.patid, CCAR.facility from AVATARDW.SYSTEM.CCAR_Colorado_Client_Assessm CCAR
		Left join
			(
				Select  patid, facility,Max(C000_Assessment_Date) MaxAssesDt
					from AVATARDW.SYSTEM.CCAR_Colorado_Client_Assessm
					Where (C125_Type_Of_Update_Value not like 'Interim%' or C125_Type_Of_Update_Value ='[NULL]')
					Group by patid, facility
			) sub
			ON  CCAR.patid	 		 = sub.patid
			and CCAR.facility		 = sub.facility
			WHERE CCAR.C000_Assessment_Date  = sub.MaxAssesDt
			Group by CCAR.patid, CCAR.facility
) ccMax
				on esc.patid	  = ccMax.patid
				and esc.facility  = ccMax.facility
left join AVATARDW.SYSTEM.CCAR_Colorado_Client_Assessm cc
				on  esc.patid		= cc.patid
				and esc.facility 	= cc.facility
				and ccMax.MaxID		= cc.id

--finds clients guarantors, but only those that are considered Medicaid by financial class in GUAR table
inner join (Select patid, facility, Episode_number, Max(guarantor_id) guarantor_id
				from 
				(Select patid, facility, Max(Episode_number) episode_number, guarantor_id 
				from AVATARDW.SYSTEM.billing_guar_subs_data 
				Where guarantor_id in (Select guarantor_id from AVATARDW.system.billing_guar_table 
											where financial_class_value like '%Medicaid%' ) 
				group by patid, facility, guarantor_id) sub
				Group by patid, facility, episode_number
				) subs
				on  subs.patid			= esc.patid
				and subs.facility 		= esc.facility
				and subs.episode_number	= esc.episode_number
inner JOIN AVATARDW.system.billing_guar_emp_data Cov
				on  subs.patid			= Cov.patid
				and subs.facility 		= Cov.facility
				and subs.episode_number = Cov.episode_number
				and subs.guarantor_id 	= Cov.guarantor_id
WHERE 
esc.preadmit_admission_date <= cc.C115_Date_of_Update
and Cast(CCAdd.Care_coordin_add_caseloadID as int)- Cast(IsNull(CCRem.care_coordin_remove_CaseloadID,'9999') as int) <>0

UNION ALL

--No CCARS, but headers loaded to indicate what was status from Tier
select distinct
 ISNULL(hr.empmanager, 'No Manager') managername
, Case CCAdd.care_coordin_add_value when NULL then 'No CC' when '' then 'No CC' else CCAdd.care_coordin_ADD_Value end CareCoor
, esc.patid
, (esc.patient_name_last + ',' + esc.patient_name_first) Patient_Name
, Cast(esc.preadmit_admission_date as date) preadmit_admission_date
, Cast('2099-12-31' as date)   AssessmentDate
, Cast('2099-12-31' as date)    UpdateDate
, 'X' Draft_Final
, Cast(ISNULL(esc.last_date_of_service,'2099-12-31') as date) LastDateOfService
, '' ActionType
, '' UpdateValue
, Cast(Cov.cov_effective_date as date) Cov_Effective_Date
, ISNULL(empTeam ,'No Team') Team
, '' Green
, '' Yellow
, '' Red
, 0 FutureDate --no real indicator yet for this
, CCMax.MaxID
, Case  ISNULL(cc.draft_final,'x') 
	  when 'D' then DateAdd(m,1,cc.C000_Assessment_Date)
	  when 'F' then DateAdd(d,365,cc.C115_Date_of_Update)
	  when 'x' then Cov.cov_effective_date
	  else ''
	  end DueDateCalc

FROM
(Select patid, facility, episode_number, patient_name_last, patient_name_first,last_date_of_service,c_admit_type_of_code,preadmit_admission_date ,program_code
				from AVATARDW.system.view_episode_summary_current
				where program_code='7000' 
				and c_admit_type_of_code = 1
				and Upper(patient_name_last) not like 'TEST%'
				and Upper(patient_name_last) not like 'GROUP%'
				and Upper(patient_name_last) not like 'ZZZ%'
				and facility =1
		 )   esc
inner join (Select patid, facility, Episode_number, Max(guarantor_id) guarantor_id
				from 
				(Select patid, facility, Max(Episode_number) episode_number, guarantor_id from AVATARDW.SYSTEM.billing_guar_subs_data 
					Where guarantor_id in (Select guarantor_id from AVATARDW.system.billing_guar_table where financial_class_value like '%Medicaid%' ) 
					group by patid, facility, guarantor_id
					) sub
					Group by patid, facility, episode_number
				) subs
				on  subs.patid			= esc.patid
				and subs.facility 		= esc.facility
				and subs.episode_number	= esc.episode_number
 left join AVATARDW.SYSTEM.CCAR_Colorado_Client_Assessm cc
				on  esc.patid		= cc.patid
				and esc.facility 	= cc.facility
				--and ccMax.MaxID		= cc.id
inner join 
 (	Select patid, facility,episode_number, care_coordin_ADD_CASELOADID ,care_coordin_add,care_coordin_add_value
	from AVATARDW.CWSSYSTEM.caseload_management 
	WHERE option_id='USER13' ) CCAdd
				on  CCAdd.patid			= esc.patid
				and CCAdd.facility 		= esc.facility 
				and CCAdd.episode_number= esc.episode_number
left join 
(Select patid, facility,episode_number, care_coordin_remove_CASELOADID ,care_coordin_remove
	from AVATARDW.CWSSYSTEM.caseload_management 
	WHERE option_id='USER22' 
 )CCRem
on esc.facility = CCrem.facility
and esc.patid   = CCrem.patid
and esc.episode_number = CCrem.episode_number
and CCAdd.care_coordin_ADD_CASELOADID=CCRem.care_coordin_remove_CASELOADID
 
left join AVATARDW.cwssystem.caseload_management cm
				on  cm.patid 			= esc.patid 
				and cm.facility			= esc.facility
				and cm.episode_number 	= CCAdd.episode_number 
				and cm.care_coordin_ADD_CASELOADID 	= CCAdd.care_coordin_ADD_CASELOADID
left join (Select  userid, staff_member_id from AvatarDW.SYSTEM.RADplus_users 
			) rad
				on cm.care_coordin_add = rad.userid
left join HR.HR.Emp_Avatar hr
				on rad.staff_member_id = hr.avatarstaffid						
left join 
(
	Select Max(CCAR.ID) MaxID, CCAR.patid, CCAR.facility from AVATARDW.SYSTEM.CCAR_Colorado_Client_Assessm CCAR
		Left join
			(
				Select  patid, facility,Max(C000_Assessment_Date) MaxAssesDt
					from AVATARDW.SYSTEM.CCAR_Colorado_Client_Assessm
					Where (C125_Type_Of_Update_Value not like 'Interim%' or C125_Type_Of_Update_Value ='[NULL]')
					Group by patid, facility
			) sub
			ON  CCAR.patid	 		 = sub.patid
			and CCAR.facility		 = sub.facility
			WHERE CCAR.C000_Assessment_Date  = sub.MaxAssesDt
			Group by CCAR.patid, CCAR.facility
) ccMax
				on  esc.patid	  = ccMax.patid
				and esc.facility  = ccMax.facility


inner JOIN AVATARDW.system.billing_guar_emp_data Cov
				on  subs.patid			= Cov.patid
				and subs.facility 		= Cov.facility
				and subs.episode_number = Cov.episode_number
				and subs.guarantor_id 	= Cov.guarantor_id
WHERE 
 cc.patid is null
)
sub
)
sub2

END 
Go
