SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:	MelodyB
-- Create date: 10/24/2018
-- Extrract CEnsusLocations for GeoCoding
-- exec [STAGING].[JC_A_SP_UpdateSTAGING_CENSUS_LOCATION]
-- ===============================================

CREATE PROCEDURE [STAGING].[JC_A_SP_UpdateSTAGING_CENSUS_LOCATION]
AS
BEGIN

if OBJECT_ID('tempdb.dbo.#PatDays','U') is not null DROP table #PatDays;
Select Max(blank_row_ID) as MAXXID ,patid, data_entry_date
into #PatDays
from avatardw.system.patient_demographic_history p
inner join chords.chords_id id 
on p.patid=id.mrn
where Patient_name not like 'Test,%'
and Patient_name not like 'Group,%'
and Patient_name not like 'ZZZ,%'
Group by patid, data_entry_date
--199047

if OBJECT_ID('tempdb.dbo.#addrs','U') is not null DROP table #addrs;
Select distinct blank_row_ID,p.patid, patient_street_1 Address1, patient_street_2 Address2, patient_city City, patient_state State, patient_zipcode Zip, patient_county_code CountyCode, patient_county_value County
,p.data_entry_date, data_entry_by
into #addrs
from avatardw.system.patient_demographic_history p
inner join chords.chords_id id 
on p.patid=id.mrn
inner join #patDays pp
on p.patid=pp.patid and  p.blank_row_id = pp.MAXXID
order by patid, blank_row_id  desc --199047
--select patid, data_entry_date, count(*) ctr  from #addrs group by patid, data_entry_date order by 3 desc


--Add Rownumbers
if OBJECT_ID('tempdb.dbo.#addrs2','U') is not null DROP table #addrs2;
Select ROW_Number() Over(Partition by patid order by blank_row_id desc) as RowNum
 , patid, address1, address2, city, state, Left(zip,5) zip,
 Case len(zip) when 10 then Right(zip,4) else '' end zip_four
  , countycode,county, data_entry_date , data_entry_by
 into #addrs2
 FRom #addrs
 --select * from #addrs2 where patid=52915961

  --look for bad address deletion candidates
if OBJECT_ID('tempdb.dbo.#DeleteRowCandidates','U') is not null DROP table #DeleteRowCandidates
 Select patid , count(*) ctr 
 into #DeleteRowCandidates
 from #addrs2 group by patid
 having count(*)>1
  
--remove no value address records as long as they are not the only record (<> rownum 1)
delete from #addrs2 where address1='' and data_entry_date='2016-09-26 00:00:00.000' and data_entry_by='CSM Conversion' --and rownum<>1
and patid in (Select patid from #DeleteRowCandidates) --915 removed
delete from #addrs2 where address1='' and data_entry_date='2016-10-17 00:00:00.000' and data_entry_by='Web service user' --and rownum<>1--6378
and patid in (Select patid from #DeleteRowCandidates)--
delete from #addrs2 where address1='' and data_entry_date='2016-10-12 00:00:00.000' and data_entry_by='Web service user' --and rownum<>1
and patid in (Select patid from #DeleteRowCandidates)--2308
delete from #addrs2 where address1='' and data_entry_date='2016-10-09 00:00:00.000' and data_entry_by='CSM Conversion' --and rownum<>1
and patid in (Select patid from #DeleteRowCandidates)--79
--select * from #addrs2


if OBJECT_ID('tempdb.dbo.#addrs3','U') is not null DROP table #addrs3
Select ROW_Number() Over(Partition by patid  order by patid asc, data_entry_date asc, county asc) as RowNum
 , patid, address1, address2, city, state, zip,zip_four, countycode,county, data_entry_date , data_entry_by
 into #addrs3
 FRom #addrs2
 --select * from #addrs3--189367
  

 --Capture less columns for next row comparisons--176477
if OBJECT_ID('tempdb.dbo.#addrs4','U') is not null DROP table #addrs4
 select rownum, patid, address1, address2, city, state,zip,zip_four,countycode,county, data_entry_date as startdate, data_entry_date as enddate
 into #addrs4
 from #addrs3
 where rownum=1 
 or 
 (address1<>'' and city<>'' and state<>'' and zip<>'' and CountyCode<>'') and rownum<>1 --removing null Address 2 as exclusion
 --select * from #addrs4--

 --redo rownumbers for address end date calc--176477
 if OBJECT_ID('tempdb.dbo.#addrs5','U') is not null DROP table #addrs5
 Select ROW_Number() Over(Partition by patid  order by patid asc, startdate asc) as RowNum
 , patid, address1, address2, city, state, zip, zip_four, countycode,county, startdate , enddate
 into #addrs5
 FRom #addrs4
 --select * from #addrs5 order by patid, startdate
 --if we have to send enc_ID it will need to be ~here format

--Update with End Date on ADDRESSES
Update t 
set enddate = (Select DAteAdd(d,0,startdate) from #addrs5 tab where tab.patid = t.patid and Cast(tab.rownum as int) = Cast(t.rownum as int ) +1)
from #addrs5 t
--select * from #addrs5

--Update nulls with Big FUTURE DATE - we will remove it later
Update #addrs5 
set endDate='2100-01-01 00:00:00.000'
 where enddate is null --128953
--select * from #addrs5 order by patid, rownum asc


--Update various forms of bad addresses with Center
--Update JEFFCO/WHeatRidge/CO for empty addresses--4013
Update #addrs5 
set City = 'Wheat Ridge', State='CO', Zip = '80033', County='JEFFERSON'
where address1='' and address2='' and city='' and state='' and CountyCode='' and zip='' --67563
--select * from #addrs5


--Update JEFFCO/WHeatRidge/CO for empty/Homeless addresses--71
Update #addrs5 
set City = 'Wheat Ridge', State='CO', Zip = '80033', county='JEFFERSON'
where address1 like '%Homeless%' and address2='' and city='' and state='' and CountyCode='' and zip='' --1519
--select * from #addrs2 where patid=10002846

--Update JEFFCO/WHeatRidge/CO for 9999 zip addresses--
Update #addrs5 
set City = 'Wheat Ridge', State='CO', Zip = '80033', County='JEFFERSON'
where address2='' and city='' and state='' and CountyCode='' and zip='99999' --1
--select * from #addrs4 where patid = 10002846 order by patid


--refine/group scoring 1 or 0 based on whether the county field is populated
--85210
if OBJECT_ID('tempdb.dbo.#addrs6','U') is not null DROP table #addrs6
select patid, address1, address2, 'missing' address3, city, state, zip, zip_four, countycode,county, Min(StartDate) StartDate, Max(ENdDate) Enddate,
case countycode when '' then 0 else 1 end score
into #addrs6
from #addrs5 --where patid=10044975
group by patid, address1, address2, city, state, zip, zip_four,countycode,county
--select * from #addrs6 order by patid, startdate desc

--Final display ready
if OBJECT_ID('tempdb.dbo.#addrs7','U') is not null DROP table #addrs7
Select patid, address1, address2, address3, city, state, zip, zip_four,countycode,county,startdate
, Case enddate when '2100-01-01' then NULL else Convert(Varchar(23), enddate, 121) end enddate
into #addrs7
from #addrs6
--select * from #addrs7

--Last Update for those with Adresses with 4 digit Counties--126
Update #Addrs7
Set address1='', address2='', address3='missing', city='Wheat Ridge', state='CO', zip='80033', county='JEFFERSON'
where len(CountyCode)>3


--final format
TRUNCATE TABLE STAGING.Census_Location
Insert into STAGING.Census_Location
select  id.person_ID, cast(startdate as date) startdate, Cast(enddate as date) enddate,address1, address2, address3, city, state, zip, Zip_Four, County
from #addrs7 t
 inner join Chords.Chords_ID id on t.patid = id.mrn

 
 --and id.PERSON_ID='JC0000039286'
--order by patid, startdate desc, enddate desc--195592, distinct 195592

END
GO
