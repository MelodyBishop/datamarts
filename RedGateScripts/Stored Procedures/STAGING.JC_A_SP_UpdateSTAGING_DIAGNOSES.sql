SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:	Compiled from code by Benjamin Pacheco & MaryS
-- Create date: 01/31/2018
-- Description:	Stored Procedure to populate the CHORDS.DIAGNOSES table.
-- CHANGES:   Migrated to JCVS2.CHORDS 4/17/2018 and updated for CHORDS - MB
	-- Overriding null encounters to be excluded
	-- Overriding null DX and OrigDX with '' empty string and excluding
	-- Long DX with [DSM] need to be trimmed to fit in 10 Character field
	-- Using ~DX logic from Service Avatar
	--Overriding NULL providers
	--Updating 6/1/2018 for a complete join against our set of providers
	--Updating to remove any DX prior to 10/1/2016
	--Updating to remove date constraint that DX happens prior to a note on the Episode.  
	--60011074 is one of many examples of earlier note than DX date
/*
PER CHORDS DATA MODEL MANUAL DIAGNOSES
This table lists all diagnoses associated with the encounters in the ENCOUNTER table. 
A record is a diagnosis code/original diagnosis code/diag provider combination unique to the index variable ENC_ID combination. 
The index variable ENC_ID uniquely identifies each encounter and is used to link the ENCOUNTER file to the both the DIAGNOSIS 
and PROCEDURE files. Changes in diagnosis coding from ICD-9 to ICD-10 in October 2015 are expected. Data partners should not include 
the problem list or chief complaint as a source of diagnoses. Also, do not include admitting diagnosis for inpatient stays.
Inclusions: Include all diagnoses associated with each encounter, primary and otherwise. Only final diagnoses should be included. 
Exclusions: Problem list, chief complaint and admitted diagnoses.
*/
-- =============================================
--Exec  [STAGING].[JC_A_SP_UpdateSTAGING_Diagnoses]
CREATE PROCEDURE [STAGING].[JC_A_SP_UpdateSTAGING_DIAGNOSES]

AS
BEGIN
SET NOCOUNT ON;
IF OBJECT_ID('tempdb.dbo.#temp_billtx_cwpat_notes', 'U') IS NOT NULL --214760
DROP TABLE dbo.#temp_billtx_cwpat_notes; 
select txhist.id , cw.blank_row_id , txhist.patid,txhist.facility,txhist.episode_number,
       cw.patid as cw_patid,    txhist.date_of_service,   cw.date_of_service as cw_date_of_serv, cw.service_start_time as cw_serv_start_time,
   	    txhist.medical_diag_1_icd10_code,  txhist.medical_diag_1_icd10_value		,practitioner_id
into #temp_billtx_cwpat_notes
from avatardw.SYSTEM.billing_tx_history txhist
inner join avatardw.cwssystem.cw_patient_notes cw
   on txhist.JOIN_TO_TX_HISTORY = cw.JOIN_TO_TX_HISTORY 
   and txhist.patid = cw.patid
   and txhist.facility = cw.facility
   and txhist.EPISODE_NUMBER=cw.EPISODE_NUMBER
 left join (Select patid, episode_number,program_code from AVATARDW.SYSTEM.View_Episode_Summary_Current  
				union 
			Select patid, episode_number,program_code from AvatarDW.system.View_Episode_Summary_Discharge) esc
 on txhist.patid = esc.patid 
 and txhist.episode_number = esc.episode_number
where txhist.facility = '1' 
  and (txhist.date_of_service < GetDate() or txhist.date_of_service is null)
  and txhist.date_of_service>='10/1/2016' 
  and txhist.medical_diag_1_icd10_code <>''
  and cw.draft_final_code='F'
  and practitioner_id not in (999999,999998)--225954 5/8/2018
  and esc.program_code = 7000
  and txhist.medical_diag_1_icd10_code <>'NOT FOUND'
  and txhist.medical_diag_1_icd10_code not like '%[DSM]%'
-- and txhist.patid in (10003230,30286270)

 --select * from #temp_billtx_cwpat_notes order by patid, date_of_service

--Last step 214780--~442621
--update note information with rank and type of diagnosis
IF OBJECT_ID('tempdb.dbo.#temp_services_notedx', 'U') IS NOT NULL 
drop table  #temp_services_notedx 
select t.id, t.patid, t.date_of_service, t.episode_number,t.facility
, t.medical_diag_1_icd10_code, t.medical_diag_1_icd10_value,practitioner_id,note_dx.ICD_code
, ISNULL(note_dx.diagnosis_status_value,'') as note_dxtype
, ISNULL(note_dx.ranking_value,'') as note_dxrank
into #temp_services_notedx 
from #temp_billtx_cwpat_notes  t
left join  
	(	select dxe.patid, dxe.facility, dxe.episode_number, dxe.diagnosis_status_value, dxe.ranking_value,dxe.icd_code ,dxr.data_entry_date
		from avatardw.system.client_diagnosis_entry dxe
		  inner join avatardw.system.client_diagnosis_record dxr
			on dxe.diagnosisrecord = dxr.id
			--where dxe.patid=60011074
	) note_dx

	on note_dx.patid = t.patid 
	and note_dx.facility = t.facility
	--Removing this since it excludes folks where notes were entered on Episode, but prior to the DX entry
	--and t.date_of_service>= note_dx.data_entry_date
WHERE note_dx.diagnosis_status_value not in ('Working','Resolved','Void','No Entry') --Skip Voids, Resolved, Working and Nulls
 	and note_dx.episode_number = t.episode_number
--select * from #temp_services_notedx order by 6 desc

--get all dx --More rows 860,383
IF OBJECT_ID('tempdb.dbo.#temp_Services_demog_DX', 'U') IS NOT NULL 
Drop table #temp_Services_demog_DX
select distinct id, t.PATID , t.episode_number,  date_of_service, practitioner_id,medical_diag_1_icd10_code, medical_diag_1_icd10_value
	,note_dxtype, note_dxrank,dx_select.icd_code as lk_joindx, dx_select.diagnosis_status_value as lk_dxtype
	, dx_select.ranking_value as lk_dxrank -- If no diagnosis on note, lookup diagnosis again
into #temp_Services_demog_DX
from #temp_services_notedx  t
  LEFT JOIN (select dxe.patid, dxe.icd_code, dxe.diagnosis_status_value, dxe.ranking_value,date_of_diagnosis,dxe.episode_number
               from avatardw.system.client_diagnosis_entry dxe
                 inner join
                    avatardw.system.client_diagnosis_record dxr
                  on dxe.diagnosisrecord = dxr.id
               where dxe.billing_order <> ' '  -- skip VOIDED Dx 
                 and diagnosis_status_code <> 5  -- change 'skip VOID DX 
               --  and dxe.ranking_code = '1'  -- primary dx only  VERIFY THIS ISN't INTERFERRING 5/4/2018 MB
			   and dxe.icd_code <>'NOT FOUND'
			   ) dx_select  
			   on dx_select.patid = t.patid
			   and dx_select.episode_number = t.EPISODE_NUMBER
--select * from #temp_Services_demog_DX order by 10 desc

--Make distinct--443221
IF OBJECT_ID('tempdb.dbo.#temp_round1_format', 'U') IS NOT NULL 
drop table #temp_round1_format
select distinct id, patid, episode_number,   date_of_service, practitioner_id,
  lk_joindx	   , lk_dxtype	   , lk_dxrank     
into #temp_round1_format
from #temp_Services_demog_DX t
--select * from #temp_round1_format order by 2,4,1,8

--Create table of ICD9/10s
SELECT distinct 
ICD10CODEPRI AS ICD10,
ICD9CODEPRI as ICD9
into #tempDXConvert
FROM TIER.dbo.FD__DIAGNOSIS_DETAIL
--select * from #tempDXConvert where ICD9='292.9'
--select ICD9 ,Count(*) Ctr from #tempDXConvert group by ICD9 order by 2 desc

--Insert AV diagnoses to CHORDS STAGING--260520
TRUNCATE TABLE CHORDS.Staging.Diagnoses
Insert into CHORDS.STAGING.Diagnoses

select 
 Replace(r2.id,MRN,id.Person_ID) Enc_ID,
Coalesce(ICD10,REPLACE(lk_joindx,'[DSM]','')) DX,
Date_OF_Service ADATE,
ISNULL(p.PROVIDER,'JC9999999999') DIAGPROVIDER,--a few provider IDs do not match staff in either TIer or AV
id.PERSON_ID,
ISNULL(e.ENCTYPE,'OE') ENCTYPE,
ISNULL(p.PROVIDER,'JC9999999999') PROVIDER,
Coalesce(ICD10,REPLACE(lk_joindx,'[DSM]','')) ORIGDX,
'10' DX_CODETYPE,
'X' PRINCIPAL_DX,
Case lk_dxrank when  'Primary' then 'P' else 'S' end PRIMARY_DX
From #temp_round1_format r2
inner join (Select staff_id, provider from STAGING.CHORDS_PROVIDER Union select staff_id, provider from CHORDS.CHORDS_Provider) p 
on Cast(p.staff_id as int)=Cast(r2.practitioner_id as int)
inner join  (Select mrn, person_ID from chords.chords_ID  UNION select mrn, person_ID from STAGING.CHORDS_ID UNION select mrn, person_ID from STAGING.CHORDS_ID_TIER)id
on r2.patid = id.mrn
inner join STAGING.Encounters e  --not allowing null ENC_IDs
on Replace(r2.id,MRN,id.Person_ID) = e.Enc_ID
--and e.adate
--order by Person_ID,Adate,Enc_ID, Primary_DX
left join #tempDXConvert c
on c.ICD9 = REPLACE(lk_joindx,'[DSM]','')
order by Len(lk_joindx) desc
END
GO
