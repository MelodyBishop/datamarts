SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--
--execute [CHORDS].[JC_A_SP_UpdateCHORDS_PROVIDER_SPECIALTY]
-- =============================================
-- Author: Melody B
-- Create date: 06/5/2018
-- Modified:   
-- Description:    Adds new Provider ID to chords.CHORDS_PROVIDER for Tier & AV every run for every provider
--					We have deduplicated based on (Lastname, Firstname MiddleInit) after removing spaces

-- =============================================
CREATE PROCEDURE [CHORDS].[JC_A_SP_UpdateCHORDS_PROVIDER_SPECIALTY]
AS
BEGIN
SET NOCOUNT ON;
Insert into CHORDS.[PROVIDER_SPECIALTY]([PROVIDER], [SPECIALTY], [SPECIALTY2], [SPECIALTY3], [SPECIALTY4], [PROVIDER_TYPE], [PROVIDER_BIRTH_YEAR], [PROVIDER_GENDER], [PROVIDER_RACE], [PROVIDER_HISPANIC], [YEAR_GRADUATED])

select sub.provider,sub.Specialty,sub.Specialty2,sub.Specialty3,sub.Specialty4,sub.Provider_Type,sub.Provider_Birth_Year,sub.Provider_Gender ,sub.Provider_Race,sub.Provider_Hispanic,sub.Year_Graduated from 
(Select
provider
,Specialty
,Specialty2
,Specialty3
,Specialty4
,Provider_Type
,Provider_Birth_Year
,Provider_Gender 
,Provider_Race
,Provider_Hispanic
,Year_Graduated
 from  STAGING.Provider_Specialty
) sub
left join chords.provider_specialty p
on p.provider = sub.provider 
where p.provider is null
END
GO
