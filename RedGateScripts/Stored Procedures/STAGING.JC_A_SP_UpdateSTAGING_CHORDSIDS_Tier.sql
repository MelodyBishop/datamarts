SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Begin Transaction
--execute [STAGING].[JC_A_SP_UpdateSTAGING_CHORDSIDS_Tier]
--commit transaction
--rollback transaction 

-- =============================================
-- Author:		Melody B
-- Create date: 5/1/2018
-- Creating copy for Tier clients
-- modified: 5/17/2018 MB To remove CCC and Non JCMH programs/deprtament entries
-- =============================================
CREATE PROCEDURE [STAGING].[JC_A_SP_UpdateSTAGING_CHORDSIDS_Tier]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

--prescreen for existing MRN
if OBJECT_ID('tempdb.dbo.#IDs','U') is not null DROP table #IDs;
Select MRN, person_id 
into #IDs 
FROM (Select mrn,person_id from chords.CHORDS_ID 
		UNION Select MRN,person_id FROM STAGING.CHORDS_ID
		UNION Select MRN ,person_id FROM STAGING.CHORDS_ID_TIER
		) mrn

--Gathers current TIER PATIDs that are not represented in AV
if OBJECT_ID('tempdb.dbo.#CHORDSpatidsTier','U') is not null DROP table #CHORDSpatidsTier;
SELECT DISTINCT 
	pdh.clientkey 
into 	#CHORDSpatidsTier
From 
	 JCMH.dbo.JCMH__SERVICE_TIER pdh  --This table should be final copy
	 left join #IDs 
	 on pdh.clientkey = #IDs.MRN
WHERE 
	substring(pdh.namel,1,5) <> 'test,'
	and pdh.namel not like '%error%' --adding screen for error
	and #IDs.mrn is null 
	and pdh.clientkey is not null
	and pdh.datesvc >='1/1/2011'
	and pdh.pgmnum not in (9900,9901,1404,2407,3000,5405)
	and deptname not in ('CCC_JCMH', 'Administration','Non JCMH Provider')
	and deptname is not NULL--7378 without, 7281 with
	and DOB is not null --excluding null birthdays to avoid making one up, once they have a bday they will be included

	--67513 before join to staging
	--select * from #CHORDSpatidsTier c left join #ids i on c.clientkey=i.mrn where i.mrn is null order by 1
	--select * from #IDs left join order by 1



if OBJECT_ID('tempdb.dbo.#Clients','U') is not null DROP table #Clients;
SELECT DISTINCT 
	#CHORDSpatidsTier.[ClientKey], 
	ROW_NUMBER() OVER (ORDER BY #CHORDSpatidsTier.clientkey) as nRow
into 
	#clients
From 
	#CHORDSpatidsTier
LEFT JOIN #IDs id
	on #CHORDSpatidsTier.clientkey=id.mrn
	where id.mrn is null
	--select * from #clients order by 2

if OBJECT_ID('tempdb.dbo.#LISTIDs','U') is not null DROP table #LISTIDs;
Select distinct mrn,person_id 
	into #ListIDs
	from #IDs sub
--select * from #IDs
--select * from #ListIDS

--Begin Transaction
Declare @CntUp int;
Declare @consumercount int;
Declare @chordsid varchar(12);
Declare @consumerid int;

set @CntUp = 1;
set @consumercount = (SELECT COUNT(*) FROM #clients);

--replacing with logic to ensure the new person_id is not duplicate
While @CntUp <= @consumercount
BEGIN
	SET @consumerid = (SELECT clientkey FRom #clients Where nRow = @CntUp);
		
	IF (SELECT PERSON_ID FROM #ListIDs WHERE PERSON_ID = @chordsid) IS NULL
		BEGIN
			SET @chordsid = 'JC' + RIGHT(CAST(CAST(CAST(NEWID() AS VARBINARY) AS BIGINT) AS VARCHAR(30)),10);
						insert into STAGING.CHORDS_ID_TIER ([MRN], [PERSON_ID])	SELECT @consumerid, @chordsid
			SET @CntUp = @CntUp + 1;
			print @chordsid + ' ' + Cast(@consumerid as varchar)
		END 
	ELSE --Try again without incrementing the counter
		BEGIN
			SET @CntUp = @CntUp
		END
END
END
GO
