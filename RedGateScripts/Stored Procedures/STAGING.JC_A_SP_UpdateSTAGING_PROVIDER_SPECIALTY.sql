SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--
--execute [STAGING].[JC_A_SP_UpdateSTAGING_PROVIDER_SPECIALTY]
-- =============================================
-- Author: Melody B
-- Create date: 06/6/2018
-- Modified:   
-- Description:    Adds new Provider ID to STAGING.CHORDS_PROVIDER for Tier & AV every run for every provider
-- =============================================
CREATE PROCEDURE [STAGING].[JC_A_SP_UpdateSTAGING_PROVIDER_SPECIALTY]
AS
BEGIN
SET NOCOUNT ON;

TRUNCATE TABLE STAGING.PROVIDER_SPECIALTY

Insert into STAGING.PROVIDER_SPECIALTY

Select Distinct 
provider
,'MH' Specialty
,NULL Specialty2
,NULL Specialty3
,NULL Specialty4
,'888' Provider_Type
,NULL Provider_Birth_Year
,'U' Provider_Gender -- u unknown default
,'UN' Provider_Race--un unknown default
,'U' Provider_Hispanic--unknown default
,NULL Year_Graduated
 from  STAGING.CHORDS_PROVIDER p
 order by provider
END
GO
