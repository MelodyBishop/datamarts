SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--Begin Transaction
--execute [STAGING].[JC_A_SP_UpdateStaging_CHORDSIDS]
--commit transaction
--rollback transaction 

-- =============================================
-- Author:		David R. Gray
-- Create date: 12/28/2017
-- Modified:    12/28/2017 - David R. Gray - Added logging
-- Description:	Adds new CHORDS ID to chords.CHORDS_ID
-- 4/17/2018 MB Moved to 10.11.0.5 schema change 
-- 5/9/2018 MB Removing collation and cleanup extra process
-- 5/14/2018 MB Changing order of operations on Person generator 
	--in case same ID is made we run through the process again until the ID is unique
--5/17/2018 MB Making selection more robust to exclude CCC/WM or Prescreening only clients
--Removing
-- =============================================
CREATE PROCEDURE [STAGING].[JC_A_SP_UpdateSTAGING_CHORDSIDS]
	
AS
BEGIN

SET NOCOUNT ON;

if OBJECT_ID('tempdb.dbo.#CHORDSpatids','U') is not null DROP table #CHORDSpatids;
if OBJECT_ID('tempdb.dbo.#clients','U') is not null DROP table #clients;
SELECT DISTINCT 
	d.patid 
into 	#CHORDSpatids
From 	AvatarDW.System.patient_demographic_history d
inner join (Select patid from avatardw.system.view_episode_summary_current 
				 where program_code in (7000) 
					and c_admit_type_of_code=1 
					and Patient_name not like 'Test,%'--14184
					and Patient_name not like 'GROUP,%'
					and Patient_name not like 'ZZZ%'
			UNION
			Select distinct patid from avatardw.system.view_episode_summary_discharge esd 
				where program_code in (7000) 
					and disc_type_of_code not in (13,17)--cc/wm--37558
					and Patient_name not like 'Test,%'--14184
					and Patient_name not like 'GROUP,%'
					and Patient_name not like 'ZZZ%'
			) ep
on d.patid = ep.patid		

WHERE 
	(upper(d.patient_name) not like 'TEST,%' 
	and upper(d.patient_name) not like 'GROUP%'
	and upper(d.patient_name) not like 'ZZZ%')
	and date_of_birth is not null
	and d.patid not in (Select DISTINCT [MRN] FROM chords.CHORDS_ID union sELECT DISTINCT MRN FROM staging.chords_id)

SELECT DISTINCT 
		#CHORDSpatids.[patid], 
		ROW_NUMBER() OVER (ORDER BY #CHORDSpatids.[patid]) as nRow
into 	#clients
From 	#CHORDSpatids
WHERE 	#CHORDSpatids.patid not in (Select DISTINCT [MRN] FROM chords.CHORDS_ID);
--select * from #Clients order by 2

if OBJECT_ID('tempdb.dbo.#IDs','U') is not null DROP table #IDs;
Select mrn,person_id 
	into #IDs
	from (Select mrn,person_id from  chords.CHORDS_ID 
			union 
			select mrn,person_id  from STAGING.CHORDS_ID 
			union 
			select mrn ,person_id from STAGING.CHORDS_ID_TIER) sub
--select * from #IDs

Declare @CntUp int;
Declare @consumercount int;
Declare @chordsid varchar(12);
Declare @consumerid int;

set @CntUp = 1;
set @consumercount = (SELECT COUNT(*) FROM #clients);

While @CntUp <= @consumercount 
	BEGIN

		SET @consumerid = (SELECT patid FRom #clients Where nRow = @CntUp);
		IF (SELECT mrn FROM #IDs WHERE mrn = @consumerid) IS NULL
			BEGIN
				SET @chordsid = 'JC' + RIGHT(CAST(CAST(CAST(NEWID() AS VARBINARY) AS BIGINT) AS VARCHAR(30)),10);
				IF (SELECT [PERSON_ID] FROM #IDs WHERE [PERSON_ID] = @chordsid) IS NULL
					BEGIN
						insert into STAGING.CHORDS_ID ([MRN], [PERSON_ID])	SELECT @consumerid, @chordsid
						SET @CntUp = @CntUp + 1;
						select Cast(@Chordsid as varchar) + 'is inserted'
					END
				ELSE
					BEGIN
					set @cntup = @cntUp
					END
			END
		ELSE 
			set @CntUp=@CntUp+1
	END
END
GO
