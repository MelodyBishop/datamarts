SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author Melody B
-- Created 5/7/2018
-- Capture TIER only Diagnosis for CHORDS db
-- The diagnosis (PrimDIagKey) is grabbed from the chargeslip (BIL_Chargeslips).  

-- Modified 5/18/2018 to add dimension of time to the DX codes and cross apply to all encounters
-- modified 6/5/2018 MB to add comparison to all IDs in staging and production

--CHORDS DEFINITION
--This table lists all diagnoses associated with the encounters in the ENCOUNTER table. A record is a diagnosis code/original diagnosis
-- code/diag provider combination unique to the index variable ENC_ID combination. The index variable ENC_ID uniquely identifies each encounter 
-- and is used to link the ENCOUNTER file to the both the DIAGNOSIS and PROCEDURE files. Changes in diagnosis coding from ICD-9 to ICD-10 in
--  October 2015 are expected. Data partners should not include the problem list or chief complaint as a source of diagnoses. Also, do not 
--  include admitting diagnosis for inpatient stays.
--Inclusions: Include all diagnoses associated with each encounter, primary and otherwise. Only final diagnoses should be included. 
--Exclusions: Problem list, chief complaint and admitted diagnoses.

--exec [STAGING].[JC_A_SP_UpdateSTAGING_Diagnoses_Tier]
-- =============================================
CREATE PROCEDURE [STAGING].[JC_A_SP_UpdateSTAGING_DIAGNOSES_TIER]
AS
BEGIN

IF OBJECT_ID('tempdb.dbo.#temp', 'U') IS NOT NULL Drop table #temp
;With CTE (person_id,clientkey, Start_Date,date_end,rank,dx )
as
(select person_id,clientKey, MIN(Date_Start) as Start_Date,date_end,rank,dx 
from STAGING.JCMH_GetAllDiagnosis_TIER t
inner join (Select mrn, person_id from chords.chords_id union select mrn, person_id from staging.chords_id_tier union select mrn, person_ID from staging.chords_id ) ID on 
ID.mrn = t.clientkey
where date_end is not null
--and clientkey in (23955,30286270) --23955
Group by person_id,clientkey, date_end,rank, dx
)

select e.ENC_ID ,DX,ADATE,PROVIDER DIAGPROVIDER,e.PERSON_ID, ENCTYPE, PROVIDER, DX as ORIGDX, 10 DX_CODETYPE,'X' PRINCIPAL_DX, CASE rank when 'Primary' then 'P' Else 'S' end PRIMARY_DX
into #temp
from  CTE
inner join chords.STAGING.Encounters_TIer e
on CTE.person_ID = e.Person_ID
and e.ADATE>=CTE.Start_Date
where --clientkey in (23955,30286270) and 
e.ADATE between start_date and date_end
order by person_id, enc_id, adate,rank

TRUNCATE TABLE STAGING.DIAGNOSES_Tier
Insert into [STAGING].[Diagnoses_Tier]([ENC_ID], [DX], [ADATE], [DIAGPROVIDER], [PERSON_ID], [ENCTYPE], [PROVIDER], [ORIGDX], [DX_CODETYPE], [PRINCIPAL_DX], [PRIMARY_DX])
Select	ENC_ID,
DX,
ADATE,
DIAGPROVIDER,
Person_ID,
ENCTYPE,
PROVIDER,
ORIGDX,
DX_CODETYPE,
Principal_DX,
PRIMARY_DX 
from #temp
END
GO
