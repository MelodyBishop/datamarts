SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--execute [CHORDS].[JC_A_SP_UpdateCHORDS_DEMOGRAPHICS]
 --=============================================
-- Author: DAVID G
-- Create date: 01/11/2018
-- Modified:   4/17/2018 Melody B
-- Description:    Adds new clients to Staging.CHORDS_Demographics 
-- Moved to CHORDS, updated to leverage history so we can capture latest info for this Level one table, Schema 4/17/2018 - MB
-- THE DEMOGRAPHICS TABLE MUST BE REBUILT EACH TIME TO CAPTURE THE LATEST DEMOGRAPHIC INFORMATION

-- =============================================
CREATE PROCEDURE [CHORDS].[JC_A_SP_UpdateCHORDS_DEMOGRAPHICS]
AS
BEGIN
--PER CHORDS DOCUMENTATION
--The DEMOGRAPHICS table contains basic person-descriptive for individuals found in all other VDW tables and serves the traditional ‘person table’ role. 
--The DEMOGRAPHICS table should reflect a patient’s most recent demographic information. 
--If a self-reported demographic attribute, such as race or primary language changes, this should be updated in the DEMOGRAPHICS table. 
--Thus, the DEMOGRAPHICS table should be rerun when the VDW is updated to reflect any changes in patient demographic information.
--One issue that can arise in the DEMOGRAPHICS table is duplicate patients.
--Duplicate patients should be removed/collapsed from the DEMOGRAPHICS table when they are identified in the healthcare organization.
--If an organization has a known problem with duplicates and no standard process to remove them, please note this on the data dictionary.
--Inclusions: Every PERSON_ID appearing in any other VDW file should appear in DEMOGRAPHICS 
--Exclusions: No individuals are excluded from the DEMOGRAPHICS table 


--Load Table from AV
insert into [CHORDS].DEMOGRAPHICS (
PERSON_ID, MRN, BIRTH_DATE, GENDER, PRIMARY_LANGUAGE, NEEDS_INTERPRETER
, RACE1, RACE2, RACE3, RACE4, RACE5, HISPANIC, SEXUAL_ORIENTATION, GENDER_IDENTITY
)
SELECT 
d.PERSON_ID, d.MRN, d.BIRTH_DATE, d.GENDER, d.PRIMARY_LANGUAGE, d.NEEDS_INTERPRETER
, d.RACE1, d.RACE2, d.RACE3, d.RACE4, d.RACE5, d.HISPANIC, d.SEXUAL_ORIENTATION, d.GENDER_IDENTITY
FROM staging.Demographics d
left join CHORDS.Demographics dem on d.Person_id=dem.person_id 
Where dem.person_id is null

--insert tier based emographics, but only if the AV version is null
insert into [CHORDS].DEMOGRAPHICS (
PERSON_ID, MRN, BIRTH_DATE, GENDER, PRIMARY_LANGUAGE, NEEDS_INTERPRETER
, RACE1, RACE2, RACE3, RACE4, RACE5, HISPANIC, SEXUAL_ORIENTATION, GENDER_IDENTITY
)
SELECT 
d.PERSON_ID, d.MRN, d.BIRTH_DATE, d.GENDER, d.PRIMARY_LANGUAGE, d.NEEDS_INTERPRETER
, d.RACE1, d.RACE2, d.RACE3, d.RACE4, d.RACE5, d.HISPANIC, d.SEXUAL_ORIENTATION, d.GENDER_IDENTITY
FROM chords.STAGING.Demographics_Tier d
left join CHORDS.Demographics dem on d.Person_id=dem.person_id 
Where dem.person_id is null

END
GO
