SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-------------------------------------------------------------------
--Author: Melody B 5/7/2018

--CHORDS DESCRIPTION
--The VITAL_SIGNS table includes various physiological measures taken by health professionals during the clinic visit. 
--The traditional clinical vital signs are body temperature, pulse rate, blood pressure, and respirations. 
--Because of HMORN investigator interest, the VDW VITAL_SIGNS table also includes anthropometry (height, weight, BMI [when pre-calculated], and head circumference).
--There is no unique primary key for the VITAL_SIGNS table. The PERSON_ID and MEASURE_DATE are the only variables required to be non-null for inclusion in the table.
--Inclusions: Include all encounters, even when all of the vitals elements are missing.
--Exclusions: None
--EXECUTE STAGING.JC_A_SP_UpdateSTAGING_VITALS_TIER
-------------------------------------------------------------------
CREATE PROCEDURE [STAGING].[JC_A_SP_UpdateSTAGING_VITALS_TIER]
AS
BEGIN

IF OBJECT_ID('tempdb.dbo.#temp','U') IS NOT NULL DROP TABLE #temp
select distinct
vital_ID,
e.person_ID,
Date_Monitored MEASURE_DATE,
Convert(time,time1) MEASURE_TIME,
MAXX_ENC_ID ENC_ID,
e.ENCType ENCTYPE,
Height_FT HT_Raw,
WEIGHT WT_Raw,
Cast(((12*Height_Ft) + Height_In) as float) Height_IN,
Weight WT,
BMI BMI_RAW,
Diastolic,
Systolic,
Diastolic Diastolic_RAW,
Systolic Systolic_RAW,
'R' BPType,
1 Position,
'' HEAD_CIR_RAW,
RESPIRATION RESPIR_RAW,
TEMPERATURE TEMP_RAW,
PULSE PULSE_RAW
Into #temp
From 
(Select  Person_ID, adate, Max(ENC_ID) MAXX_ENC_ID ,ENCTYPE from STAGING.Encounters_Tier WHERE ENCTYPE <>'TE' Group by Person_ID, adate,ENCTYPE) e 
Left join  
(Select c.person_id,v.* from tier.dbo.fd__vital_signs v
	inner join (Select mrn, person_ID from CHORDS.CHORDS_ID UNION select mrn, person_ID from STAGING.CHORDS_ID UNION select mrn, person_id from STAGING.CHORDS_ID_TIER) c 	on v.clientkey = c.mrn) VV
	on VV.Person_ID = e.person_id
	and VV.Date_Monitored = e.ADATE
left join  STAGING.Encounters ee
on e.person_id = ee.person_id
and e.Maxx_ENC_ID = ee.ENC_ID
WHERE vv.date_monitored >= '2011-01-01 00:00:00.000'
 and Height_FT<100 --do not send measures over 100 inches tall
 and weight<900 --do not send measures over 900 pounds
 and Cast(ISNULL(systolic,0) as int)<=999 --invalid
 and Cast(ISNULL(Diastolic,0) as int)<=999 --invalid
 and time1 is not null

TRUNCATE TABLE STAGING.VITAL_SIGNS_Tier
Set identity_insert STAGING.VITAL_Signs_Tier on
Insert into [STAGING].[Vital_Signs_Tier] 
([VITAL_SIGNS_ID], [PERSON_ID], [MEASURE_DATE], [MEASURE_TIME], [ENC_ID], [ENCTYPE], [HT_RAW], [WT_RAW]
, [HT], [WT], [BMI_RAW], [DIASTOLIC], [SYSTOLIC]
, [DIASTOLIC_RAW], [SYSTOLIC_RAW], [BP_TYPE], [POSITION], [HEAD_CIR_RAW], [RESPIR_RAW], [TEMP_RAW], [PULSE_RAW]
)
select Row_Number() OVER(Order by #temp.person_id, measure_date, measure_time, Enc_ID, ENCTYPE ) ROWNUM
,#temp.person_id, measure_date, measure_time, Enc_ID, ENCTYPE, HT_RAW, WT_RAW,
 Cast(Height_IN as decimal(18,3)) , Cast(WT as decimal(18,3)), BMI_RAW, Cast(ISNULL(Diastolic,0) as Numeric(3,0)) DIASTOLIC
, Cast(ISNULL(Systolic,0) as numeric(3,0)) SYSTOLIC
, ISNULL([DIASTOLIC_RAW],'') [DIASTOLIC_RAW], ISNULL([SYSTOLIC_RAW],'') [SYSTOLIC_RAW], [BPTYPE], [POSITION], ISNULL([HEAD_CIR_RAW],''), ISNULL([RESPIR_RAW],''), ISNULL([TEMP_RAW],''), ISNULL([PULSE_RAW],'')
 from #temp
 left join (Select mrn, person_ID from CHORDS.CHORDS_ID 
			UNION select mrn, person_ID from STAGING.CHORDS_ID 
			UNION select mrn, person_id from STAGING.CHORDS_ID_TIER) d on #temp.person_id = d.person_id
 where d.person_id is not null and Height_IN<100
 --order by 1
Set identity_insert STAGING.VITAL_Signs_Tier off
END 
GO
