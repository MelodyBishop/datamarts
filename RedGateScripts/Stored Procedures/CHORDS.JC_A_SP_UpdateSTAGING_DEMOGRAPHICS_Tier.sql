SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--Begin transaction
--execute [CHORDS].[JC_A_SP_UpdateSTAGING_DEMOGRAPHICS_Tier]
--commit transaction
--rollback transaction 
 --=============================================
-- Author: Melody B
-- Create date: 5/1/2018
-- Description:    Creating Tier data extract
-- Modified: 05/16/2018 MB - Excluding null BirthDay clients
-- =============================================
CREATE PROCEDURE [CHORDS].[JC_A_SP_UpdateSTAGING_DEMOGRAPHICS_Tier]
AS
BEGIN

--PER CHORDS DOCUMENTATION
--The DEMOGRAPHICS table contains basic person-descriptive for individuals found in all other VDW tables and serves the traditional ‘person table’ role. 
--The DEMOGRAPHICS table should reflect a patient’s most recent demographic information. 
--If a self-reported demographic attribute, such as race or primary language changes, this should be updated in the DEMOGRAPHICS table. 
--Thus, the DEMOGRAPHICS table should be rerun when the VDW is updated to reflect any changes in patient demographic information.
--One issue that can arise in the DEMOGRAPHICS table is duplicate patients.
--Duplicate patients should be removed/collapsed from the DEMOGRAPHICS table when they are identified in the healthcare organization.
--If an organization has a known problem with duplicates and no standard process to remove them, please note this on the data dictionary.
--Inclusions: Every PERSON_ID appearing in any other VDW file should appear in DEMOGRAPHICS 
--Exclusions: No individuals are excluded from the DEMOGRAPHICS table 
--PreGather IDS
if OBJECT_ID('tempdb.dbo.#ID','U') is not null DROP table #ID;
Select distinct mrn, person_ID 
into #ID
from 
(Select mrn, person_id from CHORDS.CHORDS_ID 
union 
select mrn, person_id from STAGING.CHORDS_ID 
union  
select mrn, person_id from STAGING.CHORDS_ID_Tier
) id

--PreGather Demographics list
if OBJECT_ID('tempdb.dbo.#temp','U') is not null DROP table #temp;
Select DISTINCT person_id 
into #temp
FROM CHORDS.STAGING.DEMOGRAPHICS_Tier UNION Select DISTINCT person_id FROM CHORDS.STAGING.DEMOGRAPHICS--58116+16710
--select * from #temp order by  person_ID

--Gathers current TIER PATIDs that are not represented in AV from Staging table
if OBJECT_ID('tempdb.dbo.#CHORDSTier','U') is not null DROP table #CHORDSTier;
SELECT DISTINCT 
	pdh.mrn , person_id
into 
	#CHORDSTier
From 
	 STAGING.CHORDS_ID_Tier pdh--18253
--select * from #CHORDSTier where mrn=22631579	 

--Gather PATID MOST RECENT DEMOGRAPHICS INFORMATION
if OBJECT_ID('tempdb.dbo.#tempDemo','U') is not null DROP table #tempDemo;
Select c.*
into #TempDemo
From
#CHORDSTier t inner join 
(Select * from Tier.[dbo].[FD__CLIENTS]) c
on t.mrn = c.clientkey
--select * from #tempDemo where dob is null 
  
if OBJECT_ID('tempdb.dbo.#CHORDSTemp','U') is not null DROP table #CHORDSTemp;
select distinct
--	patient_current_demographics.patient_name,
	m.PERSON_ID,
	t.clientkey as MRN,
	t.DOB as BIRTH_DATE, --sent this to David for REview TBD
	Case t.Gender 	
		When 'M' then 'M'
		When 'F' then 'F'
		Else 'U' end as  GENDER,
--select distinct sexual_orientation from #tempdemo
	Case  l.Description 
		when 'English' Then 'ENG' 
		when 'Spanish' then 'SPA' 
		When 'Vietnamese' then 'VIE'
		When 'Russian' then 'RUS'
		When 'Chinese' then 'CHI'
		When 'French' then 'FRE'
		When 'German' then 'GER'
		When 'Japanese' then 'JPN'
		When 'Russian' then 'RUS'
		When 'Turkish' then 'TUR'
		else 'UNK' end as PRIMARY_LANGUAGE,

	NULL other_race_code,
	NULL other_race_value_long,
	'U' as NEEDS_INTERPRETER,

 	Case eth_ai_an when 'T' then 'IN'
		Else Case eth_asian when 'T' then 'AS'
			Else Case eth_B_AA when  'T' then 'BA'
				Else Case eth_nh_pi when 'T' then'HP'
					Else Case Eth_W_C when 'T' then 'WH' 
						Else Case eth_oth_eth when 'T' then 'UN'
					else 'UN'	End
					End
				End
			 End
		 END
	  End RACE1,
	'UN' as RACE2,
	'UN' as RACE3,
	'UN' as RACE4,
	'UN' as RACE5,

	case 
	when t.Hispanic = 'T' then 'Y'
	when t.Hispanic = 'F' then 'N'
	--More if needed
	else 'U'
end as HISPANIC,

	'UN' as SEXUAL_ORIENTATION, --This field is always null
	'UN' as GENDER_IDENTITY
into #CHORDSTemp
from 
	#tempDemo t
	left join tier.dbo.LT__Language l on t.primarylanguagecode = l.code
	left join CHORDS.STAGING.CHORDS_ID_Tier m on t.clientkey=m.mrn
--where --mrn=22631579 and
	  -- m.person_id not in (Select person_ID from #temp)
--select * from #CHORDSTEMP where person_id='JC1707451950'

--Reload with latest data at all times
Truncate Table Staging.Demographics_Tier
--select * from staging.demographics_tier

insert into STAGING.DEMOGRAPHICS_Tier
select 
	#id.PERSON_ID,
	c.MRN,
	c.BIRTH_DATE,
	c.GENDER,
	--Could add more so that we assign all 
	c.PRIMARY_LANGUAGE,
	c.NEEDS_INTERPRETER,
	c.RACE1,
	c.RACE2,
	c.RACE3,
	c.RACE4,
	c.RACE5,
	c.HISPANIC,
	c.SEXUAL_ORIENTATION,
	c.GENDER_IDENTITY
from #CHORDSTemp c
inner join #ID
on c.mrn = #id.mrn
where birth_date is not null
--select * from #CHORDSTemp;
--select * from STAGING.DEMOGRAPHICS_Tier order by race1 desc
END
GO
