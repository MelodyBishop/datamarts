SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--execute STAGING.[JC_A_SP_UpdateStaging_ENCOUNTER] --Approx RunTime:  Less than 1 minute
--/* ************************************************************************************************************************************************************************************************************************************************ 
--Name:		    JC_A_SP_Create_CHORDS_Encounter_Avatar
--Author:		Mary Steffeck, JCMH
--Created:	    March 2018
--Description:  This proc will read all chargeslips in TIER and create CHORDS encounters.
--              
--Modified: 4/19/2018 to point to staging to bypass FKey constraint issues while building tables - MB    
--Added masking of the Patid portion of the Enc_ID to show the Person_ID
--6/8/2018 MB Changed begin date to 1/1/2011 to Capture ~100 TIER encounters
--6/9/2018 Changing JC9999999999 to UNK per CHORDS Specification

--CHORDS DOCUMENTATION:
-- The ENCOUNTERS table should include records from encounters between patients and medical personnel indexed by ENC_ID and PERSON_ID.
-- All available encounters with a medical provider should be included. Medical providers include: physicians, nurse practitioners,
-- registered nurses, lab technicians, social workers, etc.—generally, people licensed to provide medical care and closely related services.
-- Recurring visits to the same clinicians on the same day should be maintained as separate encounters if possible. 
-- Multiple encounters to the same provider on the same day are allowed if that is the truth in the source data and have unique ENC_ID values.
-- Inpatient hospital stays lasting multiple days should be represented as one encounter. 
-- FACILITY_CODE is included in the ENCOUNTERS table for data partners to populate with any site-specific clinic or department identifier 
-- that can aid data validation and preserve data lineage. 

--Modified to skip PREPAY from both Notes and Billing - this is our first failure- has something changed MB
--*************************************************************************************************************************************/
CREATE  procedure [STAGING].[JC_A_SP_UpdateSTAGING_ENCOUNTER]
as

--select * from STAGING.Encounters
Truncate Table STAGING.Encounters

declare @begindate datetime
set @begindate = '1/1/2011'

----delete from STAGING.Encounters
----select * from STAGING.encounters
Insert into STAGING.Encounters
Select 
Replace(enc_ID,patid,person_id) Enc_ID 
,Person_ID
,ADate
,DDate
,Provider
,Enc_Count
,Drg_Version
,Drg_Value
,EncType
,Encounter_Subtype
,location_code
,Discharge_Disposition
,Discharge_Status
,Admitting_Source
,Department

From
(
select 
	   txhist.id as enc_id,
	   cw.patid,
       ID.Person_ID as person_id --allows us to validate against STAGING too at this point
	   ,Cast(txhist.date_of_service as date) as adate  -- format to YYYYMMDD
       ,NULL as ddate
       ,ISNULL((select distinct Provider from (Select staff_id,provider from [CHORDS].[CHORDS].[CHORDS_PROVIDER] 
										UNION Select staff_id,provider from [CHORDS].STAGING.[CHORDS_PROVIDER] 
										) sub
										where convert(int,[Staff_ID]) = convert(int,txhist.PROVIDER_ID)),'UNK') as 'Provider'
	   ,1 as Enc_Count
       ,NULL as Drg_Version
       ,NULL as Drg_Value
      , case
			when cw.service_charge_code in (407,930,931,932) then 'TE'  -- phone procedures
			 else 'OE'  -- other encounter
		end as enctype
       ,'OT' as encounter_subtype  -- other non-hospital
       ,cw.location_code --as Facility_Code
       ,'U' as discharge_disposition
       ,'UN' as discharge_status
       ,'UN' as admitting_source
       ,'MH' as department
from Avatardw.SYSTEM.billing_tx_history txhist
  inner join Avatardw.cwssystem.cw_patient_notes cw
			 on txhist.JOIN_TO_TX_HISTORY = cw.JOIN_TO_TX_HISTORY 
			 and txhist.patid = cw.patid
			 and txhist.facility=cw.facility
			 and txhist.episode_number = cw.episode_number
inner join (Select distinct c.person_id,c.mrn from 
				(Select mrn, person_id --147310
				from CHORDS.CHORDS_ID
				union 
				SELECT mrn,  person_ID from STAGING.CHORDS_ID
				union 
				SELECT mrn,  person_ID from STAGING.CHORDS_ID_tier) c
				inner join
				(Select person_id, mrn from  CHORDS.staging.demographics 
				union 
				Select person_id, mrn from  CHORDS.staging.demographics_tier
				) d
				on c.person_ID=d.person_ID--142205 only
) ID
on id.mrn = cw.patid
where txhist.facility = '1' 
  and txhist.date_of_service >= '1/1/2011'--@begindate
  and UPPER(substring(cw.practitioner_name,1,4)) <> 'TEST'  -- omit TEST clinicians
  and UPPER(substring(txhist.v_patient_name,1,5))<> 'TEST,'  -- omit TEST clients
  and substring(txhist.v_patient_name,1,11) <> 'GROUP,GROUP'  -- omit TEST clients
  and txhist.v_patient_name not like 'ZZZ%'  -- omit TEST clients
  and txhist.service_code<>'PREPAY'
  and cw.service_charge_code<>'PREPAY'
) sub
 --rowcount without join to CHORDS.CHORDS_ID = 615177 30 M
GO
