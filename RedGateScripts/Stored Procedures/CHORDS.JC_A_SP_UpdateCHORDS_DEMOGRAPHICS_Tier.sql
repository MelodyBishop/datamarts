SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--Begin transaction
--execute [CHORDS].[JC_A_SP_UpdateCHORDS_DEMOGRAPHICS_Tier]
--commit transaction
--rollback transaction 
 --=============================================
-- Author: Melody B
-- Create date: 5/1/2018
-- Modified:   
-- Description:    Creating Tier data extract
-- =============================================
CREATE PROCEDURE [CHORDS].[JC_A_SP_UpdateCHORDS_DEMOGRAPHICS_Tier]
AS
BEGIN

--PER CHORDS DOCUMENTATION
--The DEMOGRAPHICS table contains basic person-descriptive for individuals found in all other VDW tables and serves the traditional ‘person table’ role. 
--The DEMOGRAPHICS table should reflect a patient’s most recent demographic information. 
--If a self-reported demographic attribute, such as race or primary language changes, this should be updated in the DEMOGRAPHICS table. 
--Thus, the DEMOGRAPHICS table should be rerun when the VDW is updated to reflect any changes in patient demographic information.
--One issue that can arise in the DEMOGRAPHICS table is duplicate patients.
--Duplicate patients should be removed/collapsed from the DEMOGRAPHICS table when they are identified in the healthcare organization.
--If an organization has a known problem with duplicates and no standard process to remove them, please note this on the data dictionary.
--Inclusions: Every PERSON_ID appearing in any other VDW file should appear in DEMOGRAPHICS 
--Exclusions: No individuals are excluded from the DEMOGRAPHICS table 



--Gathers current TIER PATIDs that are not represented in AV from Staging table
if OBJECT_ID('tempdb.dbo.#CHORDSTier','U') is not null DROP table #CHORDSTier;
SELECT DISTINCT 
	pdh.mrn , person_id
into 
	#CHORDSTier
From 
	 STAGING.CHORDS_ID_Tier pdh
	 

--Gather PATID MOST RECENT DEMOGRAPHIS INFORMATION
if OBJECT_ID('tempdb.dbo.#tempDemo','U') is not null DROP table #tempDemo;
Select c.*
into #TempDemo
From
#CHORDSTier t inner join 
(Select * from Tier.[dbo].[FD__CLIENTS]  ) c
on t.mrn = c.clientkey
--select * from #tempDemo
 
 
if OBJECT_ID('tempdb.dbo.#CHORDSTemp','U') is not null DROP table #CHORDSTemp;

select 
--	patient_current_demographics.patient_name,
	m.PERSON_ID,
	t.clientkey as MRN,
	t.DOB as BIRTH_DATE,
	Case t.Gender 	
		When 'M' then 'M'
		When 'F' then 'F'
		Else 'U' end as  GENDER,
--select distinct sexual_orientation from #tempdemo
	Case  l.Description 
		when 'English' Then 'ENG' 
		when 'Spanish' then 'SPA' 
		When 'Vietnamese' then 'VIE'
		When 'Russian' then 'RUS'
		When 'Chinese' then 'CHI'
		else 'UNK' end as PRIMARY_LANGUAGE,

	NULL other_race_code,
	NULL other_race_value_long,
	'U' as NEEDS_INTERPRETER,
	'UN' as RACE1,
	'UN' as RACE2,
	'UN' as RACE3,
	'UN' as RACE4,
	'UN' as RACE5,

	case 
	when t.Hispanic = 'T' then 'Y'
	when t.Hispanic = 'F' then 'N'
	--More if needed
	else 'U'
end as HISPANIC,

	'UN' as SEXUAL_ORIENTATION, --This field is always null
	'UN' as GENDER_IDENTITY
into #CHORDSTemp
from 
	#tempDemo t
	left join tier.dbo.LT__Language l on t.primarylanguagecode = l.code
	left join CHORDS.STAGING.CHORDS_ID_Tier m on t.clientkey=m.mrn
where
	   m.person_id not in (Select DISTINCT person_id FROM CHORDS.STAGING.[DEMOGRAPHICS_Tier]);
--select * from #CHORDSTEMP

/*
update c
set c.RACE5 =
case 
	when t.current_race = 'Native Hawaiian' then 'HP'
	when t.current_race = 'Other Pacific Islander' then 'HP'
	when t.current_race = 'Samoan' then 'HP'
    when t.current_race = 'Guamanian/Chamorro' then 'HP'
    when t.current_race = 'American Indian/Alaska Native' then 'IN'
    when t.current_race = 'Vietnamese' then 'AS'
    when t.current_race = 'Japanese' then 'AS'
    when t.current_race = 'Chinese' then 'AS'
    when t.current_race = 'Korean' then 'AS'
    when t.current_race = 'Asian Indian' then 'AS'
    when t.current_race = 'Other Asian' then 'AS'
    when t.current_race = 'White/Caucasian' then 'WH'
    when t.current_race = 'Black/African-American' then 'BA'
	else 'UN' 
end 
from #CHORDSTemp c
	inner join  #temp_split_present_concern t on t.mrn = c.MRN and t.level = 5;
*/

Truncate Table Staging.Demographics_Tier
insert into STAGING.DEMOGRAPHICS_Tier
select 
	id.PERSON_ID,
	c.MRN,
	BIRTH_DATE,
	GENDER,
	--Could add more so that we assign all 
	PRIMARY_LANGUAGE,
	NEEDS_INTERPRETER,
	RACE1,
	RACE2,
	RACE3,
	RACE4,
	RACE5,
	HISPANIC,
	SEXUAL_ORIENTATION,
	GENDER_IDENTITY
from #CHORDSTemp c
inner join (Select mrn, person_id from CHORDS.CHORDS_ID union select mrn, person_id from STAGING.CHORDS_ID) id
on c.mrn = id.mrn
		
--select * from #CHORDSTemp;
--select * from STAGING.DEMOGRAPHICS_Tier
END
GO
