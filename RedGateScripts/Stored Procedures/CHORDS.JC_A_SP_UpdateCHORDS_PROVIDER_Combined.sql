SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--
--execute [CHORDS].[JC_A_SP_UpdateCHORDS_PROVIDER_Combined]
-- =============================================
-- Author: Melody B
-- Create date: 05/3/2018
-- Modified:   
-- Description:    Adds new Provider ID to chords.CHORDS_PROVIDER for Tier & AV every run for every provider
--					We have deduplicated based on (Lastname, Firstname MiddleInit) after removing spaces

-- =============================================
CREATE PROCEDURE [CHORDS].[JC_A_SP_UpdateCHORDS_PROVIDER_Combined]
AS
BEGIN
SET NOCOUNT ON;

--Clean the slate of STAGING AND TEMP TABLES
TRUNCATE TABLE STAGING.CHORDS_PROVIDER
If OBJECT_ID('tempdb.dbo.#CHORDSprovider','U') is not null DROP table #CHORDSprovider;
If OBJECT_ID('tempdb.dbo.#PROVIDER','U') is not null DROP table #PROVIDER;
If OBJECT_ID('tempdb.dbo.#TempName','U') is not null DROP table #TempName;
If OBJECT_ID('tempdb.dbo.#Temp','U') is not null DROP table #Temp;

--ADD TIER STAFF TO TEMP TABLE--2002
Create Table #CHORDSprovider (Provider int, Fullname varchar(100),  MaskedProvider varchar(12))
Insert into  #CHORDSprovider
Select distinct op__DocID  Provider, FullName,'' MaskedProvider  
  from  tier.dbo.fd__STAFF s 
  where op__DocID not in (Select staff_ID from [CHORDS].[CHORDS_PROVIDER])
--select * from #CHORDSprovider order by fullname

--Add AV STAFF TO TEMP TABLE--
Insert into #CHORDSprovider-- 
select DISTINCT r.staff_member_id ,s.name,''
From AvatarDW.SYSTEM.RADplus_users r
inner join avatardw.SYSTEM.staff_current_demographics s
	on s.staffid = r.staff_member_id
WHERE  (upper(r.user_description) not like '%TEST,%' 
	and upper(r.user_description) not like '%GROUP,%' 
	and upper(r.user_description) not like '%ZZZ%')
    and r.[staff_member_id] not in (0, 999999, 999998)
	and Cast(r.staff_member_id as int) not in (Select staff_ID from [CHORDS].[CHORDS_PROVIDER])
--select * from #CHORDSprovider order by fullname

--Remove space from all names in TEMP TABLE--this will be final update table before LOAD to STAGING
Update #ChordsProvider
Set FullName = Replace(Fullname, ' ','')
--select * from #ChordsProvider order by 2
 


--Create temp table to work with unique naming and update #ChordsProvider table
Select distinct FullName 
into #temp 
from #ChordsProvider 	--2233 distinct names
--select * from #temp		   		 
--select * from staging.chords_provider
--select * from #CHORDSPROVIDER

--Create IDS for each of the names
Declare @Name varchar(100);
Declare @provider varchar(12);
While Exists (select top 1 Fullname from #temp)
BEGIN
	Set @Name = (Select top 1 Fullname from #temp)
	SET @provider = 'JC' + RIGHT(CAST(CAST(CAST(NEWID() AS VARBINARY) AS BIGINT) AS VARCHAR(30)),13);
	--compare IDs to live, if not a match with an existing ID then go forward
	IF (SELECT PROVIDER FROM chords.CHORDS_PROVIDER WHERE PROVIDER = @provider) IS NULL
		
			BEGIN
				--print @name
				--print @provider
				Update #CHORDSProvider 
				set MaskedProvider = @Provider where FullName = @name
				Delete from #temp where FullName=@name
			END 
END

--Final Insert to STAGING
Insert into STAGING.CHORDS_PROVIDER 
select distinct  Provider, MaskedProvider, FullName 
From #CHORDSPROVIDER --2820 providers

--Add dummy provider
Insert into STAGING.CHORDS_PROVIDER
Select 0,'UNK','Dummy'

--select * from STAGING.CHORDS_PROVIDER
END
GO
