SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--execute [CHORDS].[JC_A_SP_UpdateCHORDS_ENCOUNTERS]
 --=============================================
-- Author: Melody B
-- Create date: 06/7/2018
-- Description:    Adds new clients to Staging.CHORDS_Encounters 
-- =============================================
CREATE PROCEDURE [CHORDS].[JC_A_SP_UpdateCHORDS_ENCOUNTERS]
AS
BEGIN

insert into [CHORDS].Encounters (
[ENC_ID], [PERSON_ID], [ADATE], [DDATE], [PROVIDER], [ENC_COUNT], [DRG_VERSION], [DRG_VALUE]
, [ENCTYPE], [ENCOUNTER_SUBTYPE], [FACILITY_CODE], [DISCHARGE_DISPOSITION], [DISCHARGE_STATUS], [ADMITTING_SOURCE], [DEPARTMENT]
)

SELECT e.[ENC_ID], e.[PERSON_ID], e.[ADATE], e.[DDATE], e.[PROVIDER], e.[ENC_COUNT], e.[DRG_VERSION], e.[DRG_VALUE]
, e.[ENCTYPE], e.[ENCOUNTER_SUBTYPE], e.[FACILITY_CODE], e.[DISCHARGE_DISPOSITION], e.[DISCHARGE_STATUS], e.[ADMITTING_SOURCE], e.[DEPARTMENT]
FROM staging.Encounters e
left join CHORDS.Encounters enc on e.enc_ID = enc.enc_ID
where enc.enc_id is null

END
GO
