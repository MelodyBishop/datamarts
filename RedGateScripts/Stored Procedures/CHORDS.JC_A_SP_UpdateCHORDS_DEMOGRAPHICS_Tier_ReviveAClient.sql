SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--Begin transaction
--execute [CHORDS].[JC_A_SP_UpdateCHORDS_DEMOGRAPHICS_Tier_ReviveAClient]
--commit transaction
--rollback transaction 
 --=============================================
-- Author: Melody B
-- Create date: 5/1/2018
-- Modified:   
-- Description:    Check for Tier clients re-entering our client base/create a demographic entry for them
-- =============================================
CREATE PROCEDURE [CHORDS].[JC_A_SP_UpdateCHORDS_DEMOGRAPHICS_Tier_ReviveAClient]
AS
BEGIN

--Gathers current TIER PATIDs that are reviving into our current encounters
if OBJECT_ID('tempdb.dbo.#CHORDSTier','U') is not null DROP table #CHORDSTier;
SELECT DISTINCT 
	 e.person_id,c.mrn
into 	#CHORDSTier
From 
	 chords.encounters e
	 left join chords.chords_id c on e.person_id=c.person_id
	 left join chords.demographics d
	 on e.person_id=d.person_id
	 where d.PERSON_ID is null

	 

--Gather PATID MOST RECENT DEMOGRAPHIS INFORMATION from Tier data
if OBJECT_ID('tempdb.dbo.#tempDemo','U') is not null DROP table #tempDemo;
Select c.*
into #TempDemo
From
#CHORDSTier t inner join 
(Select * from Tier.[dbo].[FD__CLIENTS]  ) c
on t.mrn = c.clientkey
--select * from #tempDemo
 
 
if OBJECT_ID('tempdb.dbo.#CHORDSTemp','U') is not null DROP table #CHORDSTemp;

select 
--	patient_current_demographics.patient_name,
	m.PERSON_ID,
	t.clientkey as MRN,
	t.DOB as BIRTH_DATE,
	Case t.Gender 	
		When 'M' then 'M'
		When 'F' then 'F'
		Else 'U' end as  GENDER,
--select distinct sexual_orientation from #tempdemo
	Case  l.Description 
		when 'English' Then 'ENG' 
		when 'Spanish' then 'SPA' 
		When 'Vietnamese' then 'VIE'
		When 'Russian' then 'RUS'
		When 'Chinese' then 'CHI'
		else 'UNK' end as PRIMARY_LANGUAGE,

	NULL other_race_code,
	NULL other_race_value_long,
	'U' as NEEDS_INTERPRETER,
	'UN' as RACE1,
	'UN' as RACE2,
	'UN' as RACE3,
	'UN' as RACE4,
	'UN' as RACE5,

	case 
	when t.Hispanic = 'T' then 'Y'
	when t.Hispanic = 'F' then 'N'
	--More if needed
	else 'U'
end as HISPANIC,

	'UN' as SEXUAL_ORIENTATION, --This field is always null
	'UN' as GENDER_IDENTITY
into #CHORDSTemp
from 
	#tempDemo t
	left join tier.dbo.LT__Language l on t.primarylanguagecode = l.code
	left join CHORDS.STAGING.CHORDS_ID_Tier m on t.clientkey=m.mrn

--select * from #CHORDSTEMP

--Insert missing row
insert into CHORDS.DEMOGRAPHICS
select 
	id.PERSON_ID,
	c.MRN,
	BIRTH_DATE,
	GENDER,
	--Could add more so that we assign all 
	PRIMARY_LANGUAGE,
	NEEDS_INTERPRETER,
	RACE1,
	RACE2,
	RACE3,
	RACE4,
	RACE5,
	HISPANIC,
	SEXUAL_ORIENTATION,
	GENDER_IDENTITY
from #CHORDSTemp c
inner join (Select mrn, person_id from CHORDS.CHORDS_ID union select mrn, person_id from STAGING.CHORDS_ID) id
on c.mrn = id.mrn
		

END
GO
