SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:	Compiled from code by Benjamin Pacheco & MaryS
-- Create date: 01/31/2018
-- Description:	Stored Procedure to populate the CHORDS.DIAGNOSES table.
-- CHANGES:   Migrated to JCVS2.CHORDS 4/17/2018 and updated for CHORDS - MB
	-- Overriding null encounters to be excluded
	-- Overriding null DX and OrigDX with '' empty string and excluding
	-- Long DX with [DSM] need to be trimmed to fit in 10 Character field
	-- Using ~DX logic form Service Avatar
	--Overriding NULL providers
/*
PER CHORDS DATA MODEL MANUAL DIAGNOSES
This table lists all diagnoses associated with the encounters in the ENCOUNTER table. 
A record is a diagnosis code/original diagnosis code/diag provider combination unique to the index variable ENC_ID combination. 
The index variable ENC_ID uniquely identifies each encounter and is used to link the ENCOUNTER file to the both the DIAGNOSIS 
and PROCEDURE files. Changes in diagnosis coding from ICD-9 to ICD-10 in October 2015 are expected. Data partners should not include 
the problem list or chief complaint as a source of diagnoses. Also, do not include admitting diagnosis for inpatient stays.
Inclusions: Include all diagnoses associated with each encounter, primary and otherwise. Only final diagnoses should be included. 
Exclusions: Problem list, chief complaint and admitted diagnoses.
*/
-- =============================================
--Exec  [STAGING].[JC_A_SP_UpdateDiagnoses]
CREATE PROCEDURE [STAGING].[JC_A_SP_UpdateDiagnoses]

AS
BEGIN
SET NOCOUNT ON;
TRUNCATE TABLE Staging.Diagnoses
IF OBJECT_ID('tempdb.dbo.#temp_billtx_cwpat_notes', 'U') IS NOT NULL 
DROP TABLE dbo.#temp_billtx_cwpat_notes; 
select txhist.id , cw.blank_row_id , txhist.patid,txhist.facility,txhist.episode_number,
       cw.patid as cw_patid,             txhist.date_of_service,-- txhist.start_time,
       cw.date_of_service as cw_date_of_serv, cw.service_start_time as cw_serv_start_time,
   	    txhist.medical_diag_1_icd10_code,  txhist.medical_diag_1_icd10_value
		--,txhist.medical_diag_2_icd10_code,  txhist.medical_diag_2_icd10_value
		,practitioner_id
into #temp_billtx_cwpat_notes
from avatardw.[SYSTEM].billing_tx_history txhist
inner join avatardw.cwssystem.cw_patient_notes cw
   on txhist.JOIN_TO_TX_HISTORY = cw.JOIN_TO_TX_HISTORY 
   and txhist.patid = cw.patid
   and txhist.facility = cw.facility
where txhist.facility = '1' 
  and (txhist.date_of_service < GetDate() or txhist.date_of_service is null)
  --and txhist.patid=10206332
  and txhist.medical_diag_1_icd10_code <>''
  and practitioner_id not in (999999,999998)
  --select * from #temp_billtx_cwpat_notes order by date_of_service

--615658
--update note information with rank and type of diagnosis
--RESEARCH -----> Generated more records 642893, 30k extra???
IF OBJECT_ID('tempdb.dbo.#temp_services_notedx', 'U') IS NOT NULL 
drop table  #temp_services_notedx 
select t.id, t.patid, t.date_of_service, t.episode_number,t.facility
--,t.start_time
, t.medical_diag_1_icd10_code, t.medical_diag_1_icd10_value,practitioner_id,note_dx.ICD_code
, ISNULL(note_dx.diagnosis_status_value,'') as note_dxtype
, ISNULL(note_dx.ranking_value,'') as note_dxrank
into #temp_services_notedx 
from #temp_billtx_cwpat_notes  t
left join  
	(	select dxe.patid, dxe.facility, dxe.episode_number, dxe.diagnosis_status_value, dxe.ranking_value,dxe.icd_code 
		from avatardw.system.client_diagnosis_entry dxe
		  inner join avatardw.system.client_diagnosis_record dxr
			on dxe.diagnosisrecord = dxr.id
			--where dxe.patid = 60008222
				) note_dx

	on note_dx.patid = t.patid 
	and note_dx.facility = t.facility
WHERE note_dx.diagnosis_status_value not in ('Working','Resolved','Void','No Entry') --Skip Voids, Resolved, Working and Nulls
     and note_dx.icd_code = t.medical_diag_1_icd10_code
	and note_dx.episode_number = t.episode_number

--get primary dx --More rows 775276
IF OBJECT_ID('tempdb.dbo.#temp_Services_demog_DX', 'U') IS NOT NULL 
Drop table #temp_Services_demog_DX
select id, t.PATID , t.episode_number,  date_of_service, practitioner_id,medical_diag_1_icd10_code, medical_diag_1_icd10_value
	,note_dxtype, note_dxrank,dx_select.icd_code as lk_joindx, dx_select.diagnosis_status_value as lk_dxtype
	, dx_select.ranking_value as lk_dxrank -- If no diagnosis on note, lookup diagnosis again

into #temp_Services_demog_DX
from #temp_services_notedx  t
  LEFT JOIN (select dxe.patid, dxe.icd_code, dxe.diagnosis_status_value, dxe.ranking_value,date_of_diagnosis,dxe.episode_number
               from avatardw.system.client_diagnosis_entry dxe
                 inner join
                    avatardw.system.client_diagnosis_record dxr
                  on dxe.diagnosisrecord = dxr.id
               where dxe.billing_order <> ' '  -- skip VOIDED Dx 
                 and diagnosis_status_code <> 5  -- change 'skip VOID DX 
                 and dxe.ranking_code = '1'  -- primary dx only  VERIFY THIS ISN't INTERFERRING 5/4/2018 MB
			   ) dx_select  
			   on dx_select.patid = t.patid
			   and dx_select.episode_number = t.EPISODE_NUMBER
--select * from #temp_Services_demog_DX where practitioner_id is null

--Begin formatting--775276
IF OBJECT_ID('tempdb.dbo.#temp_round1_format', 'U') IS NOT NULL 
drop table #temp_round1_format
select id, patid, episode_number,   date_of_service, practitioner_id,
       medical_diag_1_icd10_code as Diag_On_Note_code, 
       medical_diag_1_icd10_value as Diag_On_Note_value, 
       note_dxtype as Diag_On_Note_DxType, 
       note_dxrank as Diag_On_Note_DxRank,      
       lk_joindx, lk_dxtype, lk_dxrank,   
       case
         when medical_diag_1_icd10_code <> ' ' then medical_diag_1_icd10_code
         else lk_joindx
       end as PrimDiagKey_fin
into #temp_round1_format
from #temp_Services_demog_DX t
--select * from #temp_round1_format where practitioner_id is null

--step 5 --override for R69s --775276
IF OBJECT_ID('tempdb.dbo.#temp_round2_format', 'U') IS NOT NULL 
drop table #temp_round2_format
select *
       ,case
          when t.PrimDiagKey_fin is NULL or t.PrimDiagKey_fin = 'NOT FOUND' then 'R69'
          else t.PrimDiagKey_fin
       end as PrimDiagKey_fin2
  
into #temp_round2_format
from #temp_round1_format t
--select * from #temp_round2_format where practitioner_id is null

--Insert AV diagnoses to CHORDS STAGING
Insert into CHORDS.STAGING.Diagnoses
select 
 Replace(r2.id,MRN,id.Person_ID) Enc_ID,
Case when r2.Diag_On_Note_code  is null then 
	Case when r2.PrimDiagKey_Fin  is null then PrimDiagKey_Fin2 
	else PrimDiagKey_Fin end 
Else r2.Diag_On_Note_code end DX,
Date_OF_Service ADATE,
ISNULL(p.PROVIDER,'JC9999999999') DIAGPROVIDER,--a few provider IDs do not match staff in either TIer or AV
--r2.practitioner_ID,
id.PERSON_ID,
ISNULL(e.ENCTYPE,'OE') ENCTYPE,
ISNULL(p.PROVIDER,'JC9999999999') PROVIDER,
Case when r2.Diag_On_Note_code  is null then 
	Case when r2.PrimDiagKey_Fin  is null then PrimDiagKey_Fin2 
	else PrimDiagKey_Fin end 
Else r2.Diag_On_Note_code end ORIGDX,
'10' DX_CODETYPE,
'X' PRINCIPAL_DX,
Case when r2.Diag_On_Note_code  is null  
	 then Case lk_dxrank when  'Primary' then 'P' else 'S' end
	else Case diag_on_Note_DXRank when 'Primary' then 'P' else 'S' end end PRIMARY_DX
--into #temp
From #temp_round2_format r2
inner join chords.CHORDS_PROVIDER p 
on Cast(p.staff_id as int)=Cast(r2.practitioner_id as int)
inner join  chords.chords_ID id
on r2.patid = id.mrn
inner join STAGING.Encounters e  --not allowing null ENC_IDs
on Replace(r2.id,MRN,id.Person_ID) = e.Enc_ID
order by enc_id
END
GO
