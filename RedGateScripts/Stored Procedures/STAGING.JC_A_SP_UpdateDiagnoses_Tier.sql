SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author Melody B
-- Created 5/7/2018
-- Capture TIER only Diagnosis for CHORDS db
-- The diagnosis (PrimDIagKey) is grabbed from the chargeslip (BIL_Chargeslips).  
-- Today 5/7 1% of Diagnoses do not have a primary in Tier
-- =============================================
CREATE PROCEDURE [STAGING].[JC_A_SP_UpdateDiagnoses_Tier]
AS
BEGIN
IF OBJECT_ID('tempdb.dbo.#temp', 'U') IS NOT NULL Drop table #temp
SELECT distinct
e.mrn, 
ENC_ID,
cs.PrimDiagKey,
ADATE,
PROVIDER DIAGPROVIDER,
ISNULL(e.Person_ID,'XXXXX') PERSON_ID,
ENCTYPE,
PROVIDER,
cs.PrimDiagKey ORIGDX,
Case when ICD10.ICD10_DOT is null then 9 else 10 end DX_CODETYPE,
'X' Principal_DX,
'P' PRIMARY_DX
--cs.ClientKey,
--cs.OP__DOCID,
--cs.PlaceOfSvcLU,
--cs.prockey,
--cs.DateSvc,
--cs.StaffKey
into #temp
FROM [STAGING].[Encounters_Tier] e
left JOIN CHORDS.CHORDS_ID ID 
	on e.person_id = ID.person_ID
INNER JOIN TIER.dbo.BIL_ChargeSlips cs
	on e.mrn = cs.clientkey
--	and e.Adate = cs.DateSvc
INNER JOIN TIER.dbo.T4W_Documents t
	on cs.op__docid = t.op__id
left join DATAMARTS.ReporTS.ICD10_DiagnosisGrouper ICD10
on cs.PrimDiagKey = ICD10.ICD10_DOT
WHERE cs.ProcKey NOT IN (1030,958,994,922)
--AND t.OP__ID = cs.OP__DocID
 and t.OP__STATUSORD = 5  -- final save only
 and CAST(CONVERT(char(8), t.op__creationdatetime, 112) AS DATETIME) >= '2011-01-01' 
AND (convert(varchar(12),cs.DATESVC,112) >= '2011-01-01')	
AND (cs.voidindicator is null or cs.voidindicator not in ('R', 'V'))
AND cs.PrimDiagKey is not null --we could do another join to find these 1% missing a primary, but for now hold off until Code REview with DG
and e.person_id = 'JC6712757681'

select * from #temp  order by adate

Insert into [STAGING].[Diagnoses_Tier]([ENC_ID], [DX], [ADATE], [DIAGPROVIDER], [PERSON_ID], [ENCTYPE], [PROVIDER], [ORIGDX], [DX_CODETYPE], [PRINCIPAL_DX], [PRIMARY_DX])
Select	ENC_ID,cs.PrimDiagKey,ADATE,DIAGPROVIDER,e.Person_ID,ENCTYPE,PROVIDER,ORIGDX,Case when ICD10.ICD10_DOT is null then 9 else 10 end DX_CODETYPE,
'X' Principal_DX,'P' PRIMARY_DX from #temp
END
GO
