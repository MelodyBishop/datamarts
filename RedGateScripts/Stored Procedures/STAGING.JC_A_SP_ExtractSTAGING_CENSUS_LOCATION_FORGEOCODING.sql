SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:	MelodyB
-- Create date: 10/24/2018
-- Extract CensusLocations for GeoCoding
-- exec [STAGING].[JC_A_SP_ExtractSTAGING_CENSUS_LOCATION_FORGEOCODING]
-- ===============================================

CREATE PROCEDURE [STAGING].[JC_A_SP_ExtractSTAGING_CENSUS_LOCATION_FORGEOCODING]
AS
BEGIN
Select  [PERSON_ID], loc_start, loc_end, ADDRESS1, ADDRESS2,  [CITY], [STATE], [ZIP]
FROM STAGING.Census_Location
order by person_id, loc_start

END
GO
