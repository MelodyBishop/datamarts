SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--Begin transaction
--execute [STAGING].[JC_A_SP_UpdateCHORDS_VITALS]
--commit transaction
--rollback transaction 
 --=============================================
-- Author: DAVID G
-- Create date: ~01/11/2018
-- Modified:   
-- Description:    Adds new Provider ID to chords.CHORDS_PROVIDER
-- Moved to CHORDS, updated, Schema 4/17/2018 - MB
--Had to use Encounters Max entry for a day, otherwise we risk multiplying Vital results since we appear to have no link between daily appointments and vitals.

--VDW Documentation
--The VITAL_SIGNS table includes various physiological measures taken by health professionals during an encounter including 
--body temperature, pulse rate, blood pressure, respirations, and anthropometry (height, weight, and BMI). 
--There is no unique primary key for the VITAL_SIGNS table.  
--VITAL_SIGNS does include an ENC_ID but multiple measures may be captured for any given encounter -yet we have an EncID???

--5/7/2018--Adding defaults for BPType = 'R' and Position = 1 sitting until we hear back from business

--This intruduces multiple rows for same day encounters with vitals taken - TBD reduce this based on left ENCID ~23
-- =============================================
CREATE PROCEDURE [STAGING].[JC_A_SP_UpdateSTAGING_VITALS]
AS
BEGIN

IF OBJECT_ID('tempdb.dbo.#temp','U') IS NOT NULL DROP TABLE #temp;--15139
select 
PERSON_ID = ID.person_id,
MEASURE_DATE = CONVERT(date,v1.admin_date_actual),
MEASURE_TIME = CONVERT(time,v1.admin_time_actual_h),
ENC_ID = NULL,
ENC_TYPE = NULL,
HT_RAW = v1.reading_value,
WT_RAW = v2.reading_value,
HT = v1.reading_entry,
WT = v2.reading_entry,
BMI_RAW = v3.reading_entry,
DIASTOLIC = v4.reading_entry,
SYSTOLIC = v5.reading_entry,
DIASTOLIC_RAW = v4.reading_entry,
SYSTOLIC_RAW = v5.reading_entry,
BP_TYPE = NULL,
POSITION = NULL,
HEAD_CIR_RAW = v6.reading_entry,
RESPIR_RAW = v7.reading_entry,
TEMP_RAW = v9.reading_entry,
PULSE_RAW = v8.reading_entry,
v1.Unique_Row_ID,
Right(v1.Unique_Row_ID,11) UniqueID
into #temp
from avatardw.cwssystem.cw_vital_signs v1
LEFT JOIN avatardw.cwssystem.cw_vital_signs v2 on v1.PATID = v2.PATID and v1.Unique_Row_Id = v2.Unique_Row_Id and v2.vital_sign = 'Weight (lbs)' and CONVERT(float,v2.reading_entry) < 1000
LEFT JOIN avatardw.cwssystem.cw_vital_signs v3 on v1.PATID = v3.PATID and v1.Unique_Row_Id = v3.Unique_Row_Id and v3.vital_sign = 'BMI' and CONVERT(float,v3.reading_entry) <= 100
LEFT JOIN avatardw.cwssystem.cw_vital_signs v4 on v1.PATID = v4.PATID and v1.Unique_Row_Id = v4.Unique_Row_Id and v4.vital_sign = 'Blood Pressure Dist' and CONVERT(float,v4.reading_entry) <= 500
LEFT JOIN avatardw.cwssystem.cw_vital_signs v5 on v1.PATID = v5.PATID and v1.Unique_Row_Id = v5.Unique_Row_Id and v5.vital_sign = 'Blood Pressure Sys' and CONVERT(float,v5.reading_entry) <= 500
LEFT JOIN avatardw.cwssystem.cw_vital_signs v6 on v1.PATID = v6.PATID and v1.Unique_Row_Id = v6.Unique_Row_Id and v6.vital_sign = 'Head Circumference (in)'
LEFT JOIN avatardw.cwssystem.cw_vital_signs v7 on v1.PATID = v7.PATID and v1.Unique_Row_Id = v7.Unique_Row_Id and v7.vital_sign = 'Respiration'
LEFT JOIN avatardw.cwssystem.cw_vital_signs v8 on v1.PATID = v8.PATID and v1.Unique_Row_Id = v8.Unique_Row_Id and v8.vital_sign = 'Pulse'
LEFT JOIN avatardw.cwssystem.cw_vital_signs v9 on v1.PATID = v9.PATID and v1.Unique_Row_Id = v9.Unique_Row_Id and v9.vital_sign = 'Temp (F)'
	LEFT JOIN (Select mrn, person_id from [CHORDS].[CHORDS_ID] union select mrn, person_id from STAGING.CHORDS_ID) ID on ID.[MRN] = v1.PATID
where v1.vital_sign = 'Height (in)' and ID.person_id is not null and v1.admin_date_actual >= '10/1/2015' 
;

--insert into #vital_signs
IF OBJECT_ID('tempdb.dbo.#temp2','U') IS NOT NULL DROP TABLE #temp2;--15139
SELECT
t.PERSON_ID,
t.MEASURE_DATE,
t.MEASURE_TIME,
IsNUll(e.ENC_ID,'xx') ENC_ID,
IsNull(e.ENCTYPE,'YY') ENC_TYPE      ,---BLANK
HT_RAW,
WT_RAW,
HT,
WT,
BMI_RAW,
ISNULL(DIASTOLIC,0) DIASTOLIC,
ISNULL(SYSTOLIC,0) SYSTOLIC,
DIASTOLIC_RAW,
SYSTOLIC_RAW,
'R' BP_TYPE,
1 POSITION,
HEAD_CIR_RAW,
RESPIR_RAW,
TEMP_RAW,
PULSE_RAW 
into #temp2
FROM #temp t
Left join (Select distinct Person_ID, adate, ENC_ID, enctype from STAGING.Encounters ) e --e has 
on t.PERSON_ID = e.Person_Id
and t.Measure_date=e.adate
order by t.Person_ID, Measure_DAte 

IF OBJECT_ID('STAGING.VITAL_SIGNS','U') IS NOT NULL TRUNCATE TABLE STAGING.VITAL_SIGNS;
Insert into STAGING.VITAL_SIGNS
SELECT * from #temp2
END;
GO
