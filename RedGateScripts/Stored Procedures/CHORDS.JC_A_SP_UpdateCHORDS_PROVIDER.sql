SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--
--execute [CHORDS].[JC_A_SP_UpdateCHORDS_PROVIDER]
-- =============================================
-- Author: Melody B
-- Create date: 06/7/2018
-- Modified:   
-- Description:    Adds new Provider ID to chords.CHORDS_PROVIDER for Tier & AV every run for every provider
--					We have deduplicated based on (Lastname, Firstname MiddleInit) after removing spaces

-- =============================================
CREATE PROCEDURE [CHORDS].[JC_A_SP_UpdateCHORDS_PROVIDER]
AS
BEGIN

Insert into CHORDS.CHORDS_PROVIDER
select  
p.STAFF_ID, p.PROVIDER
 from  STAGING.CHORDS_Provider p
left join CHORDS.CHORDS_PROVIDER pp
on p.provider = pp.provider
where pp.provider is null

END
GO
