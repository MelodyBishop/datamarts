SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--Begin transaction
--execute [CHORDS].[JC_A_SP_UpdateSTAGING_DEMOGRAPHICS]
--commit transaction
--rollback transaction 
 --=============================================
-- Author: DAVID G
-- Create date: 01/11/2018
-- Modified:   4/17/2018 Melody B
-- Description:    Adds new clients to Staging.CHORDS_Demographics
-- Added to CHORDS, updated to leverage history so we can capture latest info for this Level one table, Schema 
-- 5/8/2018 MB THE DEMOGRAPHICS TABLE MUST BE REBUILT EACH TIME TO CAPTURE THE LATEST DEMOGRAPHIC INFORMATION
-- Cleanup integrity/join on last insert to STG.DEM
-- Modified: 05/16/2018 MB - Excluding inserts for NULL BDAYS and trap them
-- =============================================
CREATE PROCEDURE [CHORDS].[JC_A_SP_UpdateSTAGING_DEMOGRAPHICS]
AS
BEGIN

--PER CHORDS DOCUMENTATION
--The DEMOGRAPHICS table contains basic person-descriptive for individuals found in all other VDW tables and serves the traditional ‘person table’ role. 
--The DEMOGRAPHICS table should reflect a patient’s most recent demographic information. 
--If a self-reported demographic attribute, such as race or primary language changes, this should be updated in the DEMOGRAPHICS table. 
--Thus, the DEMOGRAPHICS table should be rerun when the VDW is updated to reflect any changes in patient demographic information.
--One issue that can arise in the DEMOGRAPHICS table is duplicate patients.
--Duplicate patients should be removed/collapsed from the DEMOGRAPHICS table when they are identified in the healthcare organization.
--If an organization has a known problem with duplicates and no standard process to remove them, please note this on the data dictionary.
--Inclusions: Every PERSON_ID appearing in any other VDW file should appear in DEMOGRAPHICS 
--Exclusions: No individuals are excluded from the DEMOGRAPHICS table 

if OBJECT_ID('tempdb.dbo.#CHORDSpatids','U') is not null DROP table #CHORDSpatids;
--Gathers current list of PATIDs --129,053 distinct patids
SELECT DISTINCT 
	pdh.[patid] 
into 	#CHORDSpatids
From  	 AVATARDW.[SYSTEM].patient_demographic_History pdh 
WHERE 
	(upper(PDH.patient_name) not like 'TEST,%' 
	and upper(pdh.patient_name) not like 'GROUP,%'
	 and upper(pdh.patient_name) not like 'ZZZ%')
--select count(*) from #CHORDSPATIDS

--Gather PATID MOST RECENT DEMOGRAPHICS INFORMATION
if OBJECT_ID('tempdb.dbo.#tempDemo','U') is not null DROP table #tempDemo;
Select hist.*
into #TempDemo
From
(Select patid, Max(blank_row_ID) MAXXID from  AVATARDW.[SYSTEM].patient_demographic_History GRoup by patid) pdh 
inner join 
AVATARDW.[SYSTEM].patient_demographic_History hist
on pdh.patid = hist.patid
and hist.blank_row_ID = pdh.MAXXID
inner join #CHORDSPATIDS c on c.patid = pdh.patid
--select * from #tempDemo where date_of_birth is null--2402 nULL BDAYS

if OBJECT_ID('tempdb.dbo.#CHORDSTemp','U') is not null DROP table #CHORDSTemp;
if OBJECT_ID('tempdb.dbo.#temp_split_present_concern','U')  is not null drop table #temp_split_present_concern;

select 
	ID.Person_ID as PERSON_ID,
	t.patid as MRN,
	t.date_of_birth as BIRTH_DATE,
	t.sex_code as GENDER,
case 
	when t.[primary_language_value] = 'Amharic' then 'AMH'
	when t.[primary_language_value] = 'Arabic' then 'ARA'
	when t.[primary_language_value] = 'Cantonese' then 'UNK'
	when t.[primary_language_value] = 'Cherokee' then 'CHR'
	when t.[primary_language_value] = 'Chinese' then 'CHI'
	when t.[primary_language_value] = 'Croatian' then 'HRV'
	when t.[primary_language_value] = 'Czech' then 'CZE'
	when t.[primary_language_value] = 'Dutch' then 'DUT'
	when t.[primary_language_value] = 'English' then 'ENG'
	when t.[primary_language_value] = 'Farsi' then 'UNK'
	when t.[primary_language_value] = 'French' then 'FRE'
	when t.[primary_language_value] = 'German' then 'GER'
	when t.[primary_language_value] = 'Greek' then 'GRE'
	when t.[primary_language_value] = 'Hindi' then 'HIN'
	when t.[primary_language_value] = 'Hmong' then 'HMN'
	when t.[primary_language_value] = 'Italian' then 'ITA'
	when t.[primary_language_value] = 'Japanese' then 'JPN'
	when t.[primary_language_value] = 'Loatian' then 'UNK'
	when t.[primary_language_value] = 'Mandarin' then 'UNK'
	when t.[primary_language_value] = 'Mien' then 'UNK'
	when t.[primary_language_value] = 'No Entry' then 'UNK'
	when t.[primary_language_value] = 'Other' then 'UNK'
	when t.[primary_language_value] = 'Other African' then 'UNK'
	when t.[primary_language_value] = 'Other Communication' then 'UNK'
	when t.[primary_language_value] = 'Other Filipino' then 'UNK'
	when t.[primary_language_value] = 'Other Native American' then 'UNK'
	when t.[primary_language_value] = 'Pennsylvania Du' then 'UNK'
	when t.[primary_language_value] = 'Polish' then 'POL'
	when t.[primary_language_value] = 'Rumanian' then 'UNK'
	when t.[primary_language_value] = 'Russian' then 'RUS'
	when t.[primary_language_value] = 'Serbian' then 'SRP'
	when t.[primary_language_value] = 'Sign Language' then 'SGN'
	when t.[primary_language_value] = 'Spanish' then 'SPA'
	when t.[primary_language_value] = 'Tagalog' then 'TGL'
	when t.[primary_language_value] = 'Thai' then 'THA'
	when t.[primary_language_value] = 'Vietnamese' then 'YIE'
	else 'UNK'
end as PRIMARY_LANGUAGE,
	t.other_race_code,
	t.other_race_value_long,
	'U' as NEEDS_INTERPRETER,
	'UN' as RACE1,
	'UN' as RACE2,
	'UN' as RACE3,
	'UN' as RACE4,
	'UN' as RACE5,

	case 
	when t.[ethnic_origin_code] = '1' then 'N'
	when t.[ethnic_origin_code] = '2' then 'Y'
	when t.[ethnic_origin_code] = '3' then 'Y'
	when t.[ethnic_origin_code] = '4' then 'N'
	when t.[ethnic_origin_code] = '5' then 'Y'
	when t.[ethnic_origin_code] = '6' then 'U'
	else 'U'
end as HISPANIC,

	case 
		when t.[ss_demographics_dict_2_value] = 'Asexual' then 'AS'
		when t.[ss_demographics_dict_2_value] = 'Bisexual' then 'BI'
		when t.[ss_demographics_dict_2_value] = 'Declined/Unknown' then 'DC'
		when t.[ss_demographics_dict_2_value] = 'Demisexual' then 'OT'
		when t.[ss_demographics_dict_2_value] = 'Gay' then 'GA'
		when t.[ss_demographics_dict_2_value] = 'Heterosexual' then 'ST'
		when t.[ss_demographics_dict_2_value] = 'Lesbian' then 'LE'
		when t.[ss_demographics_dict_2_value] = 'No Entry' then 'UN'
		when t.[ss_demographics_dict_2_value] = 'Omnisexual' then 'OT'
		when t.[ss_demographics_dict_2_value] = 'Other (Specify)' then 'OT'
		when t.[ss_demographics_dict_2_value] = 'Pansexual' then 'OT'
		when t.[ss_demographics_dict_2_value] = 'Prefer No Label' then 'DC'
		when t.[ss_demographics_dict_2_value] = 'Queer' then 'QU'
		when t.[ss_demographics_dict_2_value] = 'Questioning' then 'QS'
		else 'UN'
	end as SEXUAL_ORIENTATION,
	
	'UN' as GENDER_IDENTITY
into #CHORDSTemp 
from 
	#tempDemo t
	inner join (Select mrn, person_ID from CHORDS.CHORDS_ID UNION Select mrn,person_ID from STAGING.CHORDS_ID UNION Select mrn,person_ID from STAGING.CHORDS_ID_TIER) ID on t.patid = ID.mrn
--select * from #CHORDSTEMP order by mrn

;with cte_split_string(MRN, other_race_code, current_race, other_race_value_long, level) as (
   select MRN, other_race_code, 
       LEFT(CAST(other_race_value_long as varchar(max)), CHARINDEX('&',other_race_value_long+'&')-1),
         STUFF(other_race_value_long, 1, CHARINDEX('&',other_race_value_long+'&'), ''), level=1
   from #CHORDSTemp
   union all
   select MRN, other_race_code,
       LEFT(CAST(other_race_value_long as varchar(max)), CHARINDEX('&',other_race_value_long+'&')-1),
        STUFF(other_race_value_long, 1, CHARINDEX('&',other_race_value_long+'&'), ''),cte_split_string.level+1
   from cte_split_string
   where other_race_value_long > ''
)

select MRN, other_race_code, current_race, other_race_value_long, level
into #temp_split_present_concern
from cte_split_string ct
order by MRN, level
option (maxrecursion 0);
--select * from #temp_split_present_concern order by 1

update c
set c.RACE1 =
case 
	when t.current_race = 'Native Hawaiian' then 'HP'
	when t.current_race = 'Other Pacific Islander' then 'HP'
	when t.current_race = 'Samoan' then 'HP'
    when t.current_race = 'Guamanian/Chamorro' then 'HP'
    when t.current_race = 'American Indian/Alaska Native' then 'IN'
    when t.current_race = 'Vietnamese' then 'AS'
    when t.current_race = 'Japanese' then 'AS'
    when t.current_race = 'Chinese' then 'AS'
    when t.current_race = 'Korean' then 'AS'
    when t.current_race = 'Asian Indian' then 'AS'
    when t.current_race = 'Other Asian' then 'AS'
    when t.current_race = 'White/Caucasian' then 'WH'
    when t.current_race = 'Black/African-American' then 'BA'
	else 'UN' 
end 
from #CHORDSTemp c
	inner join  #temp_split_present_concern t on t.mrn = c.MRN and t.level = 1;--58116 with Race 1

update c
set c.RACE2 =
case 
	when t.current_race = 'Native Hawaiian' then 'HP'
	when t.current_race = 'Other Pacific Islander' then 'HP'
	when t.current_race = 'Samoan' then 'HP'
    when t.current_race = 'Guamanian/Chamorro' then 'HP'
    when t.current_race = 'American Indian/Alaska Native' then 'IN'
    when t.current_race = 'Vietnamese' then 'AS'
    when t.current_race = 'Japanese' then 'AS'
    when t.current_race = 'Chinese' then 'AS'
    when t.current_race = 'Korean' then 'AS'
    when t.current_race = 'Asian Indian' then 'AS'
    when t.current_race = 'Other Asian' then 'AS'
    when t.current_race = 'White/Caucasian' then 'WH'
    when t.current_race = 'Black/African-American' then 'BA'
	else 'UN' 
end 
from #CHORDSTemp c
	inner join  #temp_split_present_concern t on t.mrn = c.MRN and t.level = 2;--1446 

update c
set c.RACE3 =
case 
	when t.current_race = 'Native Hawaiian' then 'HP'
	when t.current_race = 'Other Pacific Islander' then 'HP'
	when t.current_race = 'Samoan' then 'HP'
    when t.current_race = 'Guamanian/Chamorro' then 'HP'
    when t.current_race = 'American Indian/Alaska Native' then 'IN'
    when t.current_race = 'Vietnamese' then 'AS'
    when t.current_race = 'Japanese' then 'AS'
    when t.current_race = 'Chinese' then 'AS'
    when t.current_race = 'Korean' then 'AS'
    when t.current_race = 'Asian Indian' then 'AS'
    when t.current_race = 'Other Asian' then 'AS'
    when t.current_race = 'White/Caucasian' then 'WH'
    when t.current_race = 'Black/African-American' then 'BA'
	else 'UN' 
end 
from #CHORDSTemp c
	inner join  #temp_split_present_concern t on t.mrn = c.MRN and t.level = 3;--85

update c
set c.RACE4 =
case 
	when t.current_race = 'Native Hawaiian' then 'HP'
	when t.current_race = 'Other Pacific Islander' then 'HP'
	when t.current_race = 'Samoan' then 'HP'
    when t.current_race = 'Guamanian/Chamorro' then 'HP'
    when t.current_race = 'American Indian/Alaska Native' then 'IN'
    when t.current_race = 'Vietnamese' then 'AS'
    when t.current_race = 'Japanese' then 'AS'
    when t.current_race = 'Chinese' then 'AS'
    when t.current_race = 'Korean' then 'AS'
    when t.current_race = 'Asian Indian' then 'AS'
    when t.current_race = 'Other Asian' then 'AS'
    when t.current_race = 'White/Caucasian' then 'WH'
    when t.current_race = 'Black/African-American' then 'BA'
	else 'UN' 
end 
from #CHORDSTemp c
	inner join  #temp_split_present_concern t on t.mrn = c.MRN and t.level = 4;--13 rows

update c
set c.RACE5 =
case 
	when t.current_race = 'Native Hawaiian' then 'HP'
	when t.current_race = 'Other Pacific Islander' then 'HP'
	when t.current_race = 'Samoan' then 'HP'
    when t.current_race = 'Guamanian/Chamorro' then 'HP'
    when t.current_race = 'American Indian/Alaska Native' then 'IN'
    when t.current_race = 'Vietnamese' then 'AS'
    when t.current_race = 'Japanese' then 'AS'
    when t.current_race = 'Chinese' then 'AS'
    when t.current_race = 'Korean' then 'AS'
    when t.current_race = 'Asian Indian' then 'AS'
    when t.current_race = 'Other Asian' then 'AS'
    when t.current_race = 'White/Caucasian' then 'WH'
    when t.current_race = 'Black/African-American' then 'BA'
	else 'UN' 
end 
from #CHORDSTemp c
	inner join  #temp_split_present_concern t on t.mrn = c.MRN and t.level = 5;--7 rows
--select * from #CHORDSTEMP


Truncate TABLE CHORDS.STAGING.DEMOGRAPHICS
Insert into STAGING.[DEMOGRAPHICS]
select 
	PERSON_ID,
	c.MRN,
	BIRTH_DATE,
	GENDER,
	PRIMARY_LANGUAGE,
	NEEDS_INTERPRETER,
	RACE1,
	RACE2,
	RACE3,
	RACE4,
	RACE5,
	HISPANIC,
	SEXUAL_ORIENTATION,
	GENDER_IDENTITY
from #CHORDSTemp c
where BIRTH_DATE is not null
	
--select * from CHORDS.STAGING.DEMOGRAPHICS;
END
GO
