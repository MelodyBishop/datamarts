SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--execute [CHORDS].[JC_A_SP_UpdateCHORDS_DIAGNOSES]
 --=============================================
-- Author: Melody B
-- Create date: 06/7/2018
-- Description:    Adds new clients to Staging.DIAGNOSES
-- =============================================
CREATE PROCEDURE [CHORDS].[JC_A_SP_UpdateCHORDS_DIAGNOSES]
AS
BEGIN
Insert into [CHORDS].DIAGNOSES (
 ENC_ID, DX, ADATE, DIAGPROVIDER, PERSON_ID, ENCTYPE, PROVIDER, ORIGDX, DX_CODETYPE, PRINCIPAL_DX, PRIMARY_DX)

SELECT 
 e.ENC_ID, e.DX, e.ADATE, e.DIAGPROVIDER, e.PERSON_ID, e.ENCTYPE, e.PROVIDER, e.ORIGDX, e.DX_CODETYPE, e.PRINCIPAL_DX, e.PRIMARY_DX
FROM staging.Diagnoses e
left join CHORDS.DIAGNOSES d 
on e.enc_ID = d.enc_ID
and e.dx = d.dx
where (d.enc_id is null and d.dx is null)


--Load TIER Data to Table
insert into [CHORDS].DIAGNOSES (
 ENC_ID, DX, ADATE, DIAGPROVIDER, PERSON_ID, ENCTYPE, PROVIDER, ORIGDX, DX_CODETYPE, PRINCIPAL_DX, PRIMARY_DX)

SELECT 
 e.ENC_ID, e.DX, e.ADATE, e.DIAGPROVIDER, e.PERSON_ID, e.ENCTYPE, e.PROVIDER, e.ORIGDX, 
RIGHT('00' + e.DX_CODETYPE,2), e.PRINCIPAL_DX, e.PRIMARY_DX
FROM staging.Diagnoses_TIER e
left join CHORDS.DIAGNOSES d 
on e.enc_ID = d.enc_ID
and e.dx = d.dx
where (d.enc_id is null and d.dx is null)

END
GO
