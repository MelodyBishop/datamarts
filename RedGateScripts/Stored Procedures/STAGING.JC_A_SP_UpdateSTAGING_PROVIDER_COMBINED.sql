SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--
--execute [STAGING].[JC_A_SP_UpdateSTAGING_PROVIDER_COMBINED]
-- =============================================
-- Author: Melody B
-- Create date: 05/3/2018
-- Modified:   
-- Description:    Adds new Provider ID to chords.CHORDS_PROVIDER for Tier & AV every run for every provider
--					We have deduplicated based on (Lastname, Firstname MiddleInit) after removing spaces
-- Modified 6/13/2018 MB - Using EmployeeID for uniqueness between TIER and AV
-- Modified 6/26/2018 MB - Captured ~7 clincinans with 2 IDs in separate systems.  removing one.
-- Modification 6/27/2018 MB - Update to pull no tier providers and revert to using AvatarStaffID, using Employee 
-- is not necessarily unique #1529 IS Claodius in Tier and Groom in AV
-- =============================================
CREATE PROCEDURE [STAGING].[JC_A_SP_UpdateSTAGING_PROVIDER_COMBINED]
AS
BEGIN
SET NOCOUNT ON;


--Clean the slate of STAGING AND TEMP TABLES
TRUNCATE TABLE   STAGING.CHORDS_PROVIDER
If OBJECT_ID('tempdb.dbo.#CHORDSprovider','U') is not null DROP table #CHORDSprovider;
If OBJECT_ID('tempdb.dbo.#Temp','U') is not null DROP table #Temp;

Create Table #CHORDSprovider (Provider int, Fullname varchar(100),  MaskedProvider varchar(12))

--ADD AV STAFF TO TEMP TABLE

Insert into  #CHORDSprovider
select distinct Cast(provider_id as int), v_provider_name,'' 
from avatardw.SYSTEM.billing_TX_History 
WHERE  v_provider_name not like '%TEST%' 
    and provider_id not in (0, 999999, 999998)
	and Cast(provider_id as int) not in (Select staff_ID from [CHORDS].[CHORDS_PROVIDER] 
												UNION Select Staff_id from CHORDS.STAGING.CHORDS_PROVIDER
												UNION Select provider from #CHORDSprovider)
 
Insert into  #CHORDSprovider
Select distinct op__DocID  Provider, FullName,'' MaskedProvider  
  from  tier.dbo.fd__STAFF s 
  where op__DocID not in (Select staff_ID from [CHORDS].[CHORDS_PROVIDER] 
												UNION Select Staff_id from CHORDS.STAGING.CHORDS_PROVIDER
												UNION Select provider from #CHORDSprovider)
select * into #temp from #CHORDSprovider
--select * from #temp order by 1

--Create IDS for each record
Declare @OrigProv varchar(100);
Declare @Mask varchar(12);
Declare @id int

While Exists (select top 1 provider from #temp order by provider)
BEGIN
	Set @OrigProv = (Select top 1 provider  from #temp order by provider)
	SET @Mask = 'JC' + RIGHT(CAST(CAST(CAST(NEWID() AS VARBINARY) AS BIGINT) AS VARCHAR(30)),13);
	--compare IDs to live, if not a match with an existing ID then go forward
	IF (SELECT PROVIDER FROM chords.CHORDS_PROVIDER WHERE PROVIDER = @OrigProv 
		union SELECT PROVIDER FROM staging.CHORDS_PROVIDER WHERE PROVIDER = @OrigProv) IS NULL
		
			BEGIN
				Update #CHORDSProvider 
				set MaskedProvider = @Mask where provider = @OrigProv
				Delete from #temp where provider=@OrigProv
			END 
END

--Final Insert to STAGING
Insert into STAGING.CHORDS_PROVIDER 
select distinct  Provider, MaskedProvider, FullName 
From #CHORDSPROVIDER --2820 providers

--Add dummy provider
Insert into STAGING.CHORDS_PROVIDER
Select 0,'UNK','Dummy'
END
GO
