SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--
--/* ************************************************************************************************************************************************************************************************************************************************ 
--Name:		    JC_A_SP_Create_CHORDS_Census_Location
--Author:		Mary Steffeck, JCMH
--Created:	    November 2017
--Description:  This proc will read all chargeslips in TIER and create CHORDS encounters.
--              
--CENSUS_LOCATION
--The CENSUS_LOCATION table holds patient geographic location information collected at healthcare encounters. 
--Patient addresses should be geocoded, and FIPS codes down to the census tract level should be populated in the CENSUS_LOCATION table.
--Every individual in the DEMOGRAPHIC table is represented in the CENSUS_LOCATION table at least once. 
--For sites that store historical addresses, the CENSUS_LOCATION table is historical; 
--it contains census tracts of past residence(s) with dates when that location was valid. 
--The ‘current’ address for an individual is identified through their missing location end date (LOC_END).
--  A new record or row in the CENSUS_LOCATION table is generated whenever a patient has an encounter with the system and a new address is reported.
--Patient addresses that cannot be geocoded, or if the patient is homeless, should be attributed to the 
--county if possible (e.g. Denver county geocode=08031).  
--Individuals with no residence information should have a record in the CENSUS_LOCATION table with no FIPS code.  
--Inclusions: Every Person_ID in the VDW DEMOGRAPHICS table is represented in the CENSUS_LOCATION table. 
--Exclusions: None 

--
--*************************************************************************************************************************************/

--EXECUTE CHORDS.JC_A_SP_Create_CHORDS_Census_Location

CREATE   procedure [CHORDS].[JC_A_SP_Create_CHORDS_Census_Location_OBS?]
as
BEGIN

with CTE  ( ROWNUM,blank_row_id, person_id,patient_street_1, patient_city, patient_state,patient_county_code, patient_county_value, patient_zipcode, StartDate,MaxDET,Loc_Start )
AS (Select Row_Number() Over(PARTITION BY d.person_id,h.patient_street_1, patient_city, patient_state
										,patient_county_code, patient_county_value, patient_zipcode 
										,data_entry_Date order by data_entry_Date asc,data_entry_time asc) as ROWNUM
,blank_row_id,d.person_id,h.patient_street_1, h.patient_city, h.patient_state,h.patient_county_code, h.patient_county_value, patient_zipcode,data_entry_date
,Max(data_entry_time) MaxDET,Cast(UTCTimestamp as DATE) LOC_START
from CHORDS.DEMOGRAPHICS d
left join AvatarDW.SYSTEM.Patient_Demographic_History h 
on d.mrn=h.patid
--and d.facility = h.facility
--where person_id='JC9999006157'
Group by blank_row_id,person_id, patient_street_1, patient_city, patient_state,patient_county_code, patient_county_value, patient_zipcode, data_entry_date,data_entry_time,Cast(UTCTimestamp as DATE) 
)
--Insert into CHORDS.Census_Location
Select distinct rownum,person_id, startDate,NULL,patient_street_1,NULL,NULL, patient_city, patient_state, patient_zipcode, NULL, patient_county_code, patient_county_value  
From CTE 
--WHERE Patient_Street_1 is null or Patient_Street_1=''
--where person_id='JC9999006157'
order by person_id desc, startdate desc, rowNUM desc
                        
END
GO
