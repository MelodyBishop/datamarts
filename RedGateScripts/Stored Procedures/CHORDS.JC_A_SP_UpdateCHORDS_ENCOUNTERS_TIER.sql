SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--execute [CHORDS].[JC_A_SP_UpdateCHORDS_ENCOUNTERS_TIER]
 --=============================================
-- Author: Melody B
-- Create date: 06/7/2018
-- Description:    Adds new clients to Staging.CHORDS_Encounters from TIER
-- =============================================
CREATE PROCEDURE [CHORDS].[JC_A_SP_UpdateCHORDS_ENCOUNTERS_TIER]
AS
BEGIN
--Load Table

insert into [CHORDS].Encounters (
[ENC_ID], [PERSON_ID], [ADATE], [DDATE], [PROVIDER], [ENC_COUNT], [DRG_VERSION], [DRG_VALUE]
, [ENCTYPE], [ENCOUNTER_SUBTYPE], [FACILITY_CODE], [DISCHARGE_DISPOSITION], [DISCHARGE_STATUS], [ADMITTING_SOURCE], [DEPARTMENT]
)

SELECT t.[ENC_ID], t.[PERSON_ID], t.[ADATE], t.[DDATE], t.[PROVIDER], t.[ENC_COUNT], t.[DRG_VERSION], t.[DRG_VALUE]
, t.[ENCTYPE], t.[ENCOUNTER_SUBTYPE], t.[FACILITY_CODE], t.[DISCHARGE_DISPOSITION], t.[DISCHARGE_STATUS], t.[ADMITTING_SOURCE], t.[DEPARTMENT]
FROM staging.Encounters_Tier t
left join chords.encounters d
on t.person_ID = d.person_ID
and t.enc_ID = d.enc_ID
where 
d.enc_id is null

END
GO
