SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--
--/* ************************************************************************************************************************************************************************************************************************************************ 
--Name:		    JC_A_SP_Update_CHORDS_Encounter_Tier
--Author:		Mary Steffeck, JCMH
--Created:	    November 2017
--Description:  This proc will read all chargeslips in TIER and create CHORDS encounters.
--              
--Modified:   Melody Bishop  
--Changed to work for Tier only adding rows to Staging_Archive and Staging tables
--Data pulled back to 1/1/2011 per CHORDS Documentation
--Applying default values BPTYPE='R" and Position=1-sitting until we hear back from business
-- Updating to use Chargekey+|+Person_ID as encid 5/11/2018 MB
-- Modified: 5/9/2018 MB - Updating for screening out demographic removals
-- Modifed 6/5/2018 MB To remove 990 and 9901 programs for CCC- WIC and MCU
-- Modified 6/7/2018 MB To add in Tier Chords_IDs for gathering
-- Modified 6/9/2018 MB To update JC9999999999 to UNK per CHORDS SPEC
--CHORDS DOCUMENTATION:
-- The ENCOUNTERS table should include records from encounters between patients and medical personnel indexed by ENC_ID and PERSON_ID.
-- All available encounters with a medical provider should be included. Medical providers include: physicians, nurse practitioners,
-- registered nurses, lab technicians, social workers, etc.—generally, people licensed to provide medical care and closely related services.
-- Recurring visits to the same clinicians on the same day should be maintained as separate encounters if possible. 
-- Multiple encounters to the same provider on the same day are allowed if that is the truth in the source data and have unique ENC_ID values.
-- Inpatient hospital stays lasting multiple days should be represented as one encounter. 
-- FACILITY_CODE is included in the ENCOUNTERS table for data partners to populate with any site-specific clinic or department identifier 
-- that can aid data validation and preserve data lineage. 
--*************************************************************************************************************************************/
CREATE Procedure [STAGING].[JC_A_SP_UpdateSTAGING_ENCOUNTER_TIER]
--EXECUTE STAGING.[JC_A_SP_Update_STAGING_Encounter_Tier]
as
BEGIN
Truncate Table CHORDS.STAGING.Encounters_Tier

declare @begindate datetime
set @begindate = '2011-01-01' --Using CHORDS suggested begin date

Insert into CHORDS.Staging.Encounters_Tier
select mrn,
       Cast(cs.chargekey as varchar) + '|'+Person_ID,  --  what is the encounter id for TIER services
	   ID.person_id,
	   cs.datesvc, 
       NULL as ddate,
	   ISNULL(p.provider,'UNK') as Provider_ID,--CHORDS MASKED_PROVIDER
       1 as enc_count,
       NULL as drg_version,
	   NULL as drg_value,
       case
         when cs.prockey in (1934,2222,2294,2373,2186,2524,29372722,2330) then 'TE'  -- phone procedures
         else 'OE'  -- other encounter
       end as enctype,
       'OT' as encounter_subtype,  -- other non-hospital
       'T' + Cast(cs.PlaceOfSvcLU as varchar) as facility_code,
       'U' as discharge_disposition,
       'UN' as discharge_status,
       'UN' as admitting_source,
       'MH' as department    
from (Select * from tier.dbo.bil_chargeslips where pgmlu not in (9900,9901)) cs
inner join tier.dbo.T4W_DOCUMENTS d
    on cs.op__docid = d.op__id
Left join (Select staff_id, provider from CHORDS.CHORDS_PROVIDER union select staff_ID, provider from STAGING.CHORDS_PROVIDER) p
		on cs.staffkey = p.Staff_ID 
INNER join (
--Select mrn, person_id from CHORDS.CHORDS_ID union select mrn,person_id from STAGING.CHORDS_ID union select mrn,person_id from STAGING.CHORDS_ID_TIER

		Select d.person_ID,c.mrn from 
			(Select mrn, person_id from CHORDS.CHORDS_ID
				union 
			 SELECT mrn, person_ID from STAGING.CHORDS_ID
				union 
			 SELECT mrn, person_ID from STAGING.CHORDS_ID_TIER
			) c
			inner join
			(Select person_id, mrn from  CHORDS.staging.demographics 
				union 
				Select person_id, mrn from  CHORDS.staging.demographics_tier
			) d
			on c.person_ID=d.person_ID--142205 only
			--select * from staging.encounters_tier where person_id='JC2817407824'

) ID
			on ID.MRN = cs.clientkey
where --chargekey=20934774 and 
 (cs.voidindicator is null or (cs.voidindicator = 'V' and cs.OriginalCS = 0))  -- get only initial charge
	and cs.datesvc>= @begindate
  and cs.IsEncounter = 'Y'
  and d.OP__STATUSORD = 5  -- final save only
  and CAST(CONVERT(char(8), d.op__creationdatetime, 112) AS DATETIME) >= @begindate 
END
GO
