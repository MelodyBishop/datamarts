SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ************************************************************************************************************************************************************************************************************************************************ 
--Name:		    JC_A_SP_Create_CHORDS_Childless_IDs
--Author:		Melody Bishop, JCMH
--Created:	    June 6, 2018
--Description:  This proc will read capture all childless IDs for verification/validation.
--exec    [STAGING].[JC_A_SP_Create_STAGING_Childless_IDs]
-- ***************************************************************************             
CREATE   procedure [STAGING].[JC_A_SP_Create_STAGING_Childless_IDs]
as
BEGIN
TRUNCATE TABLE STAGING.CHILDLESS_IDs

if OBJECT_ID('tempdb.dbo.#temp','U') is not null DROP table #temp;
Select c.mrn,c.person_ID 
into #temp from 
chords.CHORDS_ID c
left join 
(select person_id from chords.demographics union
  select person_ID from chords.encounters
 union select person_id from chords.diagnoses
 union select person_id from chords.vital_signs

) d
on c.person_id=d.person_id 
where d.person_id is null

Insert into STAGING.Childless_IDs
Select mrn, person_id from #temp t

Delete from Chords.Chords_id
where person_id in (Select person_ID from #temp)

END
GO
