SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [STAGING].[JCMH_GetAllDiagnosis_TIER]
AS
--select * from STAGING.JCMH_GetAllDiagnosis_TIER where clientkey=30286270
--and date_end is null
/* ----------------------------------------------------------------------------------------------------------
-- Name:		JCMH_GetAllDiagnosis_TIER
-- Author:		Mary Steffeck, JCMH
-- Created:	    May, 2018
-- Description:  TIER  -- This view looks at the 5 ICD9 (Axis I - IV, Deferred) and 1 ICD10 diagnosis tables
--              
-- Modified:	   5/18/2018 MB to cover CHORDS AXIS I & II and Finals only - see original view on TierCluster for GAF, Axis III & IV & others
--					
-- ----------------------------------------------------------------------------------------------------------*/


Select top 100 percent 
clientkey, date_start,rank,dx, date_end  from 

(
SELECT top 100 percent a.clientkey, a.Date_Start, Case r.Descr when 'Other' then 'Secondary' else r.Descr end AS Rank
,ICD10 DX,l.DSM4_DSCR AS Dx_Desc,a.Date_End
FROM TIER.dbo.FD__AxisI a
,TIER.dbo.LT__DSM4_AXIS_I l 
left join (SELECT distinct 
				 ICD9CODEPRI AS ICD9,
				 ICD10CODEPRI AS ICD10
				FROM TIER.dbo.FD__DIAGNOSIS_DETAIL )  i
on l.ICD9_Code = i.ICD9
, TIER.dbo.LT__AxisRank r
where a.AXISI_Code = l.Code
AND a.AxisRankCode = r.Code
and (
		(date_start >= '2011-01-01 00:00:00.000' and date_end>='2011-01-01 00:00:00.000')
  	or  (date_start < '2011-01-01 00:00:00.000' and date_end is null)
	or  (date_start >= '2011-01-01 00:00:00.000' and date_end is null)
	)
and ICD10<>'R69'
and clientkey is not null
and date_start<=DateAdd(d,0,GetDate())
and clientkey is not null
--and clientkey=23955
--order by 1,2,3

UNION

SELECT a.clientkey, a.Date_Start,Case r.Descr when 'Other' then 'Secondary' else r.Descr end AS Rank, ICD10 DX,
       l.DSM4_DSCR AS Dx_Desc, a.Date_End
FROM TIER.dbo.FD__AxisII a,TIER.dbo.LT__DSM4_AXIS_II l 
left join (SELECT distinct 
				 ICD9CODEPRI AS ICD9,
				 ICD10CODEPRI AS ICD10
				FROM TIER.dbo.FD__DIAGNOSIS_DETAIL )  i
on l.ICD9_Code = i.ICD9

, TIER.dbo.LT__AxisRank r
where a.AXISII_Code = l.Code
AND a.AxisRankCode = r.Code
and (
		(date_start >= '2011-01-01 00:00:00.000' and date_end>='2011-01-01 00:00:00.000')
  	or  (date_start < '2011-01-01 00:00:00.000' and date_end is null)
	or  (date_start >= '2011-01-01 00:00:00.000' and date_end is null)
	)
and date_start<=DateAdd(d,0,GetDate())
and clientkey is not null
and ICD10<>'R69'
--and clientkey=23955
--order by 1,2,3

UNION

SELECT TOP 100 PERCENT 
  a.clientkey
, a.DATEDOC as date_start
, Case a.ranking when 'Other' then 'Secondary' else a.ranking end AS Rank
, a.ICD10CODEPRI AS DX
, a.DISPLAYTERM AS Dx_Desc
, a.DATERESOLVED as date_end

FROM TIER.dbo.FD__DIAGNOSIS_DETAIL a
where ICD10CODEPRI <> 'NOT FOUND'
and (
		(a.DateDoc >= '2011-01-01 00:00:00.000' and DATERESOLVED>='2011-01-01 00:00:00.000')
  	or  (a.DateDoc < '2011-01-01 00:00:00.000' and DATERESOLVED is null)
	or  (a.DateDoc >= '2011-01-01 00:00:00.000' and DATERESOLVED is null)
	)
and a.DATEDOC<=DateAdd(d,0,GetDate())
and clientkey is not null
and a.ICD10CODEPRI<>'R69'

) Diags

 ORDER BY clientkey asc,  rank asc, date_end asc
GO
