CREATE TABLE [CHORDS].[PROCEDURES]
(
[PROCEDURES_ID] [int] NOT NULL IDENTITY(1, 1),
[ENC_ID] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PROCDATE] [date] NOT NULL,
[PERFORMINGPROVIDER] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ORIGPX] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PX] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PX_CODETYPE] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ROW_ID] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PERSON_ID] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PROVIDER] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ADATE] [date] NOT NULL,
[ENCTYPE] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PXCNT] [numeric] (7, 0) NOT NULL CONSTRAINT [DF__PROCEDURE__PXCNT__4A8310C6] DEFAULT ((1)),
[CPTMOD1] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPTMOD2] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPTMOD3] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [CHORDS].[PROCEDURES] ADD CONSTRAINT [PK__PROCEDUR__EA14109E36FD0A91] PRIMARY KEY NONCLUSTERED  ([PROCEDURES_ID]) ON [PRIMARY]
GO
ALTER TABLE [CHORDS].[PROCEDURES] ADD CONSTRAINT [FK_CHORDS_PROCEDURES_DEMOGRAPHICS] FOREIGN KEY ([PERSON_ID]) REFERENCES [CHORDS].[DEMOGRAPHICS] ([PERSON_ID])
GO
ALTER TABLE [CHORDS].[PROCEDURES] ADD CONSTRAINT [FK_CHORDS_PROCEDURES_ENCOUNTERS] FOREIGN KEY ([ENC_ID]) REFERENCES [CHORDS].[ENCOUNTERS] ([ENC_ID])
GO
ALTER TABLE [CHORDS].[PROCEDURES] ADD CONSTRAINT [FK_CHORDS_PROCEDURES_PROVIDER_SPECIALTY] FOREIGN KEY ([PROVIDER]) REFERENCES [CHORDS].[PROVIDER_SPECIALTY] ([PROVIDER])
GO
ALTER TABLE [CHORDS].[PROCEDURES] ADD CONSTRAINT [FK_CHORDS_PROCEDURES_PROVIDER_SPECIALTY2] FOREIGN KEY ([PERFORMINGPROVIDER]) REFERENCES [CHORDS].[PROVIDER_SPECIALTY] ([PROVIDER])
GO
ALTER TABLE [CHORDS].[PROCEDURES] ADD CONSTRAINT [FK_CHORDS_PROCEDURES_PX_CODETYPE_LU] FOREIGN KEY ([PX_CODETYPE]) REFERENCES [CHORDS].[PX_CODETYPE_LU] ([ABBREVIATION])
GO
