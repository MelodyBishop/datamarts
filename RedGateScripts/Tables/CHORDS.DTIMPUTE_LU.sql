CREATE TABLE [CHORDS].[DTIMPUTE_LU]
(
[ABBREVIATION] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DESCRIPTION] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [CHORDS].[DTIMPUTE_LU] ADD CONSTRAINT [PK_DTIMPUTE_LU] PRIMARY KEY CLUSTERED  ([ABBREVIATION]) ON [PRIMARY]
GO
