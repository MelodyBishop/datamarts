CREATE TABLE [CHORDS].[LANGUAGES_ISO_LU]
(
[ISO_639-2_CODE] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ENGLISH_DESCRIPTION] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [CHORDS].[LANGUAGES_ISO_LU] ADD CONSTRAINT [PK_LANGUAGES_ISO_LU] PRIMARY KEY CLUSTERED  ([ISO_639-2_CODE]) ON [PRIMARY]
GO
