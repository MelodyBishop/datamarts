CREATE TABLE [STAGING].[CHILDLESS_IDs]
(
[mrn] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[person_ID] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
