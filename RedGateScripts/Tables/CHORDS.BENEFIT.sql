CREATE TABLE [CHORDS].[BENEFIT]
(
[Benefit_ID] [int] NOT NULL,
[Load_Date] [datetime] NOT NULL,
[Refresh_Date] [datetime] NOT NULL,
[Person_ID] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Benefit_Type] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Benefit_Cat] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Local_CD] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Local_Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Benefit_Date] [datetime] NOT NULL,
[ENC_ID] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Start_Date] [datetime] NULL,
[End_Date] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [CHORDS].[BENEFIT] ADD CONSTRAINT [FK_CHORDS_Benefit_DEMOGRAPHICS] FOREIGN KEY ([Person_ID]) REFERENCES [CHORDS].[DEMOGRAPHICS] ([PERSON_ID])
GO
ALTER TABLE [CHORDS].[BENEFIT] ADD CONSTRAINT [FK_CHORDS_Benefit_Encounters] FOREIGN KEY ([ENC_ID]) REFERENCES [CHORDS].[ENCOUNTERS] ([ENC_ID])
GO
