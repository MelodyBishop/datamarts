CREATE TABLE [STAGING].[CHORDS_ID]
(
[MRN] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PERSON_ID] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [STAGING].[CHORDS_ID] ADD CONSTRAINT [person_id] PRIMARY KEY CLUSTERED  ([PERSON_ID]) ON [PRIMARY]
GO
