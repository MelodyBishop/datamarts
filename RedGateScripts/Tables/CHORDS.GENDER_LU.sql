CREATE TABLE [CHORDS].[GENDER_LU]
(
[ABBREVIATION] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DESCRIPTION] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [CHORDS].[GENDER_LU] ADD CONSTRAINT [PK_GENDER_LU] PRIMARY KEY CLUSTERED  ([ABBREVIATION]) ON [PRIMARY]
GO
