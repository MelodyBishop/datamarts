CREATE TABLE [CHORDS].[ONC_SMOKING_STATUS_LU]
(
[ABBREVIATION] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DESCRIPTION] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [CHORDS].[ONC_SMOKING_STATUS_LU] ADD CONSTRAINT [PK_ONC_SMOKING_STATUS_LU] PRIMARY KEY CLUSTERED  ([ABBREVIATION]) ON [PRIMARY]
GO
