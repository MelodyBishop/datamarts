CREATE TABLE [CHORDS].[ENROLLMENT]
(
[PERSON_ID] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ENR_START] [date] NOT NULL,
[ENR_END] [date] NOT NULL,
[INS_MEDICAID] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[INS_COMMERCIAL] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[INS_PRIVATEPAY] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[INS_STATESUBSIDIZED] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[INS_SELFFUNDED] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[INS_HIGHDEDUCTIBLE] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[INS_MEDICARE] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[INS_MEDICARE_A] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[INS_MEDICARE_B] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[INS_MEDICARE_C] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[INS_MEDICARE_D] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[INS_OTHER] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PLAN_HMO] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PLAN_POS] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PLAN_PPO] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PLAN_INDEMNITY] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DRUGCOV] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OUTSIDE_UTILIZATION] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ENROLLMENT_BASIS] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PCC] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PCP] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [PK_CHORDS_ENROLLMENT] PRIMARY KEY CLUSTERED  ([PERSON_ID], [ENR_START]) ON [PRIMARY]
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_DEMOGRAPHICS] FOREIGN KEY ([PERSON_ID]) REFERENCES [CHORDS].[DEMOGRAPHICS] ([PERSON_ID])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_ENROLLMENT_BASIS_LU] FOREIGN KEY ([ENROLLMENT_BASIS]) REFERENCES [CHORDS].[ENROLLMENT_BASIS_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_PROVIDER_SPECIALTY] FOREIGN KEY ([PCP]) REFERENCES [CHORDS].[PROVIDER_SPECIALTY] ([PROVIDER])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU] FOREIGN KEY ([INS_MEDICAID]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU10] FOREIGN KEY ([INS_MEDICARE_C]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU11] FOREIGN KEY ([INS_MEDICARE_D]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU12] FOREIGN KEY ([INS_OTHER]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU13] FOREIGN KEY ([PLAN_HMO]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU14] FOREIGN KEY ([PLAN_POS]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU15] FOREIGN KEY ([PLAN_PPO]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU16] FOREIGN KEY ([PLAN_INDEMNITY]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU17] FOREIGN KEY ([DRUGCOV]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU18] FOREIGN KEY ([OUTSIDE_UTILIZATION]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU2] FOREIGN KEY ([INS_COMMERCIAL]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU3] FOREIGN KEY ([INS_PRIVATEPAY]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU4] FOREIGN KEY ([INS_STATESUBSIDIZED]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU5] FOREIGN KEY ([INS_SELFFUNDED]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU6] FOREIGN KEY ([INS_HIGHDEDUCTIBLE]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU7] FOREIGN KEY ([INS_MEDICARE]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU8] FOREIGN KEY ([INS_MEDICARE_A]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[ENROLLMENT] ADD CONSTRAINT [FK_CHORDS_ENROLLMENT_YNU_LU9] FOREIGN KEY ([INS_MEDICARE_B]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
