CREATE TABLE [STAGING].[Vital_Signs]
(
[VITAL_SIGNS_ID] [int] NOT NULL IDENTITY(1, 1),
[PERSON_ID] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MEASURE_DATE] [date] NOT NULL,
[MEASURE_TIME] [time] NOT NULL,
[ENC_ID] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENCTYPE] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HT_RAW] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WT_RAW] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HT] [decimal] (7, 3) NULL,
[WT] [decimal] (8, 4) NULL,
[BMI_RAW] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DIASTOLIC] [numeric] (3, 0) NULL,
[SYSTOLIC] [numeric] (3, 0) NULL,
[DIASTOLIC_RAW] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SYSTOLIC_RAW] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BP_TYPE] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSITION] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HEAD_CIR_RAW] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RESPIR_RAW] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TEMP_RAW] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PULSE_RAW] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [STAGING].[Vital_Signs] ADD CONSTRAINT [PK__Vital_Si__F95DF38F964EF0F4] PRIMARY KEY NONCLUSTERED  ([VITAL_SIGNS_ID]) ON [PRIMARY]
GO
