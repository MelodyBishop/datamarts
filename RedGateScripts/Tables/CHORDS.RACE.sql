CREATE TABLE [CHORDS].[RACE]
(
[patid] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Race1] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Race2] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Race3] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Race4] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Race5] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
