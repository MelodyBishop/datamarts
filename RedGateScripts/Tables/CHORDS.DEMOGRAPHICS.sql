CREATE TABLE [CHORDS].[DEMOGRAPHICS]
(
[PERSON_ID] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MRN] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BIRTH_DATE] [datetime] NULL,
[GENDER] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__DEMOGRAPH__GENDE__31B762FC] DEFAULT ('U'),
[PRIMARY_LANGUAGE] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__DEMOGRAPH__PRIMA__32AB8735] DEFAULT ('unk'),
[NEEDS_INTERPRETER] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__DEMOGRAPH__NEEDS__339FAB6E] DEFAULT ('U'),
[RACE1] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__DEMOGRAPH__RACE1__3493CFA7] DEFAULT ('UN'),
[RACE2] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__DEMOGRAPH__RACE2__3587F3E0] DEFAULT ('UN'),
[RACE3] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__DEMOGRAPH__RACE3__367C1819] DEFAULT ('UN'),
[RACE4] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__DEMOGRAPH__RACE4__37703C52] DEFAULT ('UN'),
[RACE5] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__DEMOGRAPH__RACE5__3864608B] DEFAULT ('UN'),
[HISPANIC] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__DEMOGRAPH__HISPA__395884C4] DEFAULT ('U'),
[SEXUAL_ORIENTATION] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__DEMOGRAPH__SEXUA__3A4CA8FD] DEFAULT ('UN'),
[GENDER_IDENTITY] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__DEMOGRAPH__GENDE__3B40CD36] DEFAULT ('UN')
) ON [PRIMARY]
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] ADD CONSTRAINT [PK_CHORDS_DEMOGRAPHICS] PRIMARY KEY CLUSTERED  ([PERSON_ID]) ON [PRIMARY]
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] WITH NOCHECK ADD CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_GENDER_IDENTITY_LU] FOREIGN KEY ([GENDER_IDENTITY]) REFERENCES [CHORDS].[GENDER_IDENTITY_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] WITH NOCHECK ADD CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_GENDER_LU] FOREIGN KEY ([GENDER]) REFERENCES [CHORDS].[GENDER_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] WITH NOCHECK ADD CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_LANGUAGES_ISO] FOREIGN KEY ([PRIMARY_LANGUAGE]) REFERENCES [CHORDS].[LANGUAGES_ISO_LU] ([ISO_639-2_CODE])
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] WITH NOCHECK ADD CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_RACE_LU] FOREIGN KEY ([RACE1]) REFERENCES [CHORDS].[RACE_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] WITH NOCHECK ADD CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_RACE_LU2] FOREIGN KEY ([RACE2]) REFERENCES [CHORDS].[RACE_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] WITH NOCHECK ADD CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_RACE_LU3] FOREIGN KEY ([RACE3]) REFERENCES [CHORDS].[RACE_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] WITH NOCHECK ADD CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_RACE_LU4] FOREIGN KEY ([RACE4]) REFERENCES [CHORDS].[RACE_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] WITH NOCHECK ADD CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_RACE_LU5] FOREIGN KEY ([RACE5]) REFERENCES [CHORDS].[RACE_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] WITH NOCHECK ADD CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_SEXUAL_ORIENTATION_LU] FOREIGN KEY ([SEXUAL_ORIENTATION]) REFERENCES [CHORDS].[SEXUAL_ORIENTATION_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] WITH NOCHECK ADD CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_YNU_LU] FOREIGN KEY ([NEEDS_INTERPRETER]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] WITH NOCHECK ADD CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_YNU_LU2] FOREIGN KEY ([HISPANIC]) REFERENCES [CHORDS].[YNU_LU] ([ABBREVIATION])
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] NOCHECK CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_GENDER_LU]
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] NOCHECK CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_LANGUAGES_ISO]
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] NOCHECK CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_RACE_LU]
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] NOCHECK CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_RACE_LU2]
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] NOCHECK CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_RACE_LU3]
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] NOCHECK CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_RACE_LU4]
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] NOCHECK CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_RACE_LU5]
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] NOCHECK CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_SEXUAL_ORIENTATION_LU]
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] NOCHECK CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_YNU_LU]
GO
ALTER TABLE [CHORDS].[DEMOGRAPHICS] NOCHECK CONSTRAINT [FK_CHORDS_DEMOGRAPHICS_YNU_LU2]
GO
