CREATE TABLE [STAGING].[Bad_Birthdays]
(
[MRN] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PERSON_ID] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BIRTHDAY] [datetime] NULL
) ON [PRIMARY]
GO
