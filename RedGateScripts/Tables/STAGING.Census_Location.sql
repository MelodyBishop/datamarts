CREATE TABLE [STAGING].[Census_Location]
(
[PERSON_ID] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LOC_START] [date] NOT NULL,
[LOC_END] [date] NULL,
[Address1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address3] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CITY] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP_FOUR] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[County] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [STAGING].[Census_Location] ADD CONSTRAINT [PK_CHORDS_CENSUS_LOCATION] PRIMARY KEY CLUSTERED  ([PERSON_ID], [LOC_START]) ON [PRIMARY]
GO
