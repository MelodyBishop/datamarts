
USE [Datamart2]
GO

/****** Object:  Table [Reports].[Master_Roster]    Script Date: 9/17/2018 11:03:35 AM ******/
DROP TABLE [Reports].[Master_Roster]
GO

/****** Object:  Table [Reports].[Master_Roster]    Script Date: 9/17/2018 11:03:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Reports].[Master_Roster](
	[Employee Number] [nvarchar](255) NULL,
	[NPI] [nvarchar](255) NULL,
	[Employee Last Name] [nvarchar](255) NULL,
	[Employee First Name] [nvarchar](255) NULL,
	[Employee Middle Name] [nvarchar](255) NULL,
	[License Type] [nvarchar](255) NULL,
	[Current State License #] [nvarchar](255) NULL,
	[Initial License Issue Date] [datetime] NULL,
	[Current License expiration date] [datetime] NULL,
	[Most Recent Credentialling Committee Decision Date] [datetime] NULL,
	[Credentialling Committee Decision Date Prior] [datetime] NULL,
	[Initial Credentialing Date] [datetime] NULL,
	[Date Hired] [datetime] NULL,
	[Degree] [nvarchar](255) NULL,
	[DOB] [datetime] NULL,
	[Gender] [nvarchar](255) NULL,
	[Race] [nvarchar](255) NULL,
	[Ethnicity] [nvarchar](255) NULL,
	[Languages SpokenOtherThanEnglish] [nvarchar](255) NULL,
	[Speciality] [nvarchar](255) NULL,
	[Location #] [int] NULL,
	[Med School or Highest Level of Education (Non MD)] [nvarchar](255) NULL,
	[MS/HLE Start Date] [datetime] NULL,
	[MS/HLE End Date] [datetime] NULL,
	[LPC Only Tricare Waiver] [nvarchar](255) NULL,
	[Currently Seeing Clients?] [nvarchar](255) NULL,
	[Comments] [nvarchar](255) NULL,
	[TermDate] [date] NULL
) ON [PRIMARY]
GO


