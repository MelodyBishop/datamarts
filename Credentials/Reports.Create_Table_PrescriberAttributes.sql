

USE [Datamart2]
GO

DROP TABLE [Reports].[PrescriberAttributes]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Reports].[PrescriberAttributes](
	NPI [varchar](255) NOT NULL,
	DEA Varchar(20) NULL,
    DEAExpireDate date NULL,
	PrimarySpecialty [varchar](255) NULL,
	PrimarySpecIssueDate [date] NULL,
	PrimarySpecExpDate [date] NULL,
	SecondarySpecialty [varchar](255) NULL,
	SecondarySpecIssueDate [date] NULL,
	SecondarySpecExpDate [date] NULL,
	BoardEligible [varchar](12) NULL,
	BoardCertified [varchar](12) NULL,
	ResidencySchoolWLocation Varchar(100),
	ResidencySchoolStartDate	date,
	ResidencySchoolEndDate	date,
	FellowshipTrainingWLocation Varchar(100),
	FellowshipTrainingStartDate	date,
	FellowshipTrainingEndDate	date,

) ON [PRIMARY]
GO


