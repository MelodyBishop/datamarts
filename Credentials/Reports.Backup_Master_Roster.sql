
USE [Datamart2]
GO

DROP PROCEDURE [Reports].[Backup_Master_Roster]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		MelodyB
-- Create date: 09/17/2018
-- Description:	Backing up main Roster table
--exec 
-- =============================================
CREATE PROCEDURE [Reports].[Backup_Master_Roster] 
AS
BEGIN
	SET NOCOUNT ON;
Truncate table [Reports].[Master_Roster_BU]
 Insert into [Reports].[Master_Roster_BU]	
 SELECT * from Reports.Master_Roster
END
GO


