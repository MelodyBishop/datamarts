
Create procedure dbo.Generate_KPI_MedicaidByMonth
As
Begin
Declare 
@Begin Date
,@End date

Set @Begin = '2016-10-01 00:00:00'
Set @End = GetDAte()
Truncate table dbo.[KPI_MedicaidByMonth]
Insert into dbo.[KPI_MedicaidByMonth]
SELECT 
      pgmnum
	  ,pgmname
     , ISNULL(SUM_RVU,0) SUM_RVU
	 ,primarypayor
	 ,finalsave_ym
	 ,finalsavedate
	 ,datesvc
	 ,CCNum
	 ,CostCenter
	 ,clientkey patid
  FROM [JCMH].[dbo].[JCMH__Service]  s
  left join [dbo].[Program_To_CC_Crosswalk]c
  on s.pgmnum = c.prgnum
  where [FinalSaveDate]>=@Begin and [FinalSaveDate]<=@End
  and isenrolled='Y'
 
order by 1
--select finalsave_ym, count(*),sum(Sum_RVU) SumRVU from [dbo].[KPI_MedicaidByMonth]
--where  PrimaryPayor in ('Medicaid CAP, JCMH','Medicaid - CCHA')
--Group by finalsave_ym
--testing
--select sum(sum_rvu) from [dbo].[KPI_MedicaidByMonth]--526538client rows/$952487.13
--where finalsave_ym='201806'
--select count(*) from [dbo].[KPI_MedicaidByMonth] order by 1 desc
--select * from [dbo].[KPI_MedicaidByMonth] where primarypayor like '%7 CCHA%'
End
Go

