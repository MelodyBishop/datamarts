USE [KPI]
GO

DROP TABLE [dbo].[KPI#2_NotesByMonth]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[KPI#2_NotesByMonth](
	[PATID] [varchar](20) NULL,
	[Facility] [int] NULL,
	[Episode_number] [int] NULL,
	[NOT_uniqueid] [varchar](20) NULL,
	[CountOfServices] [int] NULL,
	[join_to_tx_history] [varchar](40) NULL,
	[Program] [varchar](100) NULL,
	[ProgramExport] [varchar](100) NULL,
	[Service] [varchar](100) NULL,
	[ServiceExport] [varchar](100) NULL,
	[DOS] [date] NULL,
	[Service_YYYYMM] [int] NULL,
	CostCenter varchar(100) NULL,
	CCNum int,
	BaseRVU Decimal(18,2) NULL,
	SUMRVU decimal(18,2) NULL,
	Duration decimal(18,2) NULL,
	Units int NULL,
	CostOfService decimal(18,2) NULL

) ON [PRIMARY]
GO


