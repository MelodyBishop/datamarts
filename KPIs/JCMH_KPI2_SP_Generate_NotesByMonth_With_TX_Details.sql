USE [KPI]
GO
DROP PROCEDURE [dbo].[Generate_KPI_NotesByMonth_With_TX_Details]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-------------------------------------------------
--Purpose: To grab TX and payment info and match up to Notes By Month
--Authored:  Melody Bishop
--Date: 09/10/2018
--Include ALL guarantors and date back to BOY 2018 only

--exec [dbo].[Generate_KPI_NotesByMonth_With_TX_Details]
-------------------------------------------------------
CREATE PROCEDURE [dbo].[Generate_KPI_NotesByMonth_With_TX_Details]
as
begin
Truncate table kpi.dbo.[KPI#2_NotesByMonth_With_TX_Details]
Insert into kpi.dbo.[KPI#2_NotesByMonth_With_TX_Details]
select 
n.patid dPATID,
n.[NOT_uniqueid] dNOT_uniqueID,
n.join_to_tx_history dJoin_to_tx_history,
n.Program dProgram,
n.ProgramExport,
Service dService,
ServiceExport,
n.DOS dDOS,
Service_YYYYMM,
b.Guarantor_ID,
g.Guarantor_Name,
Case b.row when 1 then 1 else 0 end CountOfServices,
Case b.row when 1 then Case IsNumeric(claim_number) when 1 then 1 else 0 end else 0 end CountOfClaims,
Case b.row when 1 then Case IsNumeric(claim_number) when 1 then guarantor_liability else 0 end else 0 end ClaimsAmount,
b.[Claim_number],
b.[v_claim_date],
--IsNull(p.payment_amount,0) Guarantor_Total_Payments,
IsNull(b.guarantor_liability,0) Guarantor_Liability,
CostCenter dCostCenter,
CCNum dCCNUM,
Case b.Guarantor_id when 1065 then 1 else 0 end CountOfMedCap,
Case b.Guarantor_id when 99999 then 1 when 2000 then 1 when 2001 then 1 else 0 end CountOfWOff,
Case b.Guarantor_id when 99999 then guarantor_liability when 2000 then guarantor_liability when 2001 then guarantor_liability else 0 end WriteOffLiability,
[BaseRVU],
[SUMRVU],
[Duration],
Case IsNumeric(baseRVU) when 0 then 0 when 1 then Units else Units end Units,
[CostOfService]--,
--Case b.guarantor_id when 1116 then 1 else Case isnull(payment_amount,0) when 0 then 0 else 1 end end CountOfGuarPmts

From [dbo].[KPI#2_NotesByMonth] n
left join (Select Row_Number() over(Partition by patid, facility, date_of_service,join_to_tx_history 
							 order by patid, facility, date_of_service,join_to_tx_history ) as Row
			,patid, facility, date_of_service,guarantor_id, join_to_tx_history, guarantor_liability,claim_number
			, v_claim_date 
			from AVATARDW.[SYSTEM].[billing_tx_charge_detail]) b
			on n.patid = b.patid
			and n.[JOIN_TO_TX_HISTORY] = b.[JOIN_TO_TX_HISTORY]
			and n.facility = b.facility
--left join (Select patid, guarantor_id,type_of_payment,payment_amount, join_to_tx_history, Id 
--				from AVATARDW.[SYSTEM].[billing_pay_adj_history]
--				where type_of_payment = 'PAYMENT') p
--			on n.patid = p.patid
--			and b.[JOIN_TO_TX_HISTORY] = p.[JOIN_TO_TX_HISTORY]
--			and b.[JOIN_TO_PAYMENT_HISTORY_] =p.id
--			and b.guarantor_id = p.guarantor_ID
left join avatardw.system.billing_guar_table g
on b.guarantor_id = g.GUARANTOR_ID
where DOS is not null
and DOS < DateAdd(d,1,DateAdd(d,0,GetDate()))
and DOS > '2018-01-01'
--order by 1,2,3
end
GO


