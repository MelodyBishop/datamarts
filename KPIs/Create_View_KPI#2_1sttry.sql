

USE [KPI]
GO

/****** Object:  View [dbo].[vw_KPI#2]    Script Date: 10/10/2018 11:48:22 AM ******/
DROP VIEW [dbo].[vw_KPI#2]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_KPI#2]
AS
SELECT 
d.dPATID
, d.dNOT_uniqueid, 
d.djoin_to_tx_history
, d.dProgram, 
d.ProgramExport
, d.dService
, d.ServiceExport, 
d.dDOS, d.Service_YYYYMM, d.Guarantor_ID, 
d.CountOfServices, d.CountOfClaims, 
d.ClaimsAmount, d.Claim_number, 
d.v_claim_date, p.Payments, 
CASE WHEN d.Guarantor_ID = 1116 THEN 1 ELSE p.CountPayments END AS CountOfPayments,
p.Adjustments, p.CountAdjustments, 
p.ContrAdjs, p.CountContrAdjs, 
d.dCostCenter, d.dCCNum
FROM     dbo.KPI#2_NotesByMonth_With_TX_Details d 
INNER JOIN dbo.KPI#2_NotesByMonth_With_Pmt_Details p 
ON d.dPATID = p.PATID 
AND d.Service_YYYYMM = p.Pay_YYYYMM 
AND d.djoin_to_tx_history = p.pJoin_to_TX_History 
AND d.dDOS = p.pDOS 
AND d.dService = p.pService 
AND d.guarantor_id = p.pGuarantor_id
GO


