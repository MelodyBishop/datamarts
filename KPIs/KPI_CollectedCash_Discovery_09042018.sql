USE KPI
Truncate table [dbo].[KPI_PaymentsByMonth]

Insert into [dbo].[KPI_PaymentsByMonth]
Select 
patid, Claim#, Claim_Date,YYYYMM, Re_bill_Status,SumClaimLiab,   Sum(ContractualValue) SumContractualValue
,Sum([Payment Insurance]) SumPaymentInsurance,	GUARANTOR_ID
From 
(
SELECT c.[PATID]
      ,[CLAIM_NUMBER] Claim#
      ,[claim_date]
      ,cast(year([date_of_claim]) as char(4)) + right('0' + cast(month([date_of_claim]) as varchar), 2) YYYYMM
      ,[re_bill_status]
      ,[total_claim_liability] SumClaimLiab
	 --  ,[date_charge_closed]
	  ,Case pay.[payment_type_value] when 'Contractual Adjustment' then pay.[payment_amount] else 0.00 end 'ContractualValue'
	   ,Case pay.[payment_type_value] when 'Payment Insurance' then pay.[payment_amount]  else 0.00 end 'Payment Insurance' d 
	,c.GUARANTOR_ID
	 -- ,pay.check_number
	 -- ,c.[billing_claim_history_tx_detail]
	 -- ,c.[JOIN_TO_PAYMENT_HISTORY]
	 -- ,pay.[JOIN_TO_PAYMENT_HISTORY]
  FROM [AvatarDW].[SYSTEM].[billing_claim_history_tx_detail] c
 -- left join [SYSTEM].[billing_tx_charge_totals] b
  --on c.patid = b.patid 
  --and c.JOIN_TO_TX_HISTORY = b.Join_to_tx_history
  --and c.facility = b.facility
 left join AVATARDW.[SYSTEM].[billing_pay_adj_history] pay
on c.patid = pay.patid 
and c.facility = pay.facility
and c.JOIN_TO_PAYMENT_HISTORY = pay.[JOIN_TO_PAYMENT_HISTORY]
 
  --where --claim_number>=1 and  claim_number<3
   -- c.guarantor_id in (Select guarantor_id from system.billing_guar_table where guarantor_name = 'Medicaid - CCHA')
  
 
  Group by c.[PATID]
	  ,[CLAIM_NUMBER]
      ,[claim_date]
     -- ,[clm_sub_reas_value] 
      ,[date_of_claim]
	  ,cast(year([date_of_claim]) as char(4)) + right('0' + cast(month([date_of_claim]) as varchar), 2)
	  ,[re_bill_status]
	  ,[total_claim_liability]
	  ,Case pay.[payment_type_value] when 'Contractual Adjustment' then pay.[payment_amount] else 0.00 end
	  ,Case pay.[payment_type_value] when 'Payment Insurance' then pay.[payment_amount]  else 0.00 end 
	  ,c.Guarantor_ID
) sub
GRoup by patid, Claim#, Claim_Date,YYYYMM, Re_bill_Status,SumClaimLiab,GUARANTOR_ID
 
order by claim#