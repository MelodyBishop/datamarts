
Declare 
@Begin Date
,@End date

Set @Begin = '2018-07-01 00:00:00'
Set @End = '2018-07-31 23:59:59'
--TRUNCATE TABLE dbo.KPI_MedicaidByMonth
--INSERT INTO  dbo.KPI_MedicaidByMonth
--Select distinct  clientkey, Month(datesvc) MonthNum,Datename(Month,datesvc) Month, Year(dateSvc) Year , dateSvc ServiceDate,Gender, GenColor
 --	from(
use jcmh
SELECT 
      pgmnum
	  ,pgmname
     , Sum(s.SUM_RVU) SUM_RVU
  FROM [JCMH].[dbo].[JCMH__Service]  s
  --left join KPi.[dbo].[Program_TO_CC_Crosswalk] cw
  --on s.pgmnum = cw.prgnum
  where PrimaryPayor in ('Medicaid CAP, JCMH','Medicaid - CCHA')
 --and iseligible='y'
  and [FinalSaveDate]>=@Begin and [FinalSaveDate]<=@End
 -- and medicaidID is not null
Group by pgmnum,pgmname
order by 1

--select * from dbo.KPI_MedicaidByMonth
--246,449 w/o WM or CCC