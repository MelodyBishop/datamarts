USE [KPI]
GO
DROP PROCEDURE [dbo].[Generate_KPI_NotesByMonth_With_Claims_Details]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-------------------------------------------------
--Purpose: To grab Claims and payment info and match up to Notes By Month
--Authored:  Melody Bishop
--Date: 09/10/2018
--Include ALL guarantors and date back to BOY 2018 only

-------------------------------------------------------
CREATE PROCEDURE [dbo].[Generate_KPI_NotesByMonth_With_Claims_Details]
as
begin
--exec [dbo].[Generate_KPI_NotesByMonth_With_Claims_Details]

Truncate table kpi.dbo.[KPI_NotesByMonth_With_Claim_Details]
Insert into kpi.dbo.[KPI_NotesByMonth_With_Claim_Details]
select 
1 RowClaims,
n.patid,
n.[NOT_uniqueid],
n.CountOfService,
n.join_to_tx_history,
Program,
Service,
n.DOS,
DF,
Service_YYYYMM,
b.Guarantor_ID,
g.Guarantor_Name,
b.[Claim_number],
b.[v_claim_date],
b.guarantor_total_payments
from [dbo].[KPI_NotesByMonth] n
inner join AVATARDW.[SYSTEM].[billing_tx_charge_detail] b
on n.patid = b.patid
and n.[JOIN_TO_TX_HISTORY] = b.[JOIN_TO_TX_HISTORY]
left join avatardw.system.billing_guar_table g
on b.guarantor_id = g.GUARANTOR_ID
where n.DOS>'2016-09-30'
--and n.patid=102658
--and g.guarantor_name in ('Medicaid CAP, JCMH', 'Medicaid - CCHA')
and b.v_claim_date is not null
end
GO


