
USE [KPI]
GO

/****** Object:  Table [dbo].[KPI_MedicaidByMonth]    Script Date: 9/18/2018 9:52:21 AM ******/
DROP TABLE [dbo].[KPI_MedicaidByMonth]
GO

/****** Object:  Table [dbo].[KPI_MedicaidByMonth]    Script Date: 9/18/2018 9:52:21 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[KPI_MedicaidByMonth](
	[pgmname] [varchar](100) NULL,
	[pgmnum] [int] NULL,
	[SUM_RVU] [decimal](18, 2) NULL,
	[SvcCount] [int] NULL,
	[finalsave_ym] [varchar](6) NULL,
	[YYYY] int,
	[MM] int,
	[CostCenter] [varchar](100) NULL,
	[primarypayor] [varchar](100) NULL
) ON [PRIMARY]
GO


