
USE [KPI]
GO

DROP PROCEDURE [dbo].[JCMH_SP_KPI1_MedicaidByMonth_WDetails]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------
----Melody Bishop
----Created 9/19/2018
----Pulls details from JCMH.Avatar_Service table for KPI #1 

-- --exec [dbo].[JCMH_SP_KPI1_MedicaidByMonth_WDetails]
------------------------------------------------------------
Create procedure [dbo].[JCMH_SP_KPI1_MedicaidByMonth_WDetails]
As
Begin
Declare 
@Begin Date
,@End date

Set @Begin = '2018-06-01 00:00:00'
Set @End = GetDAte()
Truncate table dbo.[KPI_MedicaidByMonth_WDetails]
Insert into dbo.[KPI_MedicaidByMonth_WDetails]

select  1 ServiceCount
,Case ISNULL(Calc_units,0) when 0 then 0 else 1 end BillSvcCount
,[service_program_code]+'-'+[service_program_value] Program
, Case base_rvu when '' then '0' else IsNULL(base_rvu,0) end Base_RVU
, IsNull(sum_rvu,0) SUM_RVU
, UnitCost
,Cost_of_Service
--,[datesvc_yyyy] YYYY
--,[datesvc_mm] MMM
,Left(datesvc_ymd,6) DOS_YYYYMM
,CostCenter
,primary_guarantor PrimaryPayor
,Duration
,CPT_code
,Location_value Location
,FinalSave FinalSavedDate
,[service_charge_code]+'-'+[service_charge_value] ServiceCharge
,Practitioner_name

from jcmh.[dbo].[JCMH__Service_Avatar_research] s --was.[dbo].[JCMH__SERVICE] s
left join [dbo].[Program_To_CC_Crosswalk]c
  on s.[service_program_code] = c.prgnum
where 
primary_guarantor in (1116)
and DateSvc_ymd>=@Begin and DateSvc_ymd<=@End
and patient_name_last not like 'TEST%'
and patient_name_last not like 'GROUP%'
and patient_name_last not like 'ZZZ%'
End
GO


