--USE [KPI]
--GO
--DROP PROCEDURE [dbo].[Generate_KPI_NotesByMonth_With_Pmt_Details]
--GO
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---------------------------------------------------
----To grab Notes and Paid Claim details
----Authored:  Melody Bishop
----Date: 09/11/2018
---------------------------------------------------------
--CREATE PROCEDURE [dbo].[Generate_KPI_NotesByMonth_With_Pmt_Details]
--as
--begin
----exec dbo.Generate_KPI_NotesByMonth_With_Pmt_Details

--Truncate table kpi.dbo.[KPI_NotesByMonth_With_Pmt_Details]
--Insert into kpi.dbo.[KPI_NotesByMonth_With_Pmt_Details]
select 
n.patid,
n.dos,
n.claim_number
--pmt.date_of_service,
--pmt.Paid_amount,
--g.guarantor_name,
--pmt.insurance
from [dbo].[KPI_NotesByMonth_With_Claim_Details] n
--inner join (SELECT [ClaimID]
--,patientAcct
--      ,Replace(Left(Replace(Cast([PatientAcct] as varchar(255)),'CX',''),charindex('X',Replace(Cast([PatientAcct] as varchar(255)),'CX',''),0)),'X','') ClaimRec
 --     ,insurance
--	  ,[date_of_service]
 --     ,[Billed_Amount]
 --     ,[Denied_Amount]
  --    ,[received_date]
  --    ,[Paid_Amount]
 -- FROM [KPI].[dbo].[Navicure_Payments] where  patientacct like 'CX%'
 -- ) pmt
--on n.claim_number = Replace(Left(Replace(Cast(PatientAcct as varchar(255)),'CX',''),charindex('X',Replace(Cast(PatientAcct as varchar(255)),'CX',''),0)),'X','')--REplace(REplace(claimrec,'CX',''),'X','')
--left join avatardw.system.billing_guar_table g
--on n.guarantor_id = g.GUARANTOR_ID
where --n.DOS='2018-06-08'
--and 
claim_number=192104
--and n.patid=102658
--and g.guarantor_name in ('Medicaid CAP, JCMH', 'Medicaid - CCHA')
--end
--GO


