USE [KPI]
GO

DROP PROCEDURE [dbo].[Generate_KPI2_NotesByMonth]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------
--KPI #2
--To Gather Notes History with some billing details 
-- Author: Melody Bishop
-- Date Created: 9/11/2018
--There will be no guarantor here until the note is finalized
--Draft notes considered to be too small to include, but do if I can
--Those with no billing count as a service
--Excluding notes with 0 duration
-- exec [dbo].[Generate_KPI2_NotesByMonth]
----------------------------------------------
CREATE PROCEDURE [dbo].[Generate_KPI2_NotesByMonth]
as
begin
Truncate table kpi.dbo.KPI#2_NotesByMonth
Insert into kpi.dbo.KPI#2_NotesByMonth

select 
n.patid,
1 Facility,
n.txhist_episode Episode_Number,
[NOT_uniqueid],
1 CountOfServices,
n.join_to_tx_history,
service_program_code Program,
service_program_code + '-' + service_program_Value ProgramExport,
service_charge_code Service,
service_charge_code + '-' + service_charge_value ServiceExport,
n.date_of_service DOS,
cast(year(n.date_of_service) as char(4)) + right('0' + cast(month(n.date_of_service) as varchar), 2) Service_YYYYMM,
CostCenter,
CCNum,
Case IsNumeric(base_RVU) when 0 then 0.00 when 1 then ISNULL(Base_RVU,0.00) else 0 end BaseRVU,
ISNULL(Sum_RVU,0) SUMRVU,
Duration,
Case IsNumeric(base_RVU) when 0 then 0 when 1 then Calc_Units else Calc_Units end Units,
Cost_of_service CostOfService
from  JCMH.[dbo].[JCMH__Service_Avatar_research] n
left join [dbo].[Program_To_CC_Crosswalk] c
on n.service_program_code = c.PrgNum
where duration <>0
and n.date_of_service is not null
and n.date_of_service<DateAdd(d,1,DateAdd(d,0,GetDate()))
and n.date_of_service>='2018-01-01'--771673 rows, no duration=0 769,165

end
GO


