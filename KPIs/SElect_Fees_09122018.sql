SELECT   [ID]
      ,[SERVICE_CODE] SVCCd
      ,[charge]
      ,[cpt_code]
      ,[cpt_value]
      ,[duration_range]
      ,[effective_date]
      ,[fee]
      --,[fee_type_code]
      ,[fee_type_value]
      ,[modifier]
      ,[modifier_x_ref]
      ,[service_code_description]
      ,[service_value]
      ,[state_rp_code_1] RVUValue --if we own the location we receive a higher RVU
      ,[state_rp_code_2] FacilityRVUValue ---we do not own many locations and receive different RVU for those
      ,[state_rp_code_3]
      ,[state_rp_code_4]
  FROM [AvatarDW].[SYSTEM].[billing_tx_master_fee_table]
  where end_date is null