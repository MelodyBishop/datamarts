USE [KPI]
GO
DROP PROCEDURE [dbo].[Create_RVU_Breakdown]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------
--Melodyb
--09/17/2018
--Notes:  Need to know what to do with base_RVU=''
--Need to know whether to use primary_guarantor or the guar order as suggested by Will

-------------------------------------------------------------------
--exec [dbo].[Create_RVU_Breakdown]
Create procedure [dbo].[Create_RVU_Breakdown]
as
Begin
Truncate table [dbo].[KPI_RVU_Breakdown]
Insert into [dbo].[KPI_RVU_Breakdown]

Select 
left(yyyymm,4) YYYY,
Right(yyyymm,2) MM,
yyyymm
,ccnum
,Left(costcenter,23)
,prgnum
,Sum(CountReportableServices) CountReportableSvc
,Sum(RVUServiceUnits) CountofBillableSvc
,Sum(ExtendedRVUxCFAmount) SumRVUExt$ 
,Sum(ExtendedRVUxCFAmount) SumRVUExt$ 
from 
(select
prgnum,
ccnum,
costcenter,
1 CountReportableServices,
Case ISNULL(calc_Units,0) when 0 then 0 else 1 end RVUServiceUnits
,left(datesvc_ymd,6) yyyymm,
[service_program_code] pgmnum,
ISNULL(Base_RVU,0) BaseSvc,
ISNULL(calc_Units,0) RVU_Units,
ISNULL(Sum_RVU,0) ExtRVU,
86.79 CF,
ISNULL(Sum_RVU,0)*86.79 ExtendedRVUxCFAmount
from JCMH.[dbo].[JCMH__SERVICE_avatar_research]
left join [dbo].[Program_To_CC_Crosswalk]
on [service_program_code]=prgnum
where datesvc_ymd between '20161001' and '20180731'
and primary_payor_guar_order in (1116,1065) --use this per Will...question out with Mary why so many changes, verify we use this or not.
--and costcenter='Access/Central Intake'
--and left(datesvc_ymd,6) = 201807
)sub
Group by yyyymm,ccnum,costcenter,prgnum
END
GO


