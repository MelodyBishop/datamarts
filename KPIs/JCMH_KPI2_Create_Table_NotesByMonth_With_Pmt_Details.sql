USE [KPI]
GO

DROP TABLE [dbo].[KPI#2_NotesByMonth_With_Pmt_Details]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[KPI#2_NotesByMonth_With_Pmt_Details](
	[PATID] [varchar](20) NULL,
	[Pay_YYYYMM] [int] NULL,
	[pJoin_to_TX_History] [varchar](40) NULL,
	[pDOS] [datetime] NULL,
	[pGuarantor_id] [int] NULL,
	[Payments] [decimal](18, 2) NULL,
	CountPayments int NULL,
	[Adjustments] [decimal](18, 2) NULL,
	CountAdjustments int NULL,
	[ContrAdjs] [decimal](18, 2) NULL,
	CountContrAdjs int NULL,
	[pService] [varchar](100) NULL,
	[pCostCenter] [varchar](100) NULL,
	[pCCNum] [int] NULL,
	[pProgram] [varchar](100) NULL,
	[pProgramExport] [varchar](100) NULL
) ON [PRIMARY]
GO


