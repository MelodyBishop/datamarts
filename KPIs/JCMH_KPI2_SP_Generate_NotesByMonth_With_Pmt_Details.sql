USE [KPI]
GO

DROP PROCEDURE [dbo].[Generate_KPI2_NotesByMonth_With_PmtDetails]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-------------------------------------------------------
--KPI#2 - Generate Payments for each service
--Author: MelodyB
--exec [dbo].[Generate_KPI2_NotesByMonth_With_PmtDetails]
--------------------------------------------------------------
Create procedure [dbo].[Generate_KPI2_NotesByMonth_With_PmtDetails]
as
Begin
Truncate table [dbo].[KPI#2_NotesByMonth_With_Pmt_Details]
Insert into [dbo].[KPI#2_NotesByMonth_With_Pmt_Details]
select 
n.dpatid 
,Cast(Year(n.dDOS) as varchar)+Right('00'+Cast(Month(n.dDOS) as varchar(2)),2) Pay_YYYYMM,
n.djoin_to_tx_history pJoin_To_Tx_History
,n.dDOS pDOS
, n.guarantor_id pGuarantor_ID
, ISNULL(pmt.payment_amount,0) Payments
, ISNULL(adj.payment_amount,0) Adjustments
, ISNULL(ContrAdj.payment_amount,0) ContrAdjs
, n.dService pService
,dCostCenter pCostCenter
,dCCNum pCCNum
,dProgram pProgram
,ProgramExport pProgramExport
from [dbo].[KPI#2_NotesByMonth_With_TX_Details] n
left join (Select patid, facility, join_to_tx_history,guarantor_id,payment_type_code, Sum(payment_amount) Payment_Amount
			  from AVATARDW.system.billing_pay_adj_history
			  WHERE Type_Of_Payment ='PAYMENT'
			  Group by patid, facility, join_to_tx_history,guarantor_id,payment_type_code) pmt
	on pmt.patid = n.dpatid 
	and pmt.join_to_tx_history = n.djoin_to_tx_history
	and pmt.guarantor_id = n.guarantor_id
left join (Select patid, facility, join_to_tx_history,guarantor_id,payment_type_code, Sum(payment_amount) Payment_Amount
			  from AVATARDW.system.billing_pay_adj_history
			  WHERE Type_Of_Payment ='ADJUSTMENT' and payment_type_value <>'Contractual Adjustment'
			  Group by patid, facility, join_to_tx_history,guarantor_id,payment_type_code) adj
	on adj.patid = n.dpatid 
	and adj.join_to_tx_history = n.djoin_to_tx_history
	and adj.guarantor_id = n.guarantor_id
left join (Select patid, facility, join_to_tx_history,guarantor_id,payment_type_code, Sum(payment_amount) Payment_Amount
			  from AVATARDW.system.billing_pay_adj_history
			  WHERE Type_Of_Payment ='ADJUSTMENT' and payment_type_value ='Contractual Adjustment'
			  Group by patid, facility, join_to_tx_history,guarantor_id,payment_type_code) ContrAdj
	on ContrAdj.patid = n.dpatid 
	and ContrAdj.join_to_tx_history = n.djoin_to_tx_history
	and ContrAdj.guarantor_id = n.guarantor_id
--where b.date_of_service>'2018-01-01'
order by dpatid,  dDOS
End 
GO


