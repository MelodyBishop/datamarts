USE [KPI]
GO
DROP VIEW [dbo].[vw_KPI#2]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-------------------------------------------------
--Purpose: To grab TX and payment info and match up to Notes By Month
--Authored:  Melody Bishop
--Date: 09/10/2018
--Include ALL guarantors and date back to BOY 2018 only

--exec [dbo].[Generate_KPI_NotesByMonth_With_TX_Details]
-------------------------------------------------------
Create View [dbo].[vw_KPI#2]
as
Select Case row when 1 then 1 else 0 end as SvcCount,* from 
(
Select
Row_number() 
over(Partition by dpatid  , dNot_Uniqueid--, dJoin_to_tx_History,dprogram, dservice,dDOS,guarantor_id ,dCostCenter
					order by   dpatid, dNot_Uniqueid, dJoin_to_tx_History,dprogram, dservice,dDOS,guarantor_id, dCostCenter
					) 
as Row,*

--select  dpatid, dNot_Uniqueid, dJoin_to_tx_History,dprogram, dservice,dDOS,guarantor_id ,dCostCenter
From(
select 
n.patid dPATID,
n.[NOT_uniqueid] dNOT_uniqueID,
n.join_to_tx_history dJoin_to_tx_history,
n.service_program_code dProgram,
n.service_program_code + '-' + n.service_program_value ProgramExport,
n.service_charge_code dService,
n.service_charge_code + '-' + n.service_charge_value ServiceExport,
n.date_of_service dDOS,
n.datesvc_yyyy+n.datesvc_MM  Service_YYYYMM,
b.Guarantor_ID,
g.Guarantor_Name,
Case IsNumeric(claim_number) when 1 then 1 else 0 end CountOfClaims,
Case IsNumeric(claim_number) when 1 then guarantor_liability else 0 end ClaimsAmount,
b.[Claim_number],
b.[v_claim_date],
cr.CostCenter dCostCenter,
cr.CCNum dCCNUM,
Isnull([Base_RVU],0) BaseRVU,
Isnull([SUM_RVU],0) SUMRVU,
[Duration]
, ISNULL(pmt.payment_amount,0) Payments
, Case ISNULL(pmt.payment_amount,0) when 0 then 0 else 1 end  CountPayments
, ISNULL(adj.payment_amount,0) Adjustments
, Case ISNULL(adj.payment_amount,0) When 0 then 0 else 1 end CountAdjustments
, ISNULL(ContrAdj.payment_amount,0) ContrAdjs
, Case ISNULL(ContrAdj.payment_amount,0) when 0 then 0 else 1 end CountContrAdjs
from JCMH.[dbo].[JCMH__Service_Avatar_research] n
left join AVATARDW.[SYSTEM].[billing_tx_charge_detail] b
on n.patid = b.patid
and n.[JOIN_TO_TX_HISTORY] = b.[JOIN_TO_TX_HISTORY]
left join avatardw.system.billing_guar_table g
on b.guarantor_id = g.GUARANTOR_ID
left join [dbo].[Program_To_CC_Crosswalk] cr
on n.service_program_code = cr.PrgNum
left join (Select patid, facility, join_to_tx_history,guarantor_id,payment_type_code, Sum(payment_amount) Payment_Amount
			  from AVATARDW.system.billing_pay_adj_history
			  WHERE Type_Of_Payment ='PAYMENT'
			  Group by patid, facility, join_to_tx_history,guarantor_id,payment_type_code) pmt
	on pmt.patid = n.patid 
	and pmt.join_to_tx_history = n.join_to_tx_history
	and pmt.guarantor_id = b.guarantor_id
left join (Select patid, facility, join_to_tx_history,guarantor_id,payment_type_code, Sum(payment_amount) Payment_Amount
			  from AVATARDW.system.billing_pay_adj_history
			  WHERE Type_Of_Payment ='ADJUSTMENT' and payment_type_value <>'Contractual Adjustment'
			  Group by patid, facility, join_to_tx_history,guarantor_id,payment_type_code) adj
	on adj.patid = n.patid 
	and adj.join_to_tx_history = n.join_to_tx_history
	and adj.guarantor_id = b.guarantor_id
left join (Select patid, facility, join_to_tx_history,guarantor_id,payment_type_code, Sum(payment_amount) Payment_Amount
			  from AVATARDW.system.billing_pay_adj_history
			  WHERE Type_Of_Payment ='ADJUSTMENT' and payment_type_value ='Contractual Adjustment'
			  Group by patid, facility, join_to_tx_history,guarantor_id,payment_type_code) ContrAdj
	on ContrAdj.patid = n.patid 
	and ContrAdj.join_to_tx_history = n.join_to_tx_history
	and ContrAdj.guarantor_id = b.guarantor_id
where 
n.date_of_service is not null
and n.date_of_service < DateAdd(d,1,DateAdd(d,0,GetDate()))
and n.date_of_service > '2018-01-01'
--and n.patid=10003230
) s
)s2
GO


