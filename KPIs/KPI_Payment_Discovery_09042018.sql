SELECT b.[PATID]
         ,[CLAIM_NUMBER] Claim#
      ,[claim_date]
      ,[clm_sub_reas_value] ClmSubRsnVal
	  ,cast(year([date_of_claim]) as char(4)) + right('0' + cast(month([date_of_claim]) as varchar), 2) YYYYMM
      ,[re_bill_status]
      ,[total_claim_liability] SumClaimLiab
	  ,Sum(b.full_charge) SumFullCharge
	  ,Sum(b.[total_payments]) SumTotalPayments
	  ,[date_charge_closed]

  FROM [AvatarDW].[SYSTEM].[billing_claim_history_tx_detail] c
  left join [SYSTEM].[billing_tx_charge_totals] b
  on c.patid = b.patid 
  and c.JOIN_TO_TX_HISTORY = b.Join_to_tx_history
  and c.facility = b.facility
 
  where claim_number>=1 and  claim_number<3
 
  Group by b.[PATID]
	  ,[CLAIM_NUMBER]
      ,[claim_date]
      ,[clm_sub_reas_value] 
      ,[date_of_claim]
	  ,cast(year([date_of_claim]) as char(4)) + right('0' + cast(month([date_of_claim]) as varchar), 2)
	  ,[re_bill_status]
	  ,[total_claim_liability]
	  ,[date_charge_closed]
	 


