USE [KPI]
GO

DROP TABLE [dbo].[KPI#2_NotesByMonth_With_TX_Details]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[KPI#2_NotesByMonth_With_TX_Details](
	[dPATID] [varchar](20) NULL,
	[dNOT_uniqueid] [varchar](20) NULL,
	[djoin_to_tx_history] [varchar](40) NULL,
	[dProgram] [varchar](50) NULL,
	[ProgramExport] [varchar](100) NULL,
	[dService] [varchar](50) NULL,
	[ServiceExport] [varchar](100) NULL,
	[dDOS] [date] NULL,
	[Service_YYYYMM] [int] NULL,
	[Guarantor_ID] [int] NULL,
	[Guarantor_Name] [varchar](40) NULL,
	[CountOfServices] int NULL,
	[CountOfClaims] int,
	[ClaimsAmount] decimal(18,2) NULL,
	[Claim_number] [int] NULL,
	[v_claim_date] [date] NULL,
	[guarantor_total_liability] decimal(18,2) NULL,
	[dCostCenter] varchar(100) NULL,
	[dCCNum] int null,
	[CountOfMedCap] int NULL,
	[CountOfWriteOff] int NULL,
	[WriteOffLiability] decimal(18,2) NULL,
	[BaseRVU] [decimal](18, 2) NULL,
	[SUMRVU] [decimal](18, 2) NULL,
	[Duration] [decimal](18, 2) NULL,
	[Units] [int] NULL,
	[CostOfService] [decimal](18, 2) NULL
) ON [PRIMARY]
GO


