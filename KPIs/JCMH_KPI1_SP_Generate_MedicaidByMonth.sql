USE [KPI]
GO

DROP PROCEDURE [dbo].[Generate_KPI_MedicaidByMonth]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------
----Melody Bishop
----Created 9/11/2018
----Pulls from JCMH.Avatar_Service table for KPI #1 
---- Based on DateOfService, and All progress notes, no general note
---- Pull counts of services after 7/1/2018
----Only Medicaid - CCHA 

----Show Extended RVUS based on services
----Moves forward fiscal year 2018-2019 and ongoing
-- --exec [dbo].[Generate_KPI_MedicaidByMonth]
------------------------------------------------------------
Create procedure [dbo].[Generate_KPI_MedicaidByMonth]
As
Begin
Declare 
@Begin Date
,@End date

Set @Begin = '2018-06-01 00:00:00'
Set @End = GetDAte()
Truncate table dbo.[KPI_MedicaidByMonth]
Insert into dbo.[KPI_MedicaidByMonth]
Select 
pgmname
, pgmnum
, Sum(ExtRVU) ExtRVU
, count(ServiceCtr) SvcCount
,Sum(BillSvcCtr) BillableSvcCount
, yyyy+mmm 
, YYYY
,MMM
, costcenter
,primarypayor
from 
(
select  1 ServiceCtr
,Case ISNULL(Calc_units,0) when 0 then 0 else 1 end BillSvcCtr
,[service_program_value] pgmname
,[service_program_code] pgmnum
, Case base_rvu when '' then '0' else IsNULL(base_rvu,0) end Base_RVU
, IsNull(sum_rvu,0) ExtRVU
, unitcost
, IsNull(calc_units,0) units
, 1 Mult --to replace with real CV factor some day
,[datesvc_yyyy] YYYY
,[datesvc_mm] MMM
,costcenter
,primary_guarantor primarypayor
from jcmh.[dbo].[JCMH__Service_Avatar_research] s --was.[dbo].[JCMH__SERVICE] s
left join [dbo].[Program_To_CC_Crosswalk]c
  on s.[service_program_code] = c.prgnum
where 
primary_guarantor in (1116)
and DateSvc_ymd>=@Begin and DateSvc_ymd<=@End
and [patient_name_last] not like 'TEST%'
and [patient_name_last] not like 'GROUP%'
and [patient_name_last] not like 'ZZZ%'

) sub
Group by pgmname, pgmnum,YYYY,MMM, costcenter,primarypayor

End
GO


