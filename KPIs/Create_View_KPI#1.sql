
USE [KPI]
GO

DROP VIEW [dbo].[vw_KPI#1]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_KPI#1]
AS
select costcenter
, ccnum
, Sum(SvcCount) SvcCount
, pgmname
, pgmnum
, Sum(ExtRVU) SumRVU
, YYYYMM
FROM 
(select  
c.costcenter
,c.ccnum
,1 SvcCount
,[service_program_value] pgmname
,[service_program_code] pgmnum
, Case base_rvu when '' then '0' else IsNULL(base_rvu,0) end BaseRVU
, IsNull(sum_rvu,0)*86.79 ExtRVU --factor is 86.79 at 10/10/2018
, IsNull(calc_units,0) Units
,[datesvc_yyyy]+[datesvc_mm] YYYYMM
,service_charge_value
,service_charge_code
,duration
,cpt_code
,location_value
,practitioner_name
,s.date_of_service
from [dbo].[Program_To_CC_Crosswalk]c
left join  jcmh.[dbo].[JCMH__Service_Avatar_research] s 
  on s.[service_program_code] = c.prgnum

where 
primary_guarantor in (1116)
and DateSvc_ymd>='2018-07-01' and DateSvc_ymd<=GetDate()
AND [datesvc_yyyy]+[datesvc_mm] >='201807'
) sub
GRoup by costcenter, ccnum,  pgmname, pgmnum,yyyymm

GO


