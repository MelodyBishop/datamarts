/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [ClaimID]
      ,Replace(Left(Replace([PatientAcct],'CX',''),charindex('X',Replace([PatientAcct],'CX',''),0)),'X','')
      ,[date_of_service]
      ,[Billed_Amount]
      ,[Denied_Amount]
      ,[received_date]
      ,[Paid_Amount]
  FROM [KPI].[dbo].[Navicure_Payments]