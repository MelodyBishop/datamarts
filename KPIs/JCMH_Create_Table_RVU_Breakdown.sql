

USE [KPI]
GO

DROP TABLE [dbo].[KPI_RVU_Breakdown]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[KPI_RVU_Breakdown](
	yyyy int,
	[yyyymm] [int] NULL,
	[ccnum] [int] NULL,
	[costcenter] [varchar](100) NULL,
	[prgnum] [int] NULL,
	[CountReportableSvc] [int] NULL,
	[CountofBillableSvc] [int] NULL,
	[SumExtRVU] [decimal](18, 2) NULL,
	[SumRVUExt$] [decimal](18, 2) NULL
) ON [PRIMARY]
GO


