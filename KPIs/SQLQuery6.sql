/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Standard Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [KPI]
GO

/****** Object:  Table [dbo].[StrikeLines]    Script Date: 11/15/2018 4:07:45 PM ******/
DROP TABLE [dbo].[StrikeLines]
GO

/****** Object:  Table [dbo].[StrikeLines]    Script Date: 11/15/2018 4:07:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StrikeLines](
	[StrikelineDescription] [VARCHAR](30) NULL,
	YYYYMM int NULL,
	[Category] [VARCHAR](30) NULL
) ON [PRIMARY]
GO


