

USE [KPI]
GO

/****** Object:  Table [dbo].[KPI_Pmt_Details]    Script Date: 9/18/2018 5:13:30 PM ******/
DROP TABLE [dbo].[KPI_Pmt_Details]
GO

/****** Object:  Table [dbo].[KPI_Pmt_Details]    Script Date: 9/18/2018 5:13:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[KPI_Pmt_Details](
	[Pmt_patid] [varchar](20) NULL,
	[Pmt_Guarantor_ID] int NULL,
	[Pmt_NOT_uniqueid] [varchar](20) NULL,
	[Pmt_join_to_tx_history] [varchar](40) NULL,
	[Pmt_Service_YYYYMM] [int] NULL,
	[Pmt_Claim_Number] [int] NULL,
	[Pmt_Date] [date] NULL,
	[Pmt_DOS] [date] NULL,
	[Pmt_Amount] [decimal](18, 2) NULL,
	[Pmt_Program] [varchar](50) NULL,
	[Pmt_CostCenter] [varchar](100) NULL
) ON [PRIMARY]
GO


