

--row count for medicaid like payors=433627
--Program Jeffco and not CCC or WM 355,545
--2018 only 128,110 rows of services
--Exclude WM = 107,945
--Require non null Medicaid ID 97,245
--distinct clients 95,962
Declare 
@Begin Date
,@End date

Set @Begin = '2018-01-01'
Set @End = '2018-12-31'
TRUNCATE TABLE dbo.KPI_CommercialByMonth
INSERT INTO  dbo.KPI_CommercialByMonth
Select distinct  commclientkey, Month(datesvc) Mo, Year(dateSvc) Yr , dateSvc SvcDt
  	
	from
(
SELECT distinct [clientkey] CommClientKey
      --,[chargekey]
      ,[fullname]
      ,[datesvc]
      ,[descr]
      ,[pgmname]
      --,[pgmnum]--redundant with name
      ,[DeptName]
      --,[JCMH_CPT_Code]--not called for yet, just counting
      --,[FlexServCode] always null
      ,[CostClass]
      ,[UnitCost]
      ,[PrimaryPayor]
      --,[PolicyNum]--not needed for KPIs
      ,[PrimaryDX]
      --,[GAF]      --,[DX Grouping]     -- ,[JCMHID]
      --,[ClinicianName]--not in the vision yet
      --,[StaffKey]
      --,[Credentials]
      ,[PlaceOfService]
     -- ,[Duration]
     -- ,[zip]
      --,[DOB]
      --,[Age]
      --,[AgeCategory]
      --,[Gender]
      --,[County]
      --,[City]
      --,[NumofDep]
      --,[Target]--what is this?
      --,[PayorType]--redundant field with Payor
      --,[FinalSave]
      --,[FinalSave_ymd]
      --,[FinalSave_ym]
      --,[IsEncounter]
     -- ,[IsEnrolled]
      --,[MedicaidID]
      --,[IsEligible]
      --,[EligCategory]
     -- ,[CCARType]
     -- ,[Base_RVU]
     -- ,[SUM_RVU]
     -- ,[ProcKey]
     -- ,[RVUIsFacility]
     -- ,[VO_CPT_Code]
      --,[Income]
      --,[Units]
      --,[Charge_Amt]
      --,[PayorPlanKey]
      --,[opdocid]
      --,[Consolidation]--always null
      --,[Indigent]
     -- ,[current_program]
      ,[current_care_coord]
      --,[episode_program_value]
      --,[episode_program_code]
     -- ,[admit_date]
      --,[discharge_date]

  FROM [JCMH].[dbo].[JCMH__Service_Avatar]
  where PrimaryPayor NOT like '%Medicai%'
  and episode_program_code=7000
  and datesvc>=@Begin and DateSvc<=@End
  --and  consolidation is not null
  and pgmname not like 'Withdrawl Management'
  and [MedicaidID] is not null
  ) sub

order by commclientkey