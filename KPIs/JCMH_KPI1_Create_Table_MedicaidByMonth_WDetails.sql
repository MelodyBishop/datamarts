
USE [KPI]
GO

DROP TABLE [dbo].[KPI_MedicaidByMonth_WDetails]
GO
---------------------------------------------------------
--Author: Melody Bishop
--Date:09/19/2018
--Using DOSYYYYMM to join to main table, removing yyyy & mmm
-------------------------------------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[KPI_MedicaidByMonth_WDetails](
	[ServiceCount] [int] NULL,
	[BillSvcCount] [int] NULL,
	[Program] [varchar](100) NULL,
	[Base_RVU] [decimal](18, 2) NULL,
	[SUM_RVU] [decimal](18, 2) NULL,
	[UnitCost] [decimal](18, 2) NULL,
	[Cost_of_Service] [decimal](18, 2) NULL,
	--[YYYY] [int] NULL,
	--[MMM] [int] NULL,
	[DOS_YYYYMM] int NULL,
	[CostCenter] [varchar](100) NULL,
	[PrimaryPayor] [varchar](100) NULL,
	[Duration] [decimal](18, 2) NULL,
	[CPT_code] [varchar](50) NULL,
	[Location] [varchar](40) NULL,
	[FinalSavedDate] [date] NULL,
	[ServiceChargeCode] [varchar](100) NULL,
	[Practitioner_name] [varchar](40) NULL
) ON [PRIMARY]
GO


