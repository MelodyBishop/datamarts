
USE [KPI]
GO

/****** Object:  StoredProcedure [dbo].[Generate_Test_RVUCalc]    Script Date: 9/17/2018 4:54:07 PM ******/
DROP PROCEDURE [dbo].[Generate_Test_RVUCalc]
GO

/****** Object:  StoredProcedure [dbo].[Generate_Test_RVUCalc]    Script Date: 9/17/2018 4:54:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------
--TEST Generate RVUs and extensions based on them

--exec KPI.[dbo].[Generate_Test_RVUCalc]
--------------------------------------------------------
Create Procedure [dbo].[Generate_Test_RVUCalc]
as
Begin
Truncate table [dbo].[KPI_TestRVUCalc]
insert into  [dbo].[KPI_TestRVUCalc]
Select 
yyyymm
,pgmnum
,Sum(ExtendedRVUxCFAmount) SumExtRVU$s
,Sum(calc_RVUUnits) CountUnitSvcs
,Sum(ServiceUnits) CountBillableSvcUnits
--,Sum(RelativeUnits) TotalRelativeUnits
,Sum(Cost_Of_Service) SumCostOfService
,unitcost
--,Sum(Charge_Amount) SumChargedAmt
--,baservu
FRom (

select
left(datesvc_ymd,6) yyyymm,
[service_program_code] pgmnum,
[CPT_Code],
service_charge_code,
service_charge_value,
Case len(Base_RVU) when 0 then '0.00'  else base_rvu end BaseRVU,
ISNULL(Sum_RVU,0) SUMRVU,
86.79 CF,
ISNULL(Sum_RVU,0)*86.79 ExtendedRVUxCFAmount,
Isnull(SUM_RVU,0)*ISNULL(calc_Units,0) RelativeUnits,
RVUISFacility,
Case when base_rvu is null then 0 else 1 end ServiceUnits,
IsNull(calc_Units,0) Calc_RVUUnits 
,cost_of_service
,unitcost
,Primary_guarantor
from JCMH.[dbo].[JCMH__SERVICE_avatar_research]
--left join AV svc rsch
where datesvc_ymd between '20161001' and '20180731'
and Primary_guarantor in (1116,1065)--('medicaid CAP, JCMH','Medicaid - CCHA')
--and cpt_code='90792'
--and [service_program_code]=8400
--and cpt_code <>'90882'
--and cpt_code <>'97803'
--and cpt_code <>'J0401'
--and RVUisfacility ='Y'
) sub
GRoup by yyyymm,pgmnum,unitcost

end
GO


