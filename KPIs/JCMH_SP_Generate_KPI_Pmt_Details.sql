USE [KPI]
GO
DROP PROCEDURE [dbo].[Generate_KPI_Pmt_Details]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-------------------------------------------------
--To grab Notes and Payments from Pay table for now, avoiding Navicure for prototype
--Authored:  Melody Bishop
--Date: 09/11/2018
-------------------------------------------------------
CREATE PROCEDURE [dbo].[Generate_KPI_Pmt_Details]
as
begin
---exec dbo.[Generate_KPI_Pmt_Details]
Truncate table kpi.dbo.[KPI_Pmt_Details]
Insert into kpi.dbo.[KPI_Pmt_Details]
select 
n.patid Pmt_Patid,
b.guarantor_id Pmt_Guarantor_ID,
n.[NOT_uniqueid] Pmt_NOT_UniqueID,
n.join_to_tx_history Pmt_Join_TO_TX_History,
Service_YYYYMM Pmt_Service_YYYYMM,
b.Claim_Number Pmt_Claim_Number,
pay.date_of_payment Pmt_Date
,pay.date_of_service Pmt_DOS
,pay.payment_amount Pmt_Amount
,n.Program Pmt_Program
,cr.CostCenter Pmt_CostCenter
from [dbo].[KPI_NotesByMonth] n
left join AVATARDW.[SYSTEM].[billing_tx_charge_detail] b
on n.patid = b.patid
and n.[JOIN_TO_TX_HISTORY] = b.[JOIN_TO_TX_HISTORY]
left join AVATARDW.[SYSTEM].[billing_pay_adj_history] pay
on n.patid = pay.patid
and n.facility = pay.facility
and n.join_to_tx_history = pay.JOIN_TO_TX_HISTORY
and b.guarantor_id = pay.guarantor_id
left join [dbo].[Program_To_CC_Crosswalk] cr
on Left(n.program,4) = cr.prgnum
where n.DOS>'2018-01-01'
and b.guarantor_id not in (99999,2000,2001)
and pay.payment_amount is not null

end
GO


