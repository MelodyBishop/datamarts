--Per Gina
--To grab claim history

--count=770,373 as of 9/5 NOT NULL date of service
-- less 2 for DOS > Today -->770,371
use avatardw
select 
n.patid,
[NOT_uniqueid],
1 CountOfService,
n.join_to_tx_history,
[service_program_code]+'-'+[service_program_value] Program,
[service_charge_code]+'-'+[service_charge_value] Service,
n.date_of_service DOS,
draft_final_value DF,
cast(year(n.date_of_service) as char(4)) + right('0' + cast(month(n.date_of_service) as varchar), 2) Service_YYYYMM,
b.Guarantor_ID,
b.Guarantor_Liability,
b.Claim_number,
b.guarantor_total_payments,
b.v_claim_date
from avatardw.[CWSSYSTEM].[cw_patient_notes] n
left join system.billing_tx_charge_detail b
on n.patid = b.patid
and n.facility = b.facility
and n.JOIN_TO_TX_HISTORY = b.JOIN_TO_TX_HISTORY
and n.EPISODE_NUMBER=b.EPISODE_NUMBER
where n.patid =10002846 
and 
n.date_of_service is not null
and n.date_of_service<DateAdd(d,1,DateAdd(d,0,GetDate()))
and guarantor_id not in (99999)
order by patid,join_to_tx_history desc

 
select * from  system.billing_tx_charge_detail b where patid=10002846 order by join_to_TX_History desc