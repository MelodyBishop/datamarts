
SELECT c.[PATID]
      ,c.[GUARANTOR_ID]
      ,c.[JOIN_TO_TX_HISTORY] Jx2Hist
      ,[CLAIM_NUMBER]
      ,c.[JOIN_TO_PAYMENT_HISTORY] Jx2Pmt
      ,[claim_date]
      ,[clm_sub_reas_value] ClmSubRsnVal
      ,[date_of_claim]
	  ,cast(year([date_of_claim]) as char(4)) + right('0' + cast(month([date_of_claim]) as varchar), 2) YYYYMM
      ,[re_bill_status]
      ,Sum([total_claim_liability]) SumClaimLiab

  FROM [AvatarDW].[SYSTEM].[billing_claim_history_tx_detail] c
 WHERE claim_number =1
  Group by c.[PATID]
      ,c.[GUARANTOR_ID]
      ,c.[JOIN_TO_TX_HISTORY]
	  ,[CLAIM_NUMBER]
	  ,c.[JOIN_TO_PAYMENT_HISTORY] 
      ,[claim_date]
      ,[clm_sub_reas_value] 
      ,[date_of_claim]
	  ,cast(year([date_of_claim]) as char(4)) + right('0' + cast(month([date_of_claim]) as varchar), 2)
	  ,[re_bill_status]
	  

