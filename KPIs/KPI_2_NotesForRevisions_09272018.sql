
--CREATE PROCEDURE [dbo].[Generate_KPI2_NotesByMonth]
--as
--begin
--Truncate table kpi.dbo.KPI#2_NotesByMonth
--Insert into kpi.dbo.KPI#2_NotesByMonth

select 
n.patid,
--1 Facility,--remove this, and 2nd facility will certainly be removed from this dataset and we are not using
n.txhist_episode Episode_Number,
[NOT_uniqueid],
1 CountOfServices,
Case Duration when 0 then 0 else 
	Case calc_units when 0 then 0 else   
		Case IsNumeric(Base_RVU) when 0 then 0 else 
			Case Len(Base_RVU) when 0 then 0  else 1  end end	end end  	CountOfBillableServices,
n.join_to_tx_history,
service_program_code Program,
service_program_code + '-' + service_program_Value ProgramExport,
service_charge_code Service,
service_charge_code + '-' + service_charge_value ServiceExport,
n.date_of_service DOS,
Case FinalSave when null then 'Draft' else 'Final' end DF,
cast(year(n.date_of_service) as char(4)) + right('0' + cast(month(n.date_of_service) as varchar), 2) Service_YYYYMM,
costcenter,
ccnum,
Case IsNumeric(base_RVU) when 0 then 0.00 when 1 then ISNULL(Base_RVU,0.00) else 0 end BaseRVU,
ISNULL(Sum_RVU,0) SUMRVU,
Duration,
Case IsNumeric(base_RVU) when 0 then 0 when 1 then Calc_Units else Calc_Units end Units,
--Cost_of_service--We are not using CostOfService Per LD

from JCMH.[dbo].[JCMH__Service_Avatar_research] n
left join [dbo].[Program_To_CC_Crosswalk] c
on n.service_program_code = c.PrgNum
where n.date_of_service is not null
and n.date_of_service<DateAdd(d,1,DateAdd(d,0,GetDate()))
and n.date_of_service>'2018-01-01'--771673 rows, no duration=0 769,165

end
GO


