--Note environmental variable in the email profile below

Declare @Count int
DECLARE @bodyText varchar(200)

Set @Count=(select Count(*)
  from  (select code, code_value from AVATARDW.[SYSTEM].[dictionaries_pat_ext]
			where field_description='Location'
			and ext_field_number=578) loc
 Left join jcmh.dbo.[JCMH_A_TL_LK_Sev_RVUFacilities] Fac
  on loc.code = Fac.Location_code
  where Fac.Location_code 
  is null)

Set @BodyText =N'Please request from David Goff whether these are IsFacilty:<BR><BR>' +
N'<table border="1">' +
N'<tr><th>Location</th><th>Name</th>' +
CAST ( ( SELECT td = code,    '',
                td = code_value,  ''
from (select code, code_value from AVATARDW.[SYSTEM].[dictionaries_pat_ext]
			where field_description='Location'
			and ext_field_number=578) loc
 Left join jcmh.dbo.[JCMH_A_TL_LK_Sev_RVUFacilities] Fac
  on loc.code = Fac.Location_code
  where Fac.Location_code 
  is null
          FOR XML PATH('tr'), TYPE 
) AS NVARCHAR(MAX) ) +
N'</table>' ;

EXEC msdb.dbo.sp_send_dbmail  
--environmental vaiable here based on server typically TIER or Datawarehouse
    @profile_name = 'DataWarehouse',  
    @recipients = 'melodyb@jcmh.org',  
    @body_format = 'HTML',
	@body = @bodyText,
    @subject = 'RVU Facility Updates needed' 
GO
	
Insert into jcmh.dbo.[JCMH_A_TL_LK_Sev_RVUFacilities]
Select code, code_value, 'N' from (select code, code_value from AVATARDW.[SYSTEM].[dictionaries_pat_ext]
			where field_description='Location'
			and ext_field_number=578) loc
			Left join jcmh.dbo.[JCMH_A_TL_LK_Sev_RVUFacilities] Fac
			on loc.code = Fac.Location_code
			  where Fac.Location_code 
			  is null
GO