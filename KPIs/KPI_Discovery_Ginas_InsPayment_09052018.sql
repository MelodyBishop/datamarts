use avatardw
select
'Ins Service' as Type,
a.PATID as PATID,
--a.v_client_name as Name,
--a.episode_number as Ep,
a.date_of_service as DOS,
c.data_entry_date as DED,
--a.v_service_code as SvcCode,
--a.v_service_value as Descr,
--a.v_PROVIDER_NAME as Staff,
a.JOIN_TO_TX_HISTORY as HistJoin,
a.transferred_service as TxfrSvc,
a.service_status_code as SvcStatCode,
a.service_status_value as SvcStatVal,
a.ORIG_JOIN_TO_TX_HISTORY as OrigHistJoin,
a.guarantor_id as GuarID,
a.join_to_payment_history_ as PmtJoin,
c.cost_of_service as COS,
a.guarantor_liability as GuarLiab,
0 as PmtAmt,
a.transfer_to_guarantor_code as TxfrGuarID,
a.transfer_to_guarantor_value as TxFrGuar,
a.remaining_balance as Bal,
a.re_bill_status_value as RebillStatVal,
a.re_bill_status as RebillStat,
a.re_bill_claim_number as RebillClm,
a.v_claim_date as ClmDate,
a.guarantor_total_payments as GuarPmts,
a.claim_number as Claim#,
NULL as PmtCode,
NULL as DOP,
NULL as PmtCom,
NULL as TxfrJoin,
NULL as RowID,
b.guarantor_name as Guar
from system.billing_tx_charge_detail a
join system.billing_guar_table b 
on a.guarantor_id = b.guarantor_id
join system.billing_tx_history c 
on a.patid = c.patid 
and a.episode_number = c.episode_number 
and a.join_to_tx_history = c.join_to_tx_history
--where-- a.patid = {?CLIENT}
--a.guarantor_id not in ('1027','1105','1106',2001,99999)--exc w/o too, we may write off some services up front
--907,074 with all Guars
-- no self pay 
--2001 is write-off