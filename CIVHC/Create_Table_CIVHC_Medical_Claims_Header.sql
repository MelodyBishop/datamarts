

USE [Datamart2]
GO

/****** Object:  Table [CIVHC].[Medical_Claims_Header]    Script Date: 11/6/2018 3:46:30 PM ******/
DROP TABLE [CIVHC].[Medical_Claims_Header]
GO

/****** Object:  Table [CIVHC].[Medical_Claims_Header]    Script Date: 11/6/2018 3:46:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CIVHC].[Medical_Claims_Header](
	[Billing_Provider_Composite_ID] [int] NULL,
	[Billing_Provider_ID] [varchar](30) NULL,
	[Claim_ID] [int] NULL,
	[Member_Composite_ID] [int] NULL,
	[Member_ID] [int] NULL,
	[ICD_Primary_Procedure_Cd] [varchar](7) NULL,
	[ICD_Vers_Flag] [varchar](1) NULL,
	[Admit_Diagnosis_Cd] [varchar](7) NULL,
	[Principal_Diagnosis_Cd] [varchar](7) NULL,
	[Admit_Dt] [date] NULL,
	[Admit_Dt_Day] [varchar](2) NULL,
	[Admit_Dt_Month] [varchar](2) NULL,
	[Admit_Dt_Year] [varchar](4) NULL,
	[Admit_Time] [varchar](4) NULL,
	[Discharge_Dt] [date] NULL,
	[Discharge_Dt_Day] [varchar](2) NULL,
	[Discharge_Dt_Month] [varchar](2) NULL,
	[Discharge_Dt_Year] [varchar](4) NULL,
	[Discharge_Time] [varchar](4) NULL,
	[Paid_Dt] [date] NULL,
	[Paid_Dt_Day] [varchar](2) NULL,
	[Paid_Dt_Month] [varchar](2) NULL,
	[Paid_Dt_Year] [varchar](4) NULL,
	[Service_End_Dt] [date] NULL,
	[Service_End_Dt_Day] [varchar](2) NULL,
	[Service_End_Dt_Month] [varchar](2) NULL,
	[Service_End_Dt_Year] [varchar](4) NULL,
	[Service_Start_Dt] [date] NULL,
	[Service_Start_Dt_Day] [varchar](2) NULL,
	[Service_Start_Dt_Month] [varchar](2) NULL,
	[Service_Start_Dt_Year] [varchar](4) NULL,
	[Allowed_Amt] [decimal](18, 2) NULL,
	[Charge_Amt] [decimal](18, 2) NULL,
	[Coinsurance_Amt] [decimal](18, 2) NULL,
	[Copay_Amt] [decimal](18, 2) NULL,
	[Deductible_Amt] [decimal](18, 2) NULL,
	[Member_Liability_Amt] [decimal](18, 2) NULL,
	[Plan_Paid_Amt] [decimal](18, 2) NULL,
	[Prepaid_Amt] [decimal](18, 2) NULL,
	[Admit_Source_Cd] [varchar](1) NULL,
	[Admit_Source_Desc] [varchar](200) NULL,
	[Admit_Type_Cd] [varchar](2) NULL,
	[Admit_Type_Desc] [varchar](100) NULL,
	[Bill_Type_Cd] [varchar](3) NULL,
	[Bill_Type_Desc] [varchar](145) NULL,
	[Capitation_Flag] [varchar](1) NULL,
	[Claim_Status_Cd] [varchar](2) NULL,
	[Claim_Type_Cd] [varchar](1) NULL,
	[COB_Flag] [varchar](1) NULL,
	[Dental_Carrier_Flag] [varchar](1) NULL,
	[Dental_Flag] [varchar](1) NULL,
	[Discharge_Status_Cd] [varchar](2) NULL,
	[E_Cd] [varchar](7) NULL,
	[ER_Flag] [varchar](1) NULL,
	[Insurance_Product_Type_Cd] [varchar](2) NULL,
	[Insurance_Product_Type_Desc] [varchar](75) NULL,
	[Length_of_Stay] [decimal](18, 2) NULL,
	[Line_Count] [int] NULL,
	[Line_of_Business_Cd] [varchar](1) NULL,
	[Member_Age_Days] [int] NULL,
	[Member_Age_Years] [int] NULL,
	[Member_Age_Years_YE] [int] NULL,
	[Member_Eligible_Flag] [varchar](1) NULL,
	[Payer_Cd] [varchar](8) NULL
) ON [PRIMARY]
GO


