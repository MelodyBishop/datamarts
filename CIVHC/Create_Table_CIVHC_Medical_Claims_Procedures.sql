
USE [Datamart2]
GO

DROP TABLE [CIVHC].[Medical_Claims_Procedures]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CIVHC].[Medical_Claims_Procedures](
	[Claim_ID] [int] NULL,
	[ICD_Vers_Flag] [varchar](1) NULL,
	[Procedure_Cd] [varchar](7) NULL,
	[Procedure_Type] [varchar](25) NULL,
	[Seq_Num] [int] NULL,
	[Procedure_Dt] [date] NULL,
	[Procedure_Dt_Day] [varchar](2) NULL,
	[Procedure_Dt_Month] [varchar](2) NULL,
	[Procedure_Dt_Year] [varchar](4) NULL
) ON [PRIMARY]
GO


