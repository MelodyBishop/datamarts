

USE [Datamart2]
GO

DROP TABLE [CIVHC].[Provider_to_Provider_Composite_Crosswalk]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CIVHC].[Provider_to_Provider_Composite_Crosswalk](
	[Provider_Composite_ID] [char](8) NULL,
	[Provider_ID] [int] NULL,
	[Effective_Date] [varchar](8) NULL
) ON [PRIMARY]
GO


