
USE [Datamart2]
GO

DROP TABLE [CIVHC].[Pharmacy_Claims_Line]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CIVHC].[Pharmacy_Claims_Line](
	[Claim_ID] [int] NULL,
	[Member_Composite_ID] [int] NULL,
	[Member_ID] [int] NULL,
	[Fill_Dt] [date] NULL,
	[Fill_Dt_Day] [varchar](2) NULL,
	[Fill_Dt_Month] [varchar](2) NULL,
	[Fill_Dt_Year] [varchar](4) NULL,
	[Charge_Amt] [decimal](18, 2) NULL,
	[Coinsurance_Amt] [decimal](18, 2) NULL,
	[Copay_Amt] [decimal](18, 2) NULL,
	[Deductible_Amt] [decimal](18, 2) NULL,
	[Dispensing_Fee_Amt] [decimal](18, 2) NULL,
	[Ingredient_Cost_Amt] [decimal](18, 2) NULL,
	[Member_Liability_Amt] [decimal](18, 2) NULL,
	[Plan_Paid_Amt] [decimal](18, 2) NULL,
	[Drug_Nm] [varchar](80) NULL,
	[NDC_Cd] [varchar](11) NULL,
	[Compound_Drug_Ind] [varchar](1) NULL,
	[Days_Supply] [int] NULL,
	[Dispensed_As_Written_Cd] [varchar](1) NULL,
	[Generic_Drug_Indicator] [varchar](1) NULL,
	[Line_No] [int] NULL,
	[Quantity_Dispensed] [int] NULL,
	[Refill_Ind] [varchar](2) NULL
) ON [PRIMARY]
GO


