

USE [Datamart2]
GO

DROP TABLE [CIVHC].[Provider_Composite]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CIVHC].[Provider_Composite](
	[Provider_Composite_ID] [varchar](8) NULL,
	[Credential_Text_1] [varchar](20) NULL,
	[Gender_Cd] [varchar](1) NULL,
	[License_1] [varchar](15) NULL,
	[License_2] [varchar](15) NULL,
	[License_3] [varchar](15) NULL,
	[License_4] [varchar](15) NULL,
	[License_5] [varchar](15) NULL,
	[License_State_1] [varchar](2) NULL,
	[License_State_2] [varchar](2) NULL,
	[License_State_3] [varchar](2) NULL,
	[License_State_4] [varchar](2) NULL,
	[License_State_5] [varchar](2) NULL,
	[Medicaid_Facility_Number] [varchar](20) NULL,
	[Medicare_Provider_Id] [varchar](20) NULL,
	[National_Provider_ID] [varchar](10) NULL,
	[Organization_Nm] [varchar](100) NULL,
	[Organization_Nm_Clean] [varchar](100) NULL,
	[Organization_Other_Nm] [varchar](100) NULL,
	[Organization_Other_Nm_Clean] [varchar](100) NULL,
	[Other_First_Initial] [varchar](1) NULL,
	[Other_First_Nm] [varchar](60) NULL,
	[Other_Last_Nm] [varchar](60) NULL,
	[Other_Middle_Initial] [varchar](1) NULL,
	[Other_Middle_Nm] [varchar](60) NULL,
	[Other_Nm_Prefix] [varchar](15) NULL,
	[Other_Nm_Suffix] [varchar](15) NULL,
	[Phone_Number] [varchar](15) NULL,
	[Primary_Address_ID] [varchar](4) NULL,
	[Provider_DEA_No] [varchar](12) NULL,
	[Provider_First_Initial] [varchar](1) NULL,
	[Provider_First_Nm] [varchar](60) NULL,
	[Provider_Last_Nm] [varchar](60) NULL,
	[Provider_Middle_Initial] [varchar](1) NULL,
	[Provider_Middle_Nm] [varchar](60) NULL,
	[Provider_Nm] [varchar](100) NULL,
	[Provider_Nm_Prefix] [varchar](15) NULL,
	[Provider_Nm_Suffix] [varchar](15) NULL,
	[Provider_Type] [varchar](3) NULL,
	[Taxonomy_Cd_1] [varchar](12) NULL,
	[Taxonomy_Cd_2] [varchar](12) NULL,
	[Taxonomy_Cd_3] [varchar](12) NULL,
	[Taxonomy_Cd_4] [varchar](12) NULL,
	[Taxonomy_Cd_5] [varchar](12) NULL
) ON [PRIMARY]
GO


