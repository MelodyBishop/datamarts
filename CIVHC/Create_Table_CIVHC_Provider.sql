

USE [Datamart2]
GO

DROP TABLE [CIVHC].[Provider]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CIVHC].[Provider](
	[Provider_ID] [varchar](30) NULL,
	[Provider_Office_City_Nm] [varchar](60) NULL,
	[Provider_Office_State_Cd] [varchar](4) NULL,
	[Provider_Office_Street_Address] [varchar](50) NULL,
	[Provider_Office_Zip_Cd] [varchar](11) NULL,
	[Provider_Office_Zip_Cd_3_Digit] [varchar](11) NULL,
	[Medicare_Provider_ID] [varchar](30) NULL,
	[Medicare_Provider_Nm] [varchar](10) NULL,
	[National_Provider_ID] [varchar](20) NULL,
	[Payer_Cd] [varchar](8) NULL,
	[Provider_DEA_No] [varchar](12) NULL,
	[Provider_Entity] [varchar](1) NULL,
	[Provider_First_Nm] [varchar](50) NULL,
	[Provider_Last_Nm] [varchar](120) NULL,
	[Provider_Middle_Nm] [varchar](50) NULL,
	[Provider_Specialty_Cd] [varchar](50) NULL,
	[Provider_State_License_Number] [varchar](20) NULL,
	[Provider_Suffix_Nm] [varchar](10) NULL,
	[Provider_Tax_ID] [varchar](10) NULL
) ON [PRIMARY]
GO


