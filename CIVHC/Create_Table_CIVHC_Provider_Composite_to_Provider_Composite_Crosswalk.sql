

USE [Datamart2]
GO

DROP TABLE CIVHC.[Provider_Composite_to_Provider_Composite_Address_Crosswalk]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE CIVHC.[Provider_Composite_to_Provider_Composite_Address_Crosswalk](
	[Provider_Composite_Address_ID] [varchar](6) NULL,
	[Provider_Composite_ID] [varchar](8) NULL
) ON [PRIMARY]
GO


