

USE [Datamart2]
GO

DROP TABLE [CIVHC].[Member_to_Member_Composite_Crosswalk]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CIVHC].[Member_to_Member_Composite_Crosswalk](
	[Member_Composite_ID] [int] NULL,
	[Member_ID] [int] NULL,
	[Effective_Date] [date] NULL
) ON [PRIMARY]
GO


