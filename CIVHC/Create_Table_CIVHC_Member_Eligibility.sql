
USE [Datamart2]
GO

DROP TABLE [CIVHC].[Member_Eligibility]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CIVHC].[Member_Eligibility](
	[Member_ID] [int] NULL,
	[Eligibility_Day] [int] NULL,
	[Eligibility_Dt] [date] NULL,
	[Eligibility_Month] [int] NULL,
	[Eligibility_Year] [int] NULL,
	[Plan_Effective_Dt] [date] NULL,
	[Plan_Effective_Dt_Day] [int] NULL,
	[Plan_Effective_Dt_Month] [int] NULL,
	[Plan_Effective_Dt_Year] [int] NULL,
	[Coverage_Level_Cd] [varchar](3) NULL,
	[Coverage_Type_Cd] [varchar](3) NULL,
	[Dental_Coverage_Flag] [varchar](1) NULL,
	[Employer_Nm] [int] NULL,
	[Group_Nm] [int] NULL,
	[Insurance_Product_Type_Cd] [varchar](2) NULL,
	[Insurance_Product_Type_Desc] [varchar](100) NULL,
	[Line_of_Business_Cd] [int] NULL,
	[Line_of_Business_Cd_Desc] [varchar](32) NULL,
	[Market_Category_Cd] [varchar](4) NULL,
	[Medical_Coverage_Flag] [varchar](1) NULL,
	[Prescription_Drug_Coverage_Flag] [varchar](1) NULL,
	[Primary_Insurance_Ind] [varchar](1) NULL
) ON [PRIMARY]
GO


