

USE [Datamart2]
GO

DROP TABLE [CIVHC].[Provider_Composite_Address]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CIVHC].[Provider_Composite_Address](
	[Provider_Composite_Address_ID] [varchar](6) NULL,
	[Address] [varchar](25) NULL,
	[Address_Type_Cd] [varchar](1) NULL,
	[City] [varchar](60) NULL,
	[Latitude] [decimal](18, 2) NULL,
	[Longitude] [decimal](18, 2) NULL,
	[State] [varchar](2) NULL,
	[Zip_Cd] [varchar](10) NULL,
	[Zip_Cd_3_Digit] [varchar](3) NULL
) ON [PRIMARY]
GO


