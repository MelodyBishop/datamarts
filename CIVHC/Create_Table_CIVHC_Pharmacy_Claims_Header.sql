
USE [Datamart2]
GO

DROP TABLE [CIVHC].[Pharmacy_Claims_Header]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CIVHC].[Pharmacy_Claims_Header](
	[Claim_ID] [int] NULL,
	[Member_Composite_ID] [int] NULL,
	[Member_ID] [int] NULL,
	[Fill_Date_First] [date] NULL,
	[Fill_Date_First_Day] [varchar](2) NULL,
	[Fill_Date_First_Month] [varchar](2) NULL,
	[Fill_Date_First_Year] [varchar](4) NULL,
	[Fill_Date_Latest] [date] NULL,
	[Fill_Date_Latest_Day] [varchar](2) NULL,
	[Fill_Date_Latest_Month] [varchar](2) NULL,
	[Fill_Date_Latest_Year] [varchar](2) NULL,
	[Paid_Dt] [date] NULL,
	[Paid_Dt_Day] [varchar](2) NULL,
	[Paid_Dt_Month] [varchar](2) NULL,
	[Paid_Dt_Year] [varchar](4) NULL,
	[Allowed_Amt] [decimal](22, 0) NULL,
	[Charge_Amt] [decimal](18, 2) NULL,
	[Coinsurance_Amt] [decimal](18, 2) NULL,
	[Copay_Amt] [decimal](18, 2) NULL,
	[Deductible_Amt] [decimal](18, 2) NULL,
	[Member_Liability_Amt] [decimal](18, 2) NULL,
	[Plan_Paid_Amt] [decimal](18, 2) NULL,
	[Postage_Claim_Amt] [decimal](18, 2) NULL,
	[Claim_Status_Cd] [varchar](2) NULL,
	[Claim_Type_Cd] [varchar](1) NULL,
	[COB_Flag] [varchar](1) NULL,
	[Insurance_Product_Type_Cd] [varchar](2) NULL,
	[Insurance_Product_Type_Desc] [varchar](75) NULL,
	[Line_of_Business_Cd] [varchar](1) NULL,
	[Member_Age_Days] [int] NULL,
	[Member_Age_Years] [int] NULL,
	[Member_Age_Years_YE] [int] NULL,
	[Member_Eligible_Flag] [varchar](1) NULL,
	[Payer_Cd] [varchar](8) NULL,
	[Pharmacy_ID] [int] NULL
) ON [PRIMARY]
GO


