
USE [Datamart2]
GO

DROP TABLE [CIVHC].[Medical_Claims_DX]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CIVHC].[Medical_Claims_DX](
	[Claim_ID] [int] NULL,
	[DX_Cd] [varchar](7) NULL,
	[DX_Description] [varchar](25) NULL,
	[DX_Type] [varchar](10) NULL,
	[ICD_Seq_Num] [int] NULL,
	[ICD_Vers_Flag] [varchar](1) NULL,
	[POA_Cd] [varchar](2) NULL,
	[POA_Description] [varchar](100) NULL,
	[POA_Seq_Num] [int] NULL
) ON [PRIMARY]
GO


