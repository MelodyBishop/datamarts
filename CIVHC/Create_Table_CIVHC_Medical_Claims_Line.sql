

USE [Datamart2]
GO

/****** Object:  Table [CIVHC].[Medical_Claims_Line]    Script Date: 11/6/2018 3:54:57 PM ******/
DROP TABLE [CIVHC].[Medical_Claims_Line]
GO

/****** Object:  Table [CIVHC].[Medical_Claims_Line]    Script Date: 11/6/2018 3:54:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CIVHC].[Medical_Claims_Line](
	[Billing_Provider_Composite_ID] [int] NULL,
	[Billing_Provider_ID] [varchar](30) NULL,
	[Claim_ID] [int] NULL,
	[Member_Composite_ID] [int] NULL,
	[Member_ID] [int] NULL,
	[Service_Provider_Composite_ID] [int] NULL,
	[Service_Provider_ID] [varchar](30) NULL,
	[CPT4_Cd] [varchar](7) NULL,
	[CPT4_Mod1_Cd] [varchar](2) NULL,
	[CPT4_Mod2_Cd] [varchar](2) NULL,
	[CPT4_Mod3_Cd] [varchar](2) NULL,
	[CPT4_Mod4_Cd] [varchar](2) NULL,
	[Revenue_Cd] [varchar](4) NULL,
	[Service_End_Dt] [date] NULL,
	[Service_End_Dt_Day] [varchar](2) NULL,
	[Service_End_Dt_Month] [varchar](2) NULL,
	[Service_End_Dt_Year] [varchar](4) NULL,
	[Service_Start_Dt] [date] NULL,
	[Service_Start_Dt_Day] [varchar](2) NULL,
	[Service_Start_Dt_Month] [varchar](2) NULL,
	[Service_Start_Dt_Year] [varchar](4) NULL,
	[Charge_Amt] [decimal](18, 2) NULL,
	[Coinsurance_Amt] [decimal](18, 2) NULL,
	[Copay_Amt] [decimal](18, 2) NULL,
	[Deductible_Amt] [decimal](18, 2) NULL,
	[Member_Liability_Amt] [decimal](18, 2) NULL,
	[Plan_Paid_Amt] [decimal](18, 2) NULL,
	[Prepaid_Amt] [decimal](18, 2) NULL,
	[NDC_Cd] [varchar](14) NULL,
	[Capitation_Flag] [varchar](1) NULL,
	[Dental_Carrier_Flag] [varchar](1) NULL,
	[Dental_Flag] [varchar](1) NULL,
	[ER_Flag] [varchar](1) NULL,
	[Line_No] [int] NULL,
	[Place_of_Service_Cd] [varchar](2) NULL,
	[Service_Qty] [int] NULL,
	[Units] [int] NULL
) ON [PRIMARY]
GO


