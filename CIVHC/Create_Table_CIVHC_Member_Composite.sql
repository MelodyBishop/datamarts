

USE [Datamart2]
GO

DROP TABLE [CIVHC].[Member_Composite]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CIVHC].[Member_Composite](
	[Member_Composite_ID] [int] NULL,
	[Member_DOB] [date] NULL,
	[Member_DOB_Day] [int] NULL,
	[Member_DOB_Month] [int] NULL,
	[Member_DOB_Year] [int] NULL,
	[Member_State_Cd] [varchar](2) NULL,
	[Member_Zip_Cd] [varchar](11) NULL,
	[Member_Zip_Cd_3_Digit] [varchar](11) NULL,
	[Ethnicity_1_Cd] [varchar](6) NULL,
	[Ethnicity_2_Cd] [varchar](6) NULL,
	[Hispanic_Ind] [varchar](1) NULL,
	[Member_Gender_Cd] [varchar](1) NULL,
	[Member_Subscriber_Rlp_Cd] [varchar](2) NULL,
	[Other_Ethnicity] [varchar](20) NULL,
	[Other_Race] [varchar](15) NULL,
	[Payer_Cd] [varchar](8) NULL,
	[Race_1_Cd] [varchar](6) NULL,
	[Race_1_Cd_Desc] [varchar](50) NULL,
	[Race_2_Cd] [varchar](6) NULL,
	[Race_2_Cd_Desc] [varchar](50) NULL
) ON [PRIMARY]
GO


